﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Jeelu.FormUI.DockWindow.Views.MdiForms
{
    public class CompanySelectForm<T> : SimpleForm where T : class
    {
        public CompanySelectForm()
        {
            InitializeComponent();
            RegistEvent();
        }

        public string EntityName { get; set; }

        public Func<IQueryable<T>> QueryFunc { get; set; } 

        private void RegistEvent()
        {
            //搜索
            _FindBtn.Click += (sender, args) =>
                              {
                                  if (!string.IsNullOrWhiteSpace(_FindKeyTbox.Text))
                                      Find(_FindKeyTbox.Text, _View);
                              };
            //确定
            _OkBtn.Click += (sender, e) =>
                            {
                                if (_View.CheckedItems.Count > 0)
                                {
                                    Entity = (T) _View.CheckedItems[0].Tag;
                                    DialogResult = DialogResult.OK;
                                }
                                else
                                    DialogResult = DialogResult.Cancel;
                                Close();
                            };
            //取消
            _CancelBtn.Click += (sender, e) =>
                                {
                                    DialogResult = DialogResult.Cancel;
                                    Close();
                                };
        }

        private static void Find(string text, ListView view)
        {
            var list = view.Items.Cast<ListViewItem>().Where(item => !item.SubItems[0].Text.Contains(text)).ToList();
            foreach (var item in list)
            {
                view.Items.Remove(item);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Text = string.Format("{0}列表", EntityName);
            var tarray = QueryFunc.Invoke();
            foreach (var t in tarray)
            {
                var item = new ListViewItem();
                item.SubItems.Add(t.ToString());
                item.Tag = t;
                _View.Items.Add(item);
            }
        }

        private T _Entity;

        /// <summary>获取当前的实体对象
        /// </summary>
        public T Entity
        {
            get { return _Entity; }
            set
            {
                if (value != null && !value.Equals(Entity))
                {
                    _Entity = value;
                    OnEntityChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>实体发生变化时的事件
        /// </summary>
        public event EventHandler EntityChangedEvent;
        protected virtual void OnEntityChanged(EventArgs e)
        {
            if (EntityChangedEvent != null)
                EntityChangedEvent(this, e);
        }

        #region 窗体设计

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly IContainer _Components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (_Components != null))
            {
                _Components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._View = new SingleRowCheckedListView();
            this._NameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._FindBtn = new System.Windows.Forms.Button();
            this._OkBtn = new System.Windows.Forms.Button();
            this._CancelBtn = new System.Windows.Forms.Button();
            this._FindKeyTbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _View
            // 
            this._View.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._View.CheckBoxes = true;
            this._View.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._NameHeader});
            this._View.FullRowSelect = true;
            this._View.GridLines = true;
            this._View.Location = new System.Drawing.Point(12, 12);
            this._View.MultiSelect = false;
            this._View.Name = "_View";
            this._View.ShowGroups = false;
            this._View.Size = new System.Drawing.Size(332, 319);
            this._View.TabIndex = 0;
            this._View.UseCompatibleStateImageBehavior = false;
            this._View.View = System.Windows.Forms.View.Details;
            // 
            // _NameHeader
            // 
            this._NameHeader.Text = "公司名称";
            this._NameHeader.Width = 270;
            // 
            // _FindBtn
            // 
            this._FindBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FindBtn.Location = new System.Drawing.Point(350, 334);
            this._FindBtn.Name = "_FindBtn";
            this._FindBtn.Size = new System.Drawing.Size(67, 26);
            this._FindBtn.TabIndex = 1;
            this._FindBtn.Text = "搜索";
            this._FindBtn.UseVisualStyleBackColor = true;
            // 
            // _OkBtn
            // 
            this._OkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._OkBtn.Location = new System.Drawing.Point(350, 12);
            this._OkBtn.Name = "_OkBtn";
            this._OkBtn.Size = new System.Drawing.Size(67, 33);
            this._OkBtn.TabIndex = 2;
            this._OkBtn.Text = "确定";
            this._OkBtn.UseVisualStyleBackColor = true;
            // 
            // _CancelBtn
            // 
            this._CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._CancelBtn.Location = new System.Drawing.Point(350, 48);
            this._CancelBtn.Name = "_CancelBtn";
            this._CancelBtn.Size = new System.Drawing.Size(67, 33);
            this._CancelBtn.TabIndex = 3;
            this._CancelBtn.Text = "取消";
            this._CancelBtn.UseVisualStyleBackColor = true;
            // 
            // _FindKeyTbox
            // 
            this._FindKeyTbox.Location = new System.Drawing.Point(12, 337);
            this._FindKeyTbox.Name = "_FindKeyTbox";
            this._FindKeyTbox.Size = new System.Drawing.Size(332, 21);
            this._FindKeyTbox.TabIndex = 4;
            // 
            // CompanySelectForm
            // 
            this.AcceptButton = this._OkBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._CancelBtn;
            this.ClientSize = new System.Drawing.Size(426, 373);
            this.ControlBox = false;
            this.Controls.Add(this._FindKeyTbox);
            this.Controls.Add(this._CancelBtn);
            this.Controls.Add(this._OkBtn);
            this.Controls.Add(this._FindBtn);
            this.Controls.Add(this._View);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private SingleRowCheckedListView _View;
        private Button _FindBtn;
        private Button _OkBtn;
        private Button _CancelBtn;
        private ColumnHeader _NameHeader;
        private TextBox _FindKeyTbox;

        #endregion
    }
}
