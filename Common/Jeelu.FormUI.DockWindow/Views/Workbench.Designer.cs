﻿namespace Jeelu.FormUI.DockWindow.Views
{
    sealed partial class Workbench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Workbench));
            this._MenuStrip = new System.Windows.Forms.MenuStrip();
            this._FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._RefrashBtn = new BoundleMenuStripButton();
            this._Separator0 = new System.Windows.Forms.ToolStripSeparator();
            this._ExportAllBtn = new BoundleMenuStripButton();
            this._ExportCurrentBtn = new BoundleMenuStripButton();
            this._Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this._ExitBtn = new BoundleMenuStripButton();
            this.产品DToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.标准产品ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目产品管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目JToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.项目管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.客户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.订单SToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成销售订单ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.售后ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.售后电话记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._ReportsStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._TodayBtn = new BoundleMenuStripButton();
            this._Separator4 = new System.Windows.Forms.ToolStripSeparator();
            this._UserManagerBtn = new BoundleMenuStripButton();
            this._HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._AboutBtn = new BoundleMenuStripButton();
            this._StatusStrip = new System.Windows.Forms.StatusStrip();
            this._StripContainer = new System.Windows.Forms.ToolStripContainer();
            this.角色管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuStrip.SuspendLayout();
            this._StripContainer.ContentPanel.SuspendLayout();
            this._StripContainer.TopToolStripPanel.SuspendLayout();
            this._StripContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuStrip
            // 
            this._MenuStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._FileToolStripMenuItem,
            this.产品DToolStripMenuItem,
            this.项目JToolStripMenuItem,
            this.订单SToolStripMenuItem,
            this.售后ToolStripMenuItem,
            this._ReportsStripMenuItem,
            this._HelpToolStripMenuItem});
            this._MenuStrip.Location = new System.Drawing.Point(0, 0);
            this._MenuStrip.Name = "_MenuStrip";
            this._MenuStrip.Size = new System.Drawing.Size(632, 24);
            this._MenuStrip.TabIndex = 1;
            // 
            // _FileToolStripMenuItem
            // 
            this._FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._RefrashBtn,
            this._Separator0,
            this._ExportAllBtn,
            this._ExportCurrentBtn,
            this._Separator1,
            this._ExitBtn});
            this._FileToolStripMenuItem.Name = "_FileToolStripMenuItem";
            this._FileToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this._FileToolStripMenuItem.Text = "文件(&F)";
            // 
            // _RefrashBtn
            // 
            this._RefrashBtn.EnableBoundle = true;
            this._RefrashBtn.Image = ((System.Drawing.Image)(resources.GetObject("_RefrashBtn.Image")));
            this._RefrashBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._RefrashBtn.Name = "_RefrashBtn";
            this._RefrashBtn.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._RefrashBtn.Size = new System.Drawing.Size(160, 22);
            this._RefrashBtn.Text = "刷新(&F)";
            // 
            // _Separator0
            // 
            this._Separator0.Name = "_Separator0";
            this._Separator0.Size = new System.Drawing.Size(157, 6);
            // 
            // _ExportAllBtn
            // 
            this._ExportAllBtn.EnableBoundle = true;
            this._ExportAllBtn.Image = ((System.Drawing.Image)(resources.GetObject("_ExportAllBtn.Image")));
            this._ExportAllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ExportAllBtn.Name = "_ExportAllBtn";
            this._ExportAllBtn.Size = new System.Drawing.Size(160, 22);
            this._ExportAllBtn.Text = "导出所有数据(&S)";
            // 
            // _ExportCurrentBtn
            // 
            this._ExportCurrentBtn.EnableBoundle = true;
            this._ExportCurrentBtn.Image = ((System.Drawing.Image)(resources.GetObject("_ExportCurrentBtn.Image")));
            this._ExportCurrentBtn.Name = "_ExportCurrentBtn";
            this._ExportCurrentBtn.Size = new System.Drawing.Size(160, 22);
            this._ExportCurrentBtn.Text = "导出当前表(&E)";
            // 
            // _Separator1
            // 
            this._Separator1.Name = "_Separator1";
            this._Separator1.Size = new System.Drawing.Size(157, 6);
            // 
            // _ExitBtn
            // 
            this._ExitBtn.EnableBoundle = false;
            this._ExitBtn.Image = null;
            this._ExitBtn.Name = "_ExitBtn";
            this._ExitBtn.Size = new System.Drawing.Size(160, 22);
            this._ExitBtn.Text = "退出(&X)";
            // 
            // 产品DToolStripMenuItem
            // 
            this.产品DToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.标准产品ToolStripMenuItem,
            this.项目产品管理ToolStripMenuItem});
            this.产品DToolStripMenuItem.Name = "产品DToolStripMenuItem";
            this.产品DToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.产品DToolStripMenuItem.Text = "产品(&D)";
            // 
            // 标准产品ToolStripMenuItem
            // 
            this.标准产品ToolStripMenuItem.Name = "标准产品ToolStripMenuItem";
            this.标准产品ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.标准产品ToolStripMenuItem.Text = "标准产品管理";
            // 
            // 项目产品管理ToolStripMenuItem
            // 
            this.项目产品管理ToolStripMenuItem.Name = "项目产品管理ToolStripMenuItem";
            this.项目产品管理ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.项目产品管理ToolStripMenuItem.Text = "项目产品管理";
            // 
            // 项目JToolStripMenuItem
            // 
            this.项目JToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.项目管理ToolStripMenuItem,
            this.客户管理ToolStripMenuItem});
            this.项目JToolStripMenuItem.Name = "项目JToolStripMenuItem";
            this.项目JToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.项目JToolStripMenuItem.Text = "项目(&J)";
            // 
            // 项目管理ToolStripMenuItem
            // 
            this.项目管理ToolStripMenuItem.Name = "项目管理ToolStripMenuItem";
            this.项目管理ToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.项目管理ToolStripMenuItem.Text = "项目管理";
            // 
            // 客户管理ToolStripMenuItem
            // 
            this.客户管理ToolStripMenuItem.Name = "客户管理ToolStripMenuItem";
            this.客户管理ToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.客户管理ToolStripMenuItem.Text = "客户管理";
            // 
            // 订单SToolStripMenuItem
            // 
            this.订单SToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.生成销售订单ToolStripMenuItem});
            this.订单SToolStripMenuItem.Name = "订单SToolStripMenuItem";
            this.订单SToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.订单SToolStripMenuItem.Text = "订单(&S)";
            // 
            // 生成销售订单ToolStripMenuItem
            // 
            this.生成销售订单ToolStripMenuItem.Name = "生成销售订单ToolStripMenuItem";
            this.生成销售订单ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.生成销售订单ToolStripMenuItem.Text = "生成销售订单";
            // 
            // 售后ToolStripMenuItem
            // 
            this.售后ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.售后电话记录ToolStripMenuItem});
            this.售后ToolStripMenuItem.Name = "售后ToolStripMenuItem";
            this.售后ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.售后ToolStripMenuItem.Text = "售后(&T)";
            // 
            // 售后电话记录ToolStripMenuItem
            // 
            this.售后电话记录ToolStripMenuItem.Name = "售后电话记录ToolStripMenuItem";
            this.售后电话记录ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.售后电话记录ToolStripMenuItem.Text = "售后电话记录";
            // 
            // _ReportsStripMenuItem
            // 
            this._ReportsStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._TodayBtn,
            this._Separator4,
            this._UserManagerBtn,
            this.角色管理ToolStripMenuItem});
            this._ReportsStripMenuItem.Name = "_ReportsStripMenuItem";
            this._ReportsStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this._ReportsStripMenuItem.Text = "权限(&M)";
            // 
            // _TodayBtn
            // 
            this._TodayBtn.EnableBoundle = true;
            this._TodayBtn.Image = ((System.Drawing.Image)(resources.GetObject("_TodayBtn.Image")));
            this._TodayBtn.Name = "_TodayBtn";
            this._TodayBtn.Size = new System.Drawing.Size(152, 22);
            this._TodayBtn.Text = "经销商管理";
            // 
            // _Separator4
            // 
            this._Separator4.Name = "_Separator4";
            this._Separator4.Size = new System.Drawing.Size(149, 6);
            // 
            // _UserManagerBtn
            // 
            this._UserManagerBtn.EnableBoundle = true;
            this._UserManagerBtn.Image = null;
            this._UserManagerBtn.Name = "_UserManagerBtn";
            this._UserManagerBtn.Size = new System.Drawing.Size(152, 22);
            this._UserManagerBtn.Text = "用户管理";
            // 
            // _HelpToolStripMenuItem
            // 
            this._HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._AboutBtn});
            this._HelpToolStripMenuItem.Name = "_HelpToolStripMenuItem";
            this._HelpToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this._HelpToolStripMenuItem.Text = "帮助(&H)";
            // 
            // _AboutBtn
            // 
            this._AboutBtn.EnableBoundle = true;
            this._AboutBtn.Image = ((System.Drawing.Image)(resources.GetObject("_AboutBtn.Image")));
            this._AboutBtn.Name = "_AboutBtn";
            this._AboutBtn.Size = new System.Drawing.Size(125, 22);
            this._AboutBtn.Text = "关于(&A)...";
            // 
            // _StatusStrip
            // 
            this._StatusStrip.Location = new System.Drawing.Point(0, 399);
            this._StatusStrip.Name = "_StatusStrip";
            this._StatusStrip.Size = new System.Drawing.Size(632, 22);
            this._StatusStrip.TabIndex = 3;
            // 
            // _StripContainer
            // 
            // 
            // _StripContainer.ContentPanel
            // 
            this._StripContainer.ContentPanel.Controls.Add(this._StatusStrip);
            this._StripContainer.ContentPanel.Size = new System.Drawing.Size(632, 421);
            this._StripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._StripContainer.Location = new System.Drawing.Point(0, 0);
            this._StripContainer.Name = "_StripContainer";
            this._StripContainer.Size = new System.Drawing.Size(632, 445);
            this._StripContainer.TabIndex = 5;
            this._StripContainer.Text = "toolStripContainer1";
            // 
            // _StripContainer.TopToolStripPanel
            // 
            this._StripContainer.TopToolStripPanel.Controls.Add(this._MenuStrip);
            // 
            // 角色管理ToolStripMenuItem
            // 
            this.角色管理ToolStripMenuItem.Name = "角色管理ToolStripMenuItem";
            this.角色管理ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.角色管理ToolStripMenuItem.Text = "角色管理";
            // 
            // Workbench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 445);
            this.Controls.Add(this._StripContainer);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuStrip;
            this.Name = "Workbench";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReporterMainForm";
            this._MenuStrip.ResumeLayout(false);
            this._MenuStrip.PerformLayout();
            this._StripContainer.ContentPanel.ResumeLayout(false);
            this._StripContainer.ContentPanel.PerformLayout();
            this._StripContainer.TopToolStripPanel.ResumeLayout(false);
            this._StripContainer.TopToolStripPanel.PerformLayout();
            this._StripContainer.ResumeLayout(false);
            this._StripContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuStrip;
        private System.Windows.Forms.StatusStrip _StatusStrip;

        private System.Windows.Forms.ToolStripContainer _StripContainer;
        private System.Windows.Forms.ToolStripMenuItem _FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _ReportsStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _HelpToolStripMenuItem;

        private BoundleMenuStripButton _RefrashBtn;
        private BoundleMenuStripButton _ExportAllBtn;
        private BoundleMenuStripButton _ExitBtn;
        private BoundleMenuStripButton _TodayBtn;
        private BoundleMenuStripButton _AboutBtn;
        private BoundleMenuStripButton _ExportCurrentBtn;

        private System.Windows.Forms.ToolStripSeparator _Separator0;
        private System.Windows.Forms.ToolStripSeparator _Separator1;
        private System.Windows.Forms.ToolStripSeparator _Separator4;
        private System.Windows.Forms.ToolStripMenuItem 产品DToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 订单SToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 售后ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目JToolStripMenuItem;
        private BoundleMenuStripButton _UserManagerBtn;
        private System.Windows.Forms.ToolStripMenuItem 标准产品ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目产品管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 项目管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 客户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成销售订单ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 售后电话记录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 角色管理ToolStripMenuItem;

    }
}