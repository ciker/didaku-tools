﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jeelu.Exceptions;

namespace Jeelu.Electronics.Exceptions
{
    public class ResistanceCollectionException : JeeluException
    {
        protected ResistanceCollectionException(string message) :
            base(message)
        {
        }

        public static ResistanceCollectionException ForNull()
        {
            return new ResistanceCollectionException("电阻集合为空.");
        }

    }
}
