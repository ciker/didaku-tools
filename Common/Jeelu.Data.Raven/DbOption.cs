using System;
using System.Xml;
using Didaku.CoderSetting;

namespace Didaku.Data.Raven
{
    /// <summary>数据持久化的相关程序员配置
    /// </summary>
    public class DbOption : XmlCoderSetting
    {
        public string DataDirectory { get; private set; }

        #region Overrides of CoderSetting<XmlElement>

        /// <summary>从指定的源(一般是一个XmlElement，也可以是任何类型)载入本类型的各个属性值
        /// </summary>
        /// <param name="source">指定的源(一般是一个XmlElement)</param>
        protected override void Load(XmlElement source)
        {
            var dataDir = source.SelectSingleNode("DataDirectory");
            if (dataDir != null) 
                DataDirectory = dataDir.InnerText;
        }

        #endregion

        #region 单件实例

        private DbOption()
        {
        }

        private static readonly Lazy<DbOption> _Instance = new Lazy<DbOption>(() => new DbOption());

        /// <summary> 获得一个本类型的单件实例.
        /// </summary>
        /// <value>The instance.</value>
        public static DbOption Instance
        {
            get { return _Instance.Value; }
        }

        #endregion
    }
}