using Didaku.Data.Interface;
using Didaku.Interface;
using Raven.Client;
using Raven.Client.Linq;

namespace Didaku.Data.Raven
{
    public interface IRavenDbStore<T, D> : IDbStore<T, D>
    {
        IRavenQueryable<T> Query { get; }
    }
}