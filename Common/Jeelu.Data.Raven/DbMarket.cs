﻿using System;
using Raven.Client;
using Raven.Client.Embedded;

namespace Didaku.Data.Raven
{
    public abstract class DbMarket
    {
        private EmbeddableDocumentStore _DocumentStore;

        protected virtual void Initialize()
        {
            _DocumentStore = new EmbeddableDocumentStore
            {
                DataDirectory = GetDataDirectory(DbOption.Instance.DataDirectory)
            };
            _DocumentStore.Initialize();
        }

        /// <summary>给出指定的数据存储路径
        /// </summary>
        /// <param name="rootDir">根路径，可不使用</param>
        /// <returns></returns>
        public abstract string GetDataDirectory(string rootDir);

        private IDocumentSession _Session;
        private ushort _Count = 1;

        internal IDocumentSession GetDocumentSession()
        {
//            if (_Session == null || _Count >= 30)
//            {
                if (_Session != null)
                    _Session.Dispose();
                _Session = _DocumentStore.OpenSession();
//                _Count = 1;
//            }
//            else
//            {
//                _Count++;
//            }
            return _Session;
        }
    }
}