﻿using System;

namespace Jeelu.SerialManager.Base
{
    /// <summary>上位机向下位机发送的双向数据，没有收到回复
    /// </summary>
    public class NoResponseEventArgs : EventArgs
    {
        private readonly int _SenderID;

        /// <summary>消息Id
        /// </summary>
        /// <param name="senderID"></param>
        public NoResponseEventArgs(int senderID)
        {
            _SenderID = senderID;
        }


        public int SenderID
        {
            get { return _SenderID; }
        }
    }
}