﻿using System;

namespace Jeelu.SerialManager.Base
{
    /// <summary>双向数据包发送后，收到回复的事件参数
    /// </summary>
    public class SendRecvEventArgs : EventArgs
    {
        private readonly byte[] _RecvData;
        private readonly int _SenderID;

        public SendRecvEventArgs(int senderID, byte[] recv, int count)
        {
            _SenderID = senderID;
            _RecvData = new byte[count];
            Array.Copy(recv, _RecvData, count);
        }

        public int SenderID
        {
            get { return _SenderID; }
        }

        /// <summary>接收到的数据
        /// </summary>
        public byte[] RecvData
        {
            get { return _RecvData; }
        }
    }
}