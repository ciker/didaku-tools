﻿using System;

namespace Jeelu.SerialManager.Base
{
    public class SerialProDataReceivedEventArgs : EventArgs
    {
        public SerialProDataReceivedEventArgs(byte[] data)
        {
            Data = data;
        }

        public byte[] Data { get; set; }
    }
}