﻿using System;
using System.Threading;
using NLog;
using Jeelu.SerialManager.Base;

namespace Jeelu.SerialManager
{
    /// <summary>演示串口通讯框架
    /// </summary>
    internal class Demo
    {
        private static readonly Logger _Logger = LogManager.GetCurrentClassLogger();

        /// <summary>轮循递增计数
        /// </summary>
        private static ushort _QueryCount = 1;

        private static ISerialCommunicationManager _Manager;

        private static void Main(string[] args)
        {
#if DEBUG
            _Manager = new SerialCommunicationManager(SerialType.DotNet, true);
            Console.WriteLine("开始具体测试...");
            while (true)
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("---------------------------------------------------------");
                Console.WriteLine("\"X\":退出测试");
                Console.WriteLine("\"0\":开始TwoWayPackage简单测试");
                Console.WriteLine("\"1\":开始呼叫器轮询测试");
                Console.WriteLine("---------------------------------------------------------");
                Console.WriteLine();
                char key = Console.ReadKey().KeyChar;
                switch (key)
                {
                    case '0':
                        Console.WriteLine();
                        Console.WriteLine("开始TwoWayPackage简单测试...");
                        Console.WriteLine();
                        SimpleTwoWayPackageTestMock();
                        break;
                    case '1':
                        Console.WriteLine();
                        Console.WriteLine("开始呼叫器轮询测试...");
                        Console.WriteLine();
                        OperatorTestMock();
                        break;
                    case 'x':
                    case 'X':
                        Console.WriteLine();
                        Console.WriteLine("退出测试");
                        Environment.Exit(0);
                        break;
                }
            }
#endif
        }

        /// <summary>呼叫器模拟测试
        /// </summary>
        private static void OperatorTestMock()
        {
            const ushort port = 3;
            _Manager.AddPort(port);
            var interval = new SendInterval
                               {
                                   PreSendInterval = new TimeSpan(0, 0, 0, 0, 5),
                                   AfterSendInterval = new TimeSpan(0, 0, 0, 0, 60),
                               };
            Thread.Sleep(500);
            for (int i = 0; i < 10000; i++)
            {
                var countByte = (byte) GetQueryCount();
                var queryCmd = new byte[] {0xA0, 0x03, 0x01, 0x03, countByte, 0x00, 0xFF};
                int chkByte = 0;
                for (int j = 1; j < queryCmd.Length - 2; j++)
                {
                    chkByte = chkByte + queryCmd[j];
                }
                queryCmd[queryCmd.Length - 2] = (byte) chkByte;
                var package = new QueryPackage(1, queryCmd, interval);
                package.PackageSent += OperatorTestMockPackageSent;
                _Manager.AddPackage(port, package);
            }
        }

        private static void OperatorTestMockPackageSent(object sender, PackageSentEventArgs e)
        {
            if (e.Replied)
            {
                _Logger.Warn(string.Format("轮询收到：{0}", e.ReceivedData.ToHex()));
            }
        }

        private static void SimpleTwoWayPackageTestMock()
        {
            _Manager.AddPort(1);
            var interval = new SendInterval
                               {
                                   PreSendInterval = new TimeSpan(0, 0, 0, 0, 5),
                                   AfterSendInterval = new TimeSpan(0, 0, 0, 0, 60),
                               };
            var queryCmd = new byte[] {0xA0, 0x03, 0x01, 0x03, 0x00, 0x07, 0xFF};
            var package = new TwoWayPackage(1, queryCmd, interval, 3);
            package.PackageSent += SimpleTwoWayPackageMockPackageSent;
            for (int i = 0; i < 3; i++)
            {
                _Manager.AddPackage(1, package);
            }
        }

        private static void SimpleTwoWayPackageMockPackageSent(object sender, PackageSentEventArgs e)
        {
            _Logger.Warn(string.Format("轮询状态:{0},数据:{1}", e.Replied, e.ReceivedData.ToHex()));
        }

        #region Helper

        /// <summary>递增轮循计数
        /// </summary>
        /// <returns></returns>
        private static ushort GetQueryCount()
        {
            if (_QueryCount > 99)
                _QueryCount = 1;
            else
                _QueryCount++;
            return _QueryCount;
        }

        #endregion
    }
}