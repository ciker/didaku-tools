﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Jeelu.SerialManager")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Jeelu.com")]
[assembly: AssemblyProduct("Jeelu.SerialManager")]
[assembly: AssemblyCopyright("Copyright © Jeelu.com 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("47CA82C7-97AC-4617-BE25-ADA544B0F20E")]
[assembly: AssemblyVersion("2013.1.*")]
[assembly: AssemblyFileVersion("2013.1")]
