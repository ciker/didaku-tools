using System;
using System.Collections.Generic;
using System.Data;
using Jeelu.Data.Interface;

namespace Jeelu.Data.SQLite.Common.Process
{
    public class DateTimeTableToArray : IDataReaderProcess<DateTime[]>
    {
        #region Implementation of IDataReaderProcess<DateTime[]>

        public DateTime[] Process(IDataReader dataReader)
        {
            var values = new List<DateTime>();
            while (dataReader.Read())
            {
                DateTime dt = dataReader.GetDateTime(0); //.GetValues(values);
                values.Add(dt);
            }
            return values.ToArray();
        }

        #endregion
    }
}