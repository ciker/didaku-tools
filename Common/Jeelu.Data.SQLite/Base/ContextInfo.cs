using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Jeelu;
using Jeelu.Data.SQLite.Common;

namespace Jeelu.Data.SQLite.Base
{
    public abstract class ContextInfo<T> : IContext
    {
        protected Dictionary<string, IDbDataParameter> DbParameters { get; private set; }
        protected Dictionary<string, PropertyInfo> PropertyInfoMap { get; private set; }
        private string _InsertSql;
        private string _UpdateSql;

        protected ContextInfo()
        {
            DbParameters = new Dictionary<string, IDbDataParameter>();
            PropertyInfoMap = new Dictionary<string, PropertyInfo>();
            var dbDataParameters = new Dictionary<string, IDbDataParameter>();
            var propertyInfoMap = new Dictionary<string, PropertyInfo>();
            ContextHelper.Fill(typeof(T), ref dbDataParameters, ref propertyInfoMap);
            DbParameters = dbDataParameters;
            PropertyInfoMap = propertyInfoMap;
        }

        #region Implementation of IContext

        public abstract string TableName { get; }

        public virtual string InsertSql
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_InsertSql))
                    _InsertSql = JoinInsertSql(TableName, DbParameters.Keys);
                return _InsertSql;
            }
        }

        public virtual string RemoveSql
        {
            get { return "DELETE FROM " + TableName + " WHERE " + TableKey + " = '{0}'"; }
        }

        public virtual string UpdateSql
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_UpdateSql))
                    _UpdateSql = JoinUpdateSql(TableName, DbParameters.Keys);
                return _UpdateSql;
            }
        }

        /// <summary>表中的主键名
        /// </summary>
        protected abstract string TableKey { get; }

        /// <summary>建表语句中的主键附近的子语句
        /// </summary>
        protected abstract string TableKeySql { get; }

        public virtual void Initialize()
        {
            string hasTableSql = string.Format(Database.HAS_TABLE, TableName);
            object result = Database.Instance.ExeScalar(hasTableSql, CommandType.Text);
            bool hasTable = false;
            if (result != null)
                hasTable = UtilityConvert.BoolParse(result.ToString());
            if (!hasTable)
            {
                const string field = "[{0}]";
                var fieldSb = new StringBuilder();
                foreach (IDbDataParameter parameter in DbParameters.Values)
                {
                    if (parameter.ParameterName == TableKey)
                        continue;
                    fieldSb.AppendFormat(field, parameter.ParameterName).Append(',');
                }
                string creatTableSql = string.Format(Database.CREAT_TABLE, TableName, TableKeySql, fieldSb.ToString().TrimEnd(','));
                Database.Instance.ExeNonQuery(creatTableSql);
            }
        }

        #endregion

        /// <summary>组装Update语句
        /// </summary>
        protected string JoinUpdateSql(string tablename, IEnumerable<string> names)
        {
            var paramSb = new StringBuilder();// (string.Format("@{0},", TableKey));
            foreach (string name in names)
            {
                if (name.Equals(TableKey))
                    continue;
                paramSb.Append(name).Append("=@").Append(name).Append(",");
            }
            return string.Format(Database.UPDATE, tablename, paramSb.ToString().TrimEnd(','), TableKey + "='{0}'");
        }

        /// <summary>组装Insert语句
        /// </summary>
        protected string JoinInsertSql(string tablename, IEnumerable<string> names)
        {
            var paramSb = new StringBuilder(string.Format("@{0},", TableKey));
            foreach (string name in names)
            {
                if (name.Equals(TableKey))
                    continue;
                paramSb.Append("@").Append(name).Append(',');
            }
            return string.Format(Database.INSERT, tablename, paramSb.ToString().TrimEnd(','));
        }

        /// <summary>获得指定数据的数据库参数
        /// </summary>
        /// <returns></returns>
        public virtual IList<IDbDataParameter> GetParameters(T obj)
        {
            foreach (IDbDataParameter dbParameter in DbParameters.Values)
            {
                object value = PropertyInfoMap[dbParameter.ParameterName].GetValue(obj, BindingFlags.GetProperty, null, null, null);
                dbParameter.Value = value;
            }
            return DbParameters.Values.ToList();
        }

    }
}