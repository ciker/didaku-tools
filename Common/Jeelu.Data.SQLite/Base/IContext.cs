namespace Jeelu.Data.SQLite.Base
{
    public interface IContext
    {
        string TableName { get; }
        string InsertSql { get; }
        string RemoveSql { get; }
        string UpdateSql { get; }
        void Initialize();
    }
}