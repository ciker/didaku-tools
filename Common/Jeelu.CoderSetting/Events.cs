﻿using System;

namespace Jeelu.CoderSetting
{
    /// <summary>
    /// 选项值发生改变时的包含事件数据的类
    /// </summary>
    public class CoderSettingChangeEventArgs : EventArgs
    {
        public CoderSettingChangeEventArgs(string key, object value)
        {
            CoderSettingValueName = key;
            CoderSettingValue = value;
        }

        public string CoderSettingValueName { get; private set; }

        public Object CoderSettingValue { get; private set; }
    }

    /// <summary>
    /// CoderSetting（程序员配置）值发生改变时的包含事件数据的类
    /// </summary>
    public class CoderSettingLoadedEventArgs : EventArgs
    {
        public CoderSettingLoadedEventArgs(ICoderSetting optionObject, object source)
        {
            CoderSettingObject = optionObject;
            CoderSettingValue = source;
        }

        public ICoderSetting CoderSettingObject { get; private set; }

        public object CoderSettingValue { get; private set; }
    }

    public delegate void CoderSettingChangedEventHandler(object sender, CoderSettingChangeEventArgs e);

    public delegate void CoderSettingChangingEventHandler(object sender, CoderSettingChangeEventArgs e);

    public delegate void CoderSettingLoadedEventHandler(object sender, CoderSettingLoadedEventArgs e);
}