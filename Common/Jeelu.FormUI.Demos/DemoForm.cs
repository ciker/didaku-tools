﻿using System;
using System.Windows.Forms;
using Jeelu.FormUI.DockWindow.Views;

namespace Jeelu.FormUI.Demos
{
    public partial class DemoForm : Form
    {
        private Workbench _Workbench;
        public DemoForm()
        {
            InitializeComponent();
        }

        private void CallWorkBeanch(object sender, EventArgs e)
        {
            _Workbench = new Workbench();
            _Workbench.Shown += delegate
            {
                var loginForm = new LoginForm();
                loginForm.Show();
            };
            _Workbench.Show();
            _Workbench.Refresh();
        }
    }
}