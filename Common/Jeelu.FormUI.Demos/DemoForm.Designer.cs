﻿namespace Jeelu.FormUI.Demos
{
    partial class DemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._CallWorkBeanchButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _CallWorkBeanchButton
            // 
            this._CallWorkBeanchButton.Location = new System.Drawing.Point(12, 12);
            this._CallWorkBeanchButton.Name = "_CallWorkBeanchButton";
            this._CallWorkBeanchButton.Size = new System.Drawing.Size(123, 39);
            this._CallWorkBeanchButton.TabIndex = 0;
            this._CallWorkBeanchButton.Text = "主窗体";
            this._CallWorkBeanchButton.UseVisualStyleBackColor = true;
            this._CallWorkBeanchButton.Click += new System.EventHandler(this.CallWorkBeanch);
            // 
            // DemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(147, 488);
            this.Controls.Add(this._CallWorkBeanchButton);
            this.Location = new System.Drawing.Point(12, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(120, 140);
            this.Name = "DemoForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Demo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _CallWorkBeanchButton;

    }
}