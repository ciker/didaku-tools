﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jeelu.Exceptions
{
    /// <summary>
    /// 类型转换异常
    /// </summary>
    [Serializable]
    public class TypeConvertingException : JeeluException
    {
        public TypeConvertingException(string exceptionMsg, Exception e)
            : base(exceptionMsg, e)
        {

        }
    }
}
