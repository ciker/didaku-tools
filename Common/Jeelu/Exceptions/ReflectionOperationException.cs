﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jeelu.Exceptions
{
    public class ReflectionOperationException : JeeluException
    {
        public ReflectionOperationException(string message, Exception e)
            : base(message, e)
        {
        }

        public ReflectionOperationException(Exception e)
            : base("反射操作异常", e)
        {
        }
    }
}
