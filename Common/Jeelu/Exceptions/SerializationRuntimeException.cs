﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jeelu.Exceptions
{
    /// <summary>当序列化的运行时异常
    /// </summary>
    public class SerializationRuntimeException : JeeluException
    {
        public SerializationRuntimeException(string message, Exception e)
            :base(message, e)
        {
        }
    }
}
