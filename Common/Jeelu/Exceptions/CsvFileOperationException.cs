﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jeelu.Exceptions
{
    /// <summary>CSV文件操作异常
    /// </summary>
    public class CsvFileOperationException : JeeluException
    {
        public CsvFileOperationException(string message, Exception e)
            : base(message, e)
        {
        }
        public CsvFileOperationException(Exception e)
            : base("CSV文件操作异常", e)
        {
        }
    }
}
