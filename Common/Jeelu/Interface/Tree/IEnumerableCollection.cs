using System.Collections;
using System.Collections.Generic;

namespace Jeelu.Interface.Tree
{
    public interface IEnumerableCollection<T> : IEnumerable<T>, ICollection
    {
        bool Contains(T item);
    }
}