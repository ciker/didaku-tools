namespace Jeelu.Interface.Tree
{
    public interface IEnumerableCollectionPair<T>
    {
        IEnumerableCollection<INode<T>> Nodes { get; }
        IEnumerableCollection<T> Values { get; }
    }
}