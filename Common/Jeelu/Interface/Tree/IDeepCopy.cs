namespace Jeelu.Interface.Tree
{
    public interface IDeepCopy
    {
        object CreateDeepCopy();
    }
}