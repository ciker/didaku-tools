﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Jeelu.Interface
{
    public interface IToXmlElement
    {
        XmlElement ToXml(XmlDocument doc);

        void Parse(XmlElement element);
    }
}
