using System;

namespace Jeelu.Collections.Tree
{
    public class NodeTreeDataEventArgs<T> : EventArgs
    {
        private readonly T _Data = default(T);

        public T Data { get { return _Data; } }

        public NodeTreeDataEventArgs(T data)
        {
            _Data = data;
        }
    }
}