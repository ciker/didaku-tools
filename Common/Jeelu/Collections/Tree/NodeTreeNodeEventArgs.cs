using System;
using Jeelu.Interface.Tree;

namespace Jeelu.Collections.Tree
{
    public class NodeTreeNodeEventArgs<T> : EventArgs
    {
        private readonly INode<T> _Node = null;

        public INode<T> Node { get { return _Node; } }

        public NodeTreeNodeEventArgs(INode<T> node)
        {
            _Node = node;
        }
    }
}