using System;
using Jeelu.Interface.Tree;

namespace Jeelu.Collections.Tree
{
    public class NodeTreeInsertEventArgs<T> : EventArgs
    {
        private readonly NodeTreeInsertOperation _Operation;
        public NodeTreeInsertOperation Operation { get { return _Operation; } }

        private readonly INode<T> _Node = null;
        public INode<T> Node { get { return _Node; } }

        public NodeTreeInsertEventArgs(NodeTreeInsertOperation operation, INode<T> node)
        {
            _Operation = operation;
            _Node = node;
        }
    }
}