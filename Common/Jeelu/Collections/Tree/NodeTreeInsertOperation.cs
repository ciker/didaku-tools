namespace Jeelu.Collections.Tree
{
    public enum NodeTreeInsertOperation
    {
        Previous,
        Next,
        Child,
        Tree
    }
}