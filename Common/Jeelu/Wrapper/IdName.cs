﻿using System;

namespace Jeelu.Wrapper
{
    /// <summary>很多场合，虽然都是用string进行索引或描述一个对象，但有时候同时希望将该对象的名称(简要描述)传递时，可以使用这个对象
    /// </summary>
    public struct IdName
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != typeof (IdName)) return false;
            return Equals((IdName) obj);
        }

        public bool Equals(IdName other)
        {
            return other.Id.Equals(Id) && Equals(other.Name, Name);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode()*397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}
