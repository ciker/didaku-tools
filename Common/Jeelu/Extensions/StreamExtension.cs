﻿namespace System.IO
{
    public static class StreamExtension
    {
        public static byte[] ToBytes(this Stream input)
        {
            int capacity = input.CanSeek ? (int) input.Length : 0;
            using (var output = new MemoryStream(capacity))
            {
                int readLength;
                var buffer = new byte[1024];
                do
                {
                    readLength = input.Read(buffer, 0, buffer.Length);
                    output.Write(buffer, 0, readLength);
                } while (readLength != 0);

                return output.ToArray();
            }
        }
    }
}