﻿namespace System
{
	public enum RoundTo
	{
		Second, Minute, Hour, Day
	}
}