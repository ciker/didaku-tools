﻿using System.Data;

namespace Jeelu.Data.Interface
{
    public interface IDataSetProcess<out T>
    {
        T Process(DataSet data);
    }
}
