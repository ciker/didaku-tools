﻿using System.Data;

namespace Jeelu.Data.Interface
{
    public interface IDataReaderProcess<T>
    {
        T Process(IDataReader data);
    }
}
