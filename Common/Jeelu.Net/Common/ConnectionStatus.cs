namespace Jeelu.Net.Common
{
    /// <summary>
    /// 网络连接状态
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal,
        /// <summary>
        /// 断开
        /// </summary>
        Break
    }
}