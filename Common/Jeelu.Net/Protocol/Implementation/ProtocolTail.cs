﻿using System.Text;
using Jeelu.Net.Interfaces;

namespace Jeelu.Net.Protocol.Implementation
{
    public class ProtocolTail : IProtocolTail
    {
        #region IProtocolTail Members

        public byte[] Tail
        {
            get { return Encoding.UTF8.GetBytes("ö"); }
        }

        #endregion
    }
}