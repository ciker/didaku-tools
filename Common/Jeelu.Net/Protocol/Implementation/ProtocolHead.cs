﻿using Jeelu.Net.Interfaces;

namespace Jeelu.Net.Protocol.Implementation
{
    public class ProtocolHead : IProtocolHead
    {
        public byte[] Head
        {
            get { return new byte[] { }; }
        }
    }
}
