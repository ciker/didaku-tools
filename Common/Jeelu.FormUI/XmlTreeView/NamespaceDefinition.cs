﻿using System.ComponentModel;

namespace Gean.Gui.WinForm.XmlTreeView
{
    public class NamespaceDefinition : INotifyPropertyChanged
    {
        private string _Namespace;
        private string _Prefix;

        public string Namespace
        {
            get { return _Namespace; }
            set
            {
                _Namespace = value;
                RaisePropertyChanged("Namespace");
            }
        }

        public string Prefix
        {
            get { return _Prefix; }
            set
            {
                _Prefix = value;
                RaisePropertyChanged("Prefix");
            }
        }

        #region INotifyPropertyChanged values

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
