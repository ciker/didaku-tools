using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using Gean.Gui.WinForm.ExceptionUI;

namespace Gean.Gui.WinForm.XmlTreeView
{
    public class XPathNavigatorTreeView : TreeView, INotifyPropertyChanged
    {
        #region Variables

        /// <summary>
        /// A debug flag used to troubleshoot the rendering of TreeNode text.
        /// </summary>
        private readonly bool _DisplayCharacterRangeBounds;

        /// <summary>
        /// Default StringFormat used for measuring and rendering TreeNode text.
        /// </summary>
        private readonly StringFormat _StringFormat;

        private bool _CanValidate;

        private int _DefaultNamespaceCount;

        private List<Error> _Errors;
        private FileInfo _FileInfo;
        private bool _IsLoading = true;
        private string _LastXPath;
        private TimeSpan _LoadTime;
        private DateTime _LoadingStarted;
        private List<NamespaceDefinition> _NamespaceDefinitions;

        /// <summary>
        /// The XPathNavigator that represents the root of the tree.
        /// </summary>
        private XPathNavigator _Navigator;

        private XPathNodeIterator _NodeIterator;
        private Uri _Uri;

        /// <summary>
        /// A flag to bypass the custom drawing of syntax highlights,
        /// optionally used to improve performance on large documents.
        /// </summary>
        private bool _UseSyntaxHighlighting = true;

        private XmlNamespaceManager _XmlNamespaceManager;
        private XPathNodeIterator _XpathResultsIterator;

        #endregion

        #region Events

        public event EventHandler LoadingFinished;

        #endregion

        #region Constructors

        public XPathNavigatorTreeView()
            : this(false)
        {
        }

        /// <summary>
        /// Initializes a new XPathNavigatorTreeView.
        /// </summary>
        public XPathNavigatorTreeView(bool displayCharacterRangeBounds)
        {
            _DisplayCharacterRangeBounds = displayCharacterRangeBounds;
            // to implement syntax highlighting, we'll be owner drawing the node text
            DrawMode = TreeViewDrawMode.OwnerDrawText;

            // this is required for sequentially rendering adjacent text, see the article
            // 'Why text appears different when drawn with GDIPlus versus GDI', section 'How to Display Adjacent Text'
            // http://support.microsoft.com/?id=307208 for more details
            _StringFormat = new StringFormat(StringFormat.GenericTypographic);
            _StringFormat.FormatFlags = _StringFormat.FormatFlags | StringFormatFlags.FitBlackBox | StringFormatFlags.NoWrap | StringFormatFlags.NoClip;
            _StringFormat.LineAlignment = StringAlignment.Center;

            // I always end up forgetting to turn this off each time I use a TreeView,
            // so I'll just take care of it here automatically.
            base.HideSelection = false;

            CopyNodeOnDrag = true;
            ItemDrag += OnItemDrag;
            AfterLabelEdit += OnAfterLabelEdit;

            AfterExpand += XPathNavigatorTreeView_AfterExpand;
            AfterCollapse += XPathNavigatorTreeView_AfterCollapse;
            DrawNode += XPathNavigatorTreeView_DrawNode;
        }

        #endregion

        #region Properties

        public static String NoSchemaWarning
        {
            get { return Error.NO_SCHEMA_WARNING; }
        }

        /// <summary>
        /// Gets the file, if any, this treeview is currently displaying.
        /// </summary>
        public FileInfo FileInfo
        {
            get { return _FileInfo; }
            private set
            {
                _FileInfo = value;
                RaisePropertyChanged("FileInfo");
            }
        }

        public Uri Uri
        {
            get { return _Uri; }

            set
            {
                _Uri = value;
                RaisePropertyChanged("Uri");
            }
        }

        /// <summary>
        /// Gets the list of nodes, if any, this treeview is currently displaying.
        /// </summary>
        public XPathNodeIterator NodeIterator
        {
            get { return _NodeIterator; }
            private set
            {
                _NodeIterator = value;
                RaisePropertyChanged("NodeIterator");
            }
        }

        /// <summary>
        /// Gets the XPathNavigator, this treeview is currently displaying.
        /// </summary>
        public XPathNavigator Navigator
        {
            get { return _Navigator; }
            private set
            {
                _Navigator = value;
                RaisePropertyChanged("Navigator");
            }
        }

        /// <summary>
        /// Gets or sets whether the custom drawing of syntax highlights
        /// should be bypassed. This can be optionally used to improve 
        /// performance on large documents.
        /// </summary>
        public bool UseSyntaxHighlighting
        {
            get { return _UseSyntaxHighlighting; }

            set
            {
                _UseSyntaxHighlighting = value;
                DrawMode = _UseSyntaxHighlighting ? TreeViewDrawMode.OwnerDrawText : TreeViewDrawMode.Normal;
                RaisePropertyChanged("UseSyntaxHighlighting");
            }
        }

        public XmlNamespaceManager XmlNamespaceManager
        {
            get { return _XmlNamespaceManager; }
            private set
            {
                _XmlNamespaceManager = value;
                RaisePropertyChanged("XmlNamespaceManager");
            }
        }

        public List<NamespaceDefinition> NamespaceDefinitions
        {
            get { return _NamespaceDefinitions; }
            private set
            {
                _NamespaceDefinitions = value;
                RaisePropertyChanged("NamespaceDefinitions");
            }
        }

        public int DefaultNamespaceCount
        {
            get { return _DefaultNamespaceCount; }
            private set
            {
                _DefaultNamespaceCount = value;
                RaisePropertyChanged("DefaultNamespaceCount");
            }
        }

        public List<Error> Errors
        {
            get { return _Errors; }
            private set
            {
                _Errors = value;
                RaisePropertyChanged("Errors");
            }
        }

        public bool IsLoading
        {
            get { return _IsLoading; }
            private set
            {
                _IsLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }

        public TimeSpan LoadTime
        {
            get { return _LoadTime; }
            private set
            {
                _LoadTime = value;
                RaisePropertyChanged("LoadTime");
            }
        }

        /// <summary>
        /// Gets whether the document, if any, specifies schema information and can, therefore, be validated.
        /// </summary>
        public bool CanValidate
        {
            get { return _CanValidate; }
            private set
            {
                _CanValidate = value;
                RaisePropertyChanged("CanValidate");
            }
        }

        public bool CopyNodeOnDrag { get; set; }

        public int CurrentResultPosition { get; private set; }
        public int ResultCount { get; private set; }

        public ChildNodeDefinitionCollection ChildNodeDefinitions { get; set; }

        #endregion

        #region Methods

        public void Reload()
        {
            if (FileInfo != null)
            {
                BeginOpen(FileInfo);
            }
        }

        /// <summary>
        /// Load the specified XML file into the tree.
        /// </summary>
        /// <param name="fileInfo"></param>
        public void BeginOpen(FileInfo fileInfo)
        {
            _LoadingStarted = DateTime.Now;

            IsLoading = true;

            FileInfo = fileInfo;

            ThreadStart start = delegate
            {
                try
                {
                    _Navigator = ReadXPathNavigator(fileInfo);
                    LoadNamespaceDefinitions(_Navigator);
                    Validate(_Navigator);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    AddError(ex.Message);
                }
                finally
                {
                    Invoke(new MethodInvoker(LoadNavigator));
                    OnLoadingFinished(EventArgs.Empty);
                }
            };
            new Thread(start).Start();
        }

        /// <summary>
        /// Loads the specified XML data into the tree.
        /// </summary>
        /// <param name="xml">XML string data to load.</param>
        public void BeginOpen(string xml)
        {
            _LoadingStarted = DateTime.Now;
            IsLoading = true;
            FileInfo = null;

            ThreadStart start = delegate
            {
                try
                {
                    _Navigator = ReadXPathNavigator(xml);

                    LoadNamespaceDefinitions(_Navigator);
                    Validate(_Navigator);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    AddError(ex.Message);
                }
                finally
                {
                    Invoke(new MethodInvoker(LoadNavigator));
                    OnLoadingFinished(EventArgs.Empty);
                }
            };
            new Thread(start).Start();
        }

        /// <summary>
        /// Loads the specified XML data into the tree.
        /// </summary>
        /// <param name="bytes">XML data to load.</param>
        public void BeginOpen(byte[] bytes)
        {
            _LoadingStarted = DateTime.Now;
            IsLoading = true;
            FileInfo = null;

            ThreadStart start = delegate
            {
                try
                {
                    _Navigator = ReadXPathNavigator(bytes);
                    LoadNamespaceDefinitions(_Navigator);
                    Validate(_Navigator);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                    AddError(ex.Message);
                }
                finally
                {
                    Invoke(new MethodInvoker(LoadNavigator));
                    OnLoadingFinished(EventArgs.Empty);
                }
            };
            new Thread(start).Start();
        }

        /// <summary>清除树，并清除与XML相关的属性。
        /// </summary>
        public void Clear()
        {
            FileInfo = null;
            Navigator = null;
            NodeIterator = null;
            BeginUpdate();
            try
            {
                Nodes.Clear();
            }
            finally
            {
                EndUpdate();
            }
        }

        private void LoadNavigator()
        {
            XPathNavigatorTreeNode node = null;

            UseWaitCursor = true;

            // suspend drawing of the tree while loading nodes to improve performance
            // as adding each node would normally require an entire redraw of the tree
            base.BeginUpdate();
            try
            {
                // clear all the nodes in the tree
                base.Nodes.Clear();

                // setting the navigator to null should just clear the tree
                // no exception needs thrown
                if (_Navigator != null)
                {
                    // if it's the root node of the document, load it's children
                    // we don't display the root
                    if (_Navigator.NodeType == XPathNodeType.Root)
                    {
                        LoadNodes(_Navigator.SelectChildren(XPathNodeType.Element), base.Nodes);
                    }
                    else
                    {
                        // otherwise, create a tree node and load it
                        node = new XPathNavigatorTreeNode(_Navigator);
                        base.Nodes.Add(node);
                    }
                }
            }
            finally
            {
                if (node != null)
                {
                    node.Expand();
                }

                // resume drawing of the tree
                base.EndUpdate();
                UseWaitCursor = false;

                IsLoading = false;
                LoadTime = DateTime.Now - _LoadingStarted;
            }
        }

        private static XPathNavigator ReadXPathNavigator(FileInfo fileInfo)
        {
            var readerSettings = new XmlReaderSettings();
            readerSettings.ConformanceLevel = ConformanceLevel.Fragment;

            XPathDocument document = null;

            using (XmlReader reader = XmlReader.Create(fileInfo.FullName, readerSettings))
            {
                document = new XPathDocument(reader);
            }

            XPathNavigator navigator = document.CreateNavigator();
            return navigator;
        }

        private static XPathNavigator ReadXPathNavigator(string xml)
        {
            var readerSettings = new XmlReaderSettings();
            readerSettings.ConformanceLevel = ConformanceLevel.Fragment;

            XPathDocument document = null;

            using (var stringReader = new StringReader(xml))
            {
                using (XmlReader reader = XmlReader.Create(stringReader, readerSettings))
                {
                    document = new XPathDocument(reader);
                }
            }

            XPathNavigator navigator = document.CreateNavigator();
            return navigator;
        }

        private static XPathNavigator ReadXPathNavigator(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                return ReadXPathNavigator(stream);
            }
        }

        private static XPathNavigator ReadXPathNavigator(MemoryStream stream)
        {
            var readerSettings = new XmlReaderSettings();
            readerSettings.ConformanceLevel = ConformanceLevel.Fragment;

            using (XmlReader reader = XmlReader.Create(stream, readerSettings))
            {
                XPathDocument document = null;
                document = new XPathDocument(reader);
                XPathNavigator navigator = document.CreateNavigator();
                return navigator;
            }
        }

        /// <summary>
        /// Loads an XPathNavigatorTreeNode for each XPathNavigator in the specified XPathNodeIterator, into the TreeView.
        /// </summary>
        /// <param name="iterator"></param>
        public void LoadNodes(XPathNodeIterator iterator)
        {
            _LoadingStarted = DateTime.Now;

            UseWaitCursor = true;

            // suspend drawing of the tree while loading nodes to improve performance
            // as adding each node would normally require an entire redraw of the tree
            base.BeginUpdate();
            try
            {
                NodeIterator = iterator;

                LoadNodes(iterator, Nodes);
            }
            finally
            {
                // resume drawing of the tree
                base.EndUpdate();

                UseWaitCursor = false;

                IsLoading = false;
                LoadTime = DateTime.Now - _LoadingStarted;
            }
        }

        /// <summary>
        /// Loads an XPathNavigatorTreeNode for each XPathNavigator in the specified XPathNodeIterator, into the 
        /// specified TreeNodeCollection.
        /// </summary>
        /// <param name="iterator"></param>
        /// <param name="treeNodeCollection"></param>
        private void LoadNodes(XPathNodeIterator iterator, TreeNodeCollection treeNodeCollection)
        {
            // handle null arguments
            if (iterator == null)
                throw new ArgumentNullException("iterator");

            if (treeNodeCollection == null)
                throw new ArgumentNullException("treeNodeCollection");

            treeNodeCollection.Clear();

            var nodes = new List<TreeNode>();

            // create and add a node for each navigator
            foreach (XPathNavigator navigator in iterator)
            {
                bool hasCustomChildNodes = HasCustomChildNodes(navigator, ChildNodeDefinitions);

                var node = new XPathNavigatorTreeNode(navigator.Clone(), hasCustomChildNodes);

                nodes.Add(node);
            }

            treeNodeCollection.AddRange(nodes.ToArray());
        }

        private bool HasCustomChildNodes(XPathNavigator navigator, IEnumerable<ChildNodeDefinition> childNodeDefinitions)
        {
            if (childNodeDefinitions == null)
                return false;

            return childNodeDefinitions.Select(childNodeDefinition => GetCustomChildNodes(navigator, childNodeDefinition)).Any(iterator => iterator != null && iterator.Count > 0);
        }

        private void LoadCustomChildNodes(XPathNavigatorTreeNode node, IEnumerable<ChildNodeDefinition> childNodeDefinitions)
        {
            if (childNodeDefinitions == null)
                return;

            foreach (ChildNodeDefinition childNodeDefinition in childNodeDefinitions)
            {
                XPathNodeIterator iterator = GetCustomChildNodes(node.Navigator, childNodeDefinition);
                if (iterator == null)
                    continue;
                // create and add a node for each navigator
                node.Nodes.AddRange((from XPathNavigator navigator in iterator let hasCustomChildNodes = HasCustomChildNodes(navigator, ChildNodeDefinitions) select new XPathNavigatorTreeNode(navigator.Clone(), hasCustomChildNodes, childNodeDefinition.IdXpath)).Cast<TreeNode>().ToArray());
            }
        }

        private XPathNodeIterator GetCustomChildNodes(XPathNavigator navigator, ChildNodeDefinition childNodeDefinition)
        {
            try
            {
                // get the id of the child node
                var id = navigator.Evaluate("string(" + childNodeDefinition.IdXpath + ")") as string;
                if (string.IsNullOrEmpty(id))
                    return null;
                // get the child xpath
                string xpath = string.Format(childNodeDefinition.ChildXpath, id);
                // get any nodes that match the child xpath 
                XPathNodeIterator iterator = navigator.Select(xpath);

                return iterator;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return null;
            }
        }

        private void LoadNamespaceDefinitions(XPathNavigator navigator)
        {
            if (navigator == null)
            {
                XmlNamespaceManager = null;
                return;
            }

            if (navigator.NameTable != null) 
                XmlNamespaceManager = new XmlNamespaceManager(navigator.NameTable);
            else
                return;

            var namespaces = navigator.Evaluate(@"//namespace::*[not(. = ../../namespace::*)]") as XPathNodeIterator;

            NamespaceDefinitions = new List<NamespaceDefinition>();

            DefaultNamespaceCount = 0;

            // add any namespaces within the scope of the navigator to the namespace manager
            if (namespaces != null)
                foreach (object namespaceElement in namespaces)
                {
                    var namespaceNavigator = namespaceElement as XPathNavigator;
                    if (namespaceNavigator == null)
                        continue;

                    var definition = new NamespaceDefinition
                                         {
                                             Prefix = namespaceNavigator.Name,
                                             Namespace = namespaceNavigator.Value,
                                         };

                    if (string.IsNullOrEmpty(definition.Prefix))
                    {
                        if (DefaultNamespaceCount > 0)
                            definition.Prefix = "default" + (DefaultNamespaceCount + 1).ToString();
                        else
                            definition.Prefix = "default";

                        DefaultNamespaceCount++;
                    }
                    NamespaceDefinitions.Add(definition);
                    XmlNamespaceManager.AddNamespace(definition.Prefix, definition.Namespace);
                }
        }

        /// <summary>
        /// Performs validation against any schemas provided in the document, if any.
        /// If validation is not possible, because no schemas are provided, an InvalidOperationException is thrown.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException" />
        public List<Error> Validate(XPathNavigator navigator, string[] schemaFilenames = null)
        {
            try
            {
                CanValidate = false;

                Errors = new List<Error>();

                if (navigator == null)
                    navigator = Navigator;

                if (navigator == null)
                    return null;

                string xsiPrefix = XmlNamespaceManager.LookupPrefix("http://www.w3.org/2001/XMLSchema-instance");

                var schemas = new XmlSchemaSet();

                if (!string.IsNullOrEmpty(xsiPrefix))
                {
                    /*
					 * I can't believe I have to do this manually, but, here we go...
					 * 
					 * When loading a document with an XmlReader and performing validation, it will automatically
					 * detect schemas specified in the document with @xsi:schemaLocation and @xsi:noNamespaceSchemaLocation attributes.
					 * We get line number and column, but not a reference to the actual offending xml node or xpath navigator.
					 * 
					 * When validating an xpath navigator, it ignores these attributes, doesn't give us line number or column,
					 * but does give us the offending xpath navigator.
					 * 
					 * */
                    foreach (object schemaAttribute in navigator.Select(
                        string.Format("//@{0}:noNamespaceSchemaLocation", xsiPrefix),
                        XmlNamespaceManager))
                    {
                        var attributeNavigator = schemaAttribute as XPathNavigator;
                        if (attributeNavigator == null)
                            continue;

                        string value = attributeNavigator.Value;
                        value = ResolveSchemaFileName(value);

                        // add the schema
                        schemas.Add(null, value);
                    }
                    foreach (object schemaAttribute in navigator.Select(
                        string.Format("//@{0}:schemaLocation", xsiPrefix),
                        XmlNamespaceManager))
                    {
                        var attributeNavigator = schemaAttribute as XPathNavigator;
                        if (attributeNavigator == null)
                            continue;

                        string value = attributeNavigator.Value;

                        IEnumerable<KeyValuePair<string, string>> namespaceDefs = ParseNamespaceDefinitions(value);

                        foreach (var pair in namespaceDefs)
                            schemas.Add(pair.Key, pair.Value);
                    }
                }

                if (schemaFilenames != null && schemaFilenames.Length > 0)
                {
                    // add any manually specified schema files
                    foreach (string schemaFilename in schemaFilenames)
                    {
                        schemas.Add(null, ResolveSchemaFileName(schemaFilename));
                    }
                }

                if (schemas.Count < 1)
                {
                    AddError(Error.NO_SCHEMA_WARNING, XmlSeverityType.Warning);
                    return Errors;
                }

                // validate the document
                navigator.CheckValidity(schemas, OnValidationEvent);

                CanValidate = true;
            }
            catch (FileNotFoundException ex)
            {
                string message = string.Format(
                    "Cannot find the schema document at '{0}'", ex.FileName);
                AddError(message);
            }
            catch (Exception ex)
            {
                AddError(ex.Message);
            }

            return Errors;
        }

        private IEnumerable<KeyValuePair<string, string>> ParseNamespaceDefinitions(string value)
        {
            var pairs = new List<KeyValuePair<string, string>>();

            // the value should be like: http://www.w3schools.com note.xsd
            // namespace path
            // split it on space
            string[] values = value.Split(" ".ToCharArray());

            for (int i = 0; i < values.Length; i += 2)
            {
                // first value is namespace (it's not supposed to have spaces in it)
                string targetNamespace = values[i];

                // second value should be the filename
                string schemaFileName = null;
                if (i < values.Length - 1)
                {
                    schemaFileName = values[i + 1];
                    schemaFileName = ResolveSchemaFileName(schemaFileName);
                }

                var pair = new KeyValuePair<string, string>(targetNamespace, schemaFileName);

                pairs.Add(pair);
            }

            return pairs;
        }

        private string ResolveSchemaFileName(string schemaFileName)
        {
            if (!File.Exists(schemaFileName))
            {
                // try prepending the current file's path
                if (FileInfo != null)
                {
                    string filename = Path.Combine(FileInfo.DirectoryName, schemaFileName);
                    if (File.Exists(filename))
                        return filename;
                }
            }
            return schemaFileName;
        }

        /// <summary>
        /// Finds and selects an XPathNavigatorTreeNode for the given XPathNavigator.
        /// </summary>
        /// <param name="navigator">An XPathNavigator to find and select in the tree.</param>
        /// <returns></returns>
        public bool SelectXmlTreeNode(XPathNavigator navigator)
        {
            if (navigator == null)
                throw new ArgumentNullException("navigator");

            // we've found a node, so build an ancestor stack
            Stack<XPathNavigator> ancestors = GetAncestors(navigator);

            // now find treenodes to match the ancestors
            TreeNode treeNode = GetXmlTreeNode(ancestors);

            if (treeNode == null)
                return false;

            // select the node
            SelectedNode = treeNode;

            return true;
        }

        /// <summary>
        /// Finds a TreeNode for a given stack of XPathNavigators.
        /// </summary>
        /// <param name="ancestors">A Stack of XPathNavigators with which to find a TreeNode.</param>
        /// <returns></returns>
        private TreeNode GetXmlTreeNode(Stack<XPathNavigator> ancestors)
        {
            XPathNavigator navigator = null;

            // start at the root
            TreeNodeCollection nodes = Nodes;

            TreeNode treeNode = null;

            // loop through the ancestor XPathNavigators
            while (ancestors.Count > 0 && (navigator = ancestors.Pop()) != null)
            {
                // loop through the TreeNodes at the current level
                foreach (TreeNode node in nodes)
                {
                    // make sure it's an XPathNavigatorTreeNode
                    var xmlTreeNode = node as XPathNavigatorTreeNode;
                    if (xmlTreeNode == null)
                        continue;

                    // check to see if we've found the correct TreeNode
                    if (xmlTreeNode.Navigator.IsSamePosition(navigator))
                    {
                        // expand the tree node, if it hasn't alreay been expanded
                        if (!node.IsExpanded)
                            node.Expand();

                        // we've taken another step towards the target node
                        treeNode = node;

                        // update the current level
                        nodes = node.Nodes;

                        // handle the next level, if any
                        break;
                    }
                }
            }

            // return the result, if any was found
            return treeNode;
        }

        /// <summary>
        /// Builds and returns a Stack of XPathNavigator ancestors for a given XPathNavigator.
        /// </summary>
        /// <param name="navigator">The XPathNavigator from which to build a stack.</param>
        /// <returns></returns>
        private Stack<XPathNavigator> GetAncestors(XPathNavigator navigator)
        {
            if (navigator == null)
                throw new ArgumentNullException("navigator");

            var ancestors = new Stack<XPathNavigator>();

            // navigate up the xml tree, building the stack as we go
            while (navigator != null)
            {
                // push the current ancestor onto the stack
                ancestors.Push(navigator);

                // clone the current navigator cursor, so we don't lose our place
                navigator = navigator.Clone();

                // if we've reached the top, we're done
                if (!navigator.MoveToParent())
                    break;

                // if we've reached the root, we're done
                if (navigator.NodeType == XPathNodeType.Root)
                    break;
            }

            // return the result
            return ancestors;
        }

        /// <summary>
        /// Evaluates the XPath expression and returns the typed result.
        /// </summary>
        /// <param name="xpath">A string representing an XPath expression that can be evaluated.</param>
        /// <returns></returns>
        public object SelectXmlNodes(string xpath)
        {
            // get the selected node
            var node = SelectedNode as XPathNavigatorTreeNode;

            // if there is no selected node, default to the root node
            if (node == null && Nodes.Count > 0)
                node = GetRootXmlTreeNode();

            if (node == null)
                return null;

            // evaluate the expression, return the result
            return node.Navigator.Evaluate(xpath, XmlNamespaceManager);
        }

        /// <summary>
        /// Returns the root XPathNavigatorTreeNode in the tree.
        /// </summary>
        /// <returns></returns>
        private XPathNavigatorTreeNode GetRootXmlTreeNode()
        {
            foreach (TreeNode node in Nodes)
            {
                var xmlTreeNode = node as XPathNavigatorTreeNode;

                if (xmlTreeNode == null || xmlTreeNode.Navigator == null)
                    continue;

                if (xmlTreeNode.Navigator.NodeType != XPathNodeType.Element)
                    continue;

                return xmlTreeNode;
            }

            return Nodes[0] as XPathNavigatorTreeNode;
        }

        /// <summary>
        /// Finds and selects an XPathNavigatorTreeNode using an XPath expression.
        /// </summary>
        /// <param name="xpath">An XPath expression.</param>
        /// <returns></returns>
        public bool FindByXpath(string xpath)
        {
            return FindByXpath(xpath, Direction.Down);
        }

        public bool FindByXpath(string xpath, Direction direction)
        {
            if (CurrentResultPosition < 2 && direction == Direction.Up)
                throw new InvalidOperationException(Error.CANNOT_MOVE_UP_ERROR);

            object result = null;

            // no need to re-evaluate the expression results if the xpath hasn't changed since last time, and they're not searching up
            if (xpath != _LastXPath || direction == Direction.Up || _XpathResultsIterator == null)
            {
                // have to re-evaluate the expression, so the iterator is reset at -1
                result = SelectXmlNodes(xpath);

                if (xpath != _LastXPath || _XpathResultsIterator == null)
                {
                    CurrentResultPosition = 0;
                    ResultCount = 0;

                    _LastXPath = xpath;
                }

                if (result == null)
                    return false;

                // did the expression evaluate to a node set?
                _XpathResultsIterator = result as XPathNodeIterator;
            }

            if (_XpathResultsIterator != null)
            {
                // the expression evaluated to a node set
                ResultCount = _XpathResultsIterator.Count;

                if (ResultCount < 1)
                    return false;

                if (direction == Direction.Up)
                {
                    CurrentResultPosition--;

                    for (int i = 0; i < CurrentResultPosition; i++)
                    {
                        if (!_XpathResultsIterator.MoveNext())
                            break;
                    }
                }
                else
                {
                    if (_XpathResultsIterator.MoveNext())
                        CurrentResultPosition++;
                }

                // select the first node in the set
                return SelectXmlTreeNode(_XpathResultsIterator.Current);
            }
            else
            {
                // the expression evaluated to something else, most likely a count()
                // show the result in a new window
                var dialog = new ExpressionResultsWindow();
                dialog.Expression = xpath;
                dialog.Result = result.ToString();
                dialog.ShowDialog(FindForm());
                return true;
            }
        }

        /// <summary>
        /// Returns a string representing the full path of an XPathNavigator.
        /// </summary>
        /// <param name="navigator">An XPathNavigator.</param>
        /// <returns></returns>
        public string GetXmlNodeFullPath(XPathNavigator navigator)
        {
            if (navigator == null)
                return null;

            // create a StringBuilder for assembling the path
            var sb = new StringBuilder();

            // clone the navigator (cursor), so the node doesn't lose it's place
            navigator = navigator.Clone();

            // traverse the navigator's ancestry all the way to the top
            while (navigator != null)
            {
                // skip anything but elements
                if (navigator.NodeType == XPathNodeType.Element)
                {
                    // insert the node and a seperator
                    sb.Insert(0, navigator.Name);
                    sb.Insert(0, "/");
                }
                if (!navigator.MoveToParent())
                    break;
            }

            return sb.ToString();
        }

        public List<KeyValuePair<string, string>> GetAttributeXPaths(XPathNavigator navigator)
        {
            XPathNavigator n = navigator.Clone();
            if (!n.HasAttributes)
                return null;

            string xpath = GetXmlNodeFullPath(navigator);
            var pairs = new List<KeyValuePair<string, string>>();

            // first attribute
            n.MoveToFirstAttribute();
            pairs.Add(FormatNameAndXPath(xpath, n.Name, n.Value));

            // subsequent attributes
            while (n.MoveToNextAttribute())
                pairs.Add(FormatNameAndXPath(xpath, n.Name, n.Value));

            return pairs;
        }

        public KeyValuePair<string, string> FormatNameAndXPath(string nodeXPath, string name, string value)
        {
            string displayName = "@" + name;
            string attributeValue = string.IsNullOrWhiteSpace(value) ? "value" : value;
            string xpath = string.Format("{0}[@{1}='{2}']", nodeXPath, name, attributeValue);

            // key=@name, value=/a/b/c[@name='value']
            return new KeyValuePair<string, string>(displayName, xpath);
        }

        private void OnValidationEvent(object sender, ValidationEventArgs e)
        {
            var error = new Error();
            error.Description = e.Message;
            error.Category = e.Severity;

            var exception = e.Exception as XmlSchemaValidationException;
            if (exception != null)
            {
                error.SourceObject = exception.SourceObject;
            }

            AddError(error);
        }

        public void AddError(string description)
        {
            AddError(description, XmlSeverityType.Error);
        }

        private void AddError(string description, XmlSeverityType category)
        {
            var error = new Error();
            error.Description = description;
            error.Category = category;

            AddError(error);
        }

        private void AddError(Error error)
        {
            if (FileInfo == null)
                error.File = Error.UNTITLED;
            else
                error.File = FileInfo.FullName;

            if (Errors == null)
                Errors = new List<Error>();

            Errors.Add(error);

            error.DefaultOrder = Errors.Count;
        }

        /// <summary>
        /// Copies the current selected xml node (and all of it's sub nodes) to the clipboard
        /// as formatted XML text.
        /// </summary>
        public void CopyFormattedOuterXml()
        {
            var selected = SelectedNode as XPathNavigatorTreeNode;

            if (selected == null)
                return;

            string text = GetXmlTreeNodeFormattedOuterXml(selected);

            if (!string.IsNullOrEmpty(text))
                Clipboard.SetText(text);
        }

        /// <summary>
        /// Copies the current selected xml node to the clipboard as XML text.
        /// </summary>
        public void CopyNodeText()
        {
            var selected = SelectedNode as XPathNavigatorTreeNode;

            if (selected == null)
                return;

            string text = selected.Text;

            if (!string.IsNullOrEmpty(text))
                Clipboard.SetText(text);
        }

        public string GetNodeTextBase64Decoded()
        {
            var selected = SelectedNode as XPathNavigatorTreeNode;
            if (selected != null)
            {
                byte[] bytes = Convert.FromBase64String(selected.Text);
                if (bytes != null)
                    return Encoding.UTF8.GetString(bytes);
            }
            return string.Empty;
        }

        public string GetNodeValueBase64Decoded()
        {
            var selected = SelectedNode as XPathNavigatorTreeNode;
            if (selected != null)
            {
                byte[] bytes = Convert.FromBase64String(selected.Navigator.Value);
                if (bytes != null)
                    return Encoding.UTF8.GetString(bytes);
            }
            return string.Empty;
        }

        /// <summary>
        /// Returns the selected xml node (and all of it's sub nodes) as formatted XML text.
        /// </summary>
        public string GetXmlTreeNodeFormattedOuterXml(XPathNavigatorTreeNode node)
        {
            return GetXPathNavigatorFormattedOuterXml(node.Navigator);
        }

        public string GetXPathNavigatorFormattedOuterXml(XPathNavigator navigator)
        {
            var stringBuilder = new StringBuilder();

            var settings = new XmlWriterSettings();

            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;

            using (XmlWriter writer = XmlWriter.Create(stringBuilder, settings))
            {
                navigator.WriteSubtree(writer);

                writer.Flush();

                return stringBuilder.ToString();
            }
        }

        public new void ExpandAll()
        {
            TreeNode selected = SelectedNode;

            if (selected == null)
            {
                if (Nodes.Count > 0)
                    selected = Nodes[0];
                else
                    return;
            }

            try
            {
                BeginUpdate();

                selected.ExpandAll();
            }
            finally
            {
                EndUpdate();
            }
        }

        public new void CollapseAll()
        {
            TreeNode selected = SelectedNode;

            if (selected == null)
            {
                if (Nodes.Count > 0)
                    selected = Nodes[0];
                else
                    return;
            }

            try
            {
                BeginUpdate();

                CollapseAll(selected);
            }
            finally
            {
                EndUpdate();
            }
        }

        private void CollapseAll(TreeNode treeNode)
        {
            if (treeNode == null)
                return;

            if (treeNode.Nodes != null)
            {
                foreach (TreeNode childNode in treeNode.Nodes)
                {
                    CollapseAll(childNode);
                }
            }

            if (treeNode.IsExpanded)
                treeNode.Collapse();
        }

        /// <summary>
        /// Overwrites the tab page's file with XML formatting (tabs and crlf's)
        /// </summary>
        public void Save(bool formatting)
        {
            if (FileInfo == null)
            {
                using (var dialog = new SaveFileDialog())
                {
                    dialog.Filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*";
                    if (dialog.ShowDialog(this) != DialogResult.OK)
                        return;

                    FileInfo = new FileInfo(dialog.FileName);
                }
            }

            SaveAs(FileInfo.FullName, formatting);
        }

        /// <summary>
        /// Prompts the user to save a copy of the tab page's XML file.
        /// </summary>
        public void SaveAs(bool formatting)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*";
                if (FileInfo != null)
                    dialog.FileName = Path.GetFileName(FileInfo.FullName);
                if (dialog.ShowDialog(this) != DialogResult.OK)
                    return;

                FileInfo = new FileInfo(dialog.FileName);
            }

            SaveAs(FileInfo.FullName, formatting);
        }

        /// <summary>
        /// Saves a copy of the tab page's XML file to the specified path.
        /// </summary>
        public void SaveAs(string filename, bool formatting)
        {
            using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Save(stream, formatting);
            }
        }

        private void Save(Stream stream, bool formatting)
        {
            var settings = new XmlWriterSettings();

            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.Encoding = Encoding.UTF8;
            settings.OmitXmlDeclaration = false;
            settings.Indent = formatting;

            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                if (Navigator != null)
                {
                    Navigator.WriteSubtree(writer);
                }
                else if (NodeIterator != null)
                {
                    foreach (XPathNavigator node in NodeIterator)
                    {
                        switch (node.NodeType)
                        {
                            case XPathNodeType.Attribute:
                                writer.WriteString(node.Value);
                                writer.WriteWhitespace(Environment.NewLine);
                                break;

                            default:
                                node.WriteSubtree(writer);
                                if (node.NodeType == XPathNodeType.Text)
                                    writer.WriteWhitespace(Environment.NewLine);
                                break;
                        }
                    }
                }

                writer.Flush();
            }
        }

        public virtual void OnLoadingFinished(EventArgs e)
        {
            IsLoading = false;

            if (LoadingFinished != null)
                LoadingFinished(this, e);
        }

        private bool DrawStrings(string text, Rectangle bounds, Graphics graphics)
        {
            bool drawn = false;

            // draw delimiters
            Color color = Color.FromArgb(0, 0, 255);
            drawn = DrawStrings(text, bounds, graphics, color, RegularExpressions.Xml, "Delimiter");

            // draw attribute values
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Xml, "AttributeValue");

            // draw text
            color = SystemColors.ControlText;
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Xml, "Text");

            // draw name
            color = Color.FromArgb(163, 21, 21);
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Xml, "Name");

            // draw attribute names
            color = Color.FromArgb(255, 0, 0);
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Xml, "AttributeName");

            // draw comment delimiters
            color = Color.FromArgb(0, 0, 255);
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Comments, "Delimiter");

            // draw comments
            color = Color.FromArgb(0, 128, 0);
            drawn |= DrawStrings(text, bounds, graphics, color, RegularExpressions.Comments, "Comments");

            return drawn;
        }

        private bool DrawStrings(string text, Rectangle bounds, Graphics graphics, Color color, Regex regex, string groupName)
        {
            if (!regex.IsMatch(text))
                return false;

            MatchCollection matches = regex.Matches(text);

            bool result = false;

            foreach (Match match in matches)
            {
                using (var brush = new SolidBrush(color))
                {
                    foreach (Capture capture in match.Groups[groupName].Captures)
                    {
                        // create a character range for the capture
                        var characterRanges = new[]
                                                  {
                                                      new CharacterRange(capture.Index, capture.Length)
                                                  };

                        // create a new string format, using the default one as a prototype
                        var stringFormat = new StringFormat(_StringFormat);

                        stringFormat.SetMeasurableCharacterRanges(characterRanges);

                        // measure the character ranges for the capture, getting an array of regions
                        var regions = new Region[1];
                        regions = graphics.MeasureCharacterRanges(text, Font, bounds, stringFormat);

                        // Draw each measured string within each region.
                        foreach (Region region in regions)
                        {
                            RectangleF rectangle = region.GetBounds(graphics);

                            graphics.DrawString(capture.Value, Font, brush, rectangle, _StringFormat);

                            // draw character range bounding rectangles, for troubleshooting only
                            if (_DisplayCharacterRangeBounds)
                                using (var pen = new Pen(brush.Color))
                                    graphics.DrawRectangle(pen, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);

                            result = true;
                        }
                    }
                }
            }

            return result;
        }

        private void HandleException(Exception exception)
        {
            try
            {
                Debug.WriteLine(exception);
                ExceptionDialog.ShowDialog(this, exception);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        #endregion

        #region Event Handlers

        private void OnItemDrag(object sender, ItemDragEventArgs e)
        {
            try
            {
                if (!CopyNodeOnDrag)
                    return;

                string text = string.Empty;

                var node = e.Item as TreeNode;

                if (node == null)
                    return;

                text = node.Text;

                var xmlTreeNode = node as XPathNavigatorTreeNode;
                if (xmlTreeNode != null)
                    text = GetXmlTreeNodeFormattedOuterXml(xmlTreeNode);

                if (!string.IsNullOrEmpty(text))
                    DoDragDrop(text, DragDropEffects.Copy);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                ExceptionDialog.ShowDialog(this, ex);
            }
        }

        private void OnAfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            e.CancelEdit = true;
        }

        private void XPathNavigatorTreeView_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            try
            {
                //// remove the end tag we inserted when the node was expanded
                //TreeNode node = e.Node.Tag as TreeNode;
                //if (node != null)
                //{
                //   // remove it
                //   TreeNodeCollection nodes = base.Nodes;
                //   if (node.Parent != null)
                //   {
                //      nodes = node.Parent.Nodes;
                //   }
                //   nodes.Remove(node);
                //}
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void XPathNavigatorTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            try
            {
                using (var provider = new WaitCursorProvider())
                {
                    var node = e.Node as XPathNavigatorTreeNode;

                    if (node == null)
                        return;

                    // for better performance opening large files, tree nodes are loaded on demand.
                    if (!node.HasExpanded)
                    {
                        // make sure we don't have to load this tree node again.
                        node.HasExpanded = true;

                        // load the child nodes of the specified xml tree node
                        LoadNodes(node.Navigator.SelectChildren(XPathNodeType.All), node.Nodes);

                        // load any custom child node definitions
                        LoadCustomChildNodes(node, ChildNodeDefinitions);
                    }
                }

                // // add the end tag node for the currently expanding node
                //if (node.Nodes.Count > 0)
                //{
                //   TreeNodeCollection nodes = base.Nodes;
                //   if (node.Parent != null)
                //   {
                //      nodes = node.Parent.Nodes;
                //   }

                //   var endTagNode = node.Tag as TreeNode;
                //   if (endTagNode == null)
                //   {
                //      endTagNode = new TreeNode(string.Format("</{0}>", node.Navigator.Name));
                //      node.Tag = endTagNode;
                //   }

                //   if (!nodes.Contains(endTagNode))
                //      nodes.Insert(e.Node.Index + 1, endTagNode);
                //}
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void XPathNavigatorTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            try
            {
                if (!_UseSyntaxHighlighting)
                {
                    e.DrawDefault = true;
                    return;
                }

                if (e.Node.IsEditing)
                {
                    e.DrawDefault = true;
                    return;
                }

                if ((e.State & TreeNodeStates.Selected) == TreeNodeStates.Selected)
                {
                    e.DrawDefault = true;
                    return;
                }

                // not sure why the TreeView doesn't take care of this...
                // I was getting requests to draw nodes that weren't even visible on the screen.
                // Obviously, this was drastically slowing down the drawing of the tree!
                if (!e.Node.IsVisible)
                    return;

                Rectangle bounds = e.Bounds;

                // I tried using the suggested TextRenderingHint.AntiAlias, but I don't think it looks right with
                // small fonts, so I'm disabling it.  Need to make this a user option in a later version.

                // this is required for sequentially rendering adjacent text, see the article
                // 'Why text appears different when drawn with GDIPlus versus GDI', section 'How to Display Adjacent Text'
                // http://support.microsoft.com/?id=307208 for more details
                // e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                string text = e.Node.Text;

                // the node isn't selected, draw it's text with syntax highlights
                bool drawn = false;

                // don't bother with syntax highlighting for text nodes
                var xpathNode = e.Node as XPathNavigatorTreeNode;
                if (xpathNode == null || xpathNode.Navigator.NodeType != XPathNodeType.Text)
                {
                    using (var brush = new SolidBrush(BackColor))
                        e.Graphics.FillRectangle(brush, bounds);

                    // draw non text nodes with syntax highlights
                    drawn = DrawStrings(text, bounds, e.Graphics);
                }

                // if the text wasn't matched by any of our regular expressions, it's likely just a text node
                // draw it without any syntax highlights
                if (!drawn)
                {
                    e.DrawDefault = true;
                    return;
                }

                //// If the node has focus, draw the focus rectangle.
                //if ((e.State & TreeNodeStates.Focused) != 0)
                //   ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region INotifyPropertyChanged values

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}