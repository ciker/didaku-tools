﻿using System.Xml.Schema;

namespace Gean.Gui.WinForm.XmlTreeView
{
    public class Error
    {
        public const string CANNOT_MOVE_UP_ERROR = "Cannot move up past the first result.	";
        public const string NO_SCHEMA_WARNING = "The document does not have a schema specified.  Double click this message to manually assign a schema.	";
        public const string SCHEMA_FILE_NOT_FOUND_ERROR = "	Cannot find the schema document at '{0}'.  Double click this message to manually assign a schema.	Make sure to leave the '{0}', as this will get replaced with the missing file name at run-time.";
        public const string UNTITLED = "	Untitled	The title used for new, unsaved files.";

        public XmlSeverityType Category { get; set; }
        public int DefaultOrder { get; set; }
        public string Description { get; set; }
        public string File { get; set; }
        public object SourceObject { get; set; }
    }
}
