﻿namespace Gean.Gui.WinForm.XmlTreeView
{
    public class ChildNodeDefinition
    {
        private string _ChildXpath = "//*[@id='{0}']";
        private string _IDXpath = "@id";

        public string IdXpath
        {
            get { return _IDXpath; }
            set { _IDXpath = value; }
        }

        public string ChildXpath
        {
            get { return _ChildXpath; }
            set { _ChildXpath = value; }
        }

        public override string ToString()
        {
            return _IDXpath;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            string values = _IDXpath + "|" + _ChildXpath;
            return values.GetHashCode();
        }
    }
}