﻿using System;
using System.Windows.Forms;

namespace Gean.Gui.WinForm.XmlTreeView
{
    public class WaitCursorProvider : IDisposable
    {
        private readonly Cursor _PreviousCursor;

        private bool _Disposed;

        public WaitCursorProvider()
        {
            _PreviousCursor = Cursor.Current;

            Cursor.Current = Cursors.WaitCursor;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        #endregion

        private void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                    Cursor.Current = _PreviousCursor;
                }

                // Note disposing has been done.
                _Disposed = true;
            }
        }

        ~WaitCursorProvider()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
    }
}