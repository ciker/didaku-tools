using System;
using System.Collections;
using System.Collections.Generic;

namespace Jeelu.Math
{
    public class Permutations<T> : IEnumerable<IList<T>>
    {
        private readonly List<IList<T>> _Permutations;
        private readonly int _Length;
        private readonly List<int[]> _Indices;
        private int[] _Value;
        private int _Level = -1;

        public Permutations(IList<T> items)
            : this(items, items.Count)
        {
        }

        public Permutations(IList<T> items, int length)
        {
            _Length = length;
            _Permutations = new List<IList<T>>();
            _Indices = new List<int[]>();
            BuildIndices();
            foreach (var oneCom in new Combinations<T>(items, length))
            {
                _Permutations.AddRange(GetPermutations(oneCom));
            }
        }

        private void BuildIndices()
        {
            _Value = new int[_Length];
            Visit(0);
        }

        private void Visit(int k)
        {
            _Level += 1;
            _Value[k] = _Level;

            if (_Level == _Length)
            {
                _Indices.Add(_Value);
                var newValue = new int[_Length];
                Array.Copy(_Value, newValue, _Length);
                _Value = newValue;
            }
            else
            {
                for (int i = 0; i < _Length; i++)
                {
                    if (_Value[i] == 0)
                    {
                        Visit(i);
                    }
                }
            }

            _Level -= 1;
            _Value[k] = 0;
        }

        private IEnumerable<IList<T>> GetPermutations(IList<T> oneCom)
        {
            var t = new List<IList<T>>();

            foreach (var idxs in _Indices)
            {
                var onePerm = new T[_Length];
                for (int i = 0; i < _Length; i++)
                {
                    onePerm[i] = oneCom[idxs[i] - 1];
                }
                t.Add(onePerm);
            }

            return t;
        }

        private int GetFactorial(int n)
        {
            int result = 1;
            while (n > 1)
            {
                result *= n;
                n--;
            }
            return result;
        }

        public IEnumerator<IList<T>> GetEnumerator()
        {
            return _Permutations.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Permutations.GetEnumerator();
        }

        public int Count
        {
            get { return _Permutations.Count; }
        }
    }
}
