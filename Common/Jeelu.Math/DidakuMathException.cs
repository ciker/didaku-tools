﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jeelu.Exceptions;

namespace Jeelu.Math
{
    public class JeeluMathException : JeeluException
    {
        public JeeluMathException(string message):base(message)
        {
            
        }
    }
}
