﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gean.Net.Protocol;

namespace Pansoft.CQMS.QueueInfoScreen.Protocol
{
    public class ReCall : AbstractProtocol 
    {
        public ReCall()
            : base("PanBranchLcd", "ReCall")
        {
        }
    }
}
