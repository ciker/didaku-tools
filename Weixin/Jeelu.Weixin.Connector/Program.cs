﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Jeelu.Weixin.Connector
{
    public class Program
    {
        private static string _UserId;
        private static void Main(string[] args)
        {
            if (string.IsNullOrWhiteSpace(_UserId))
            {
                _UserId = Guid.NewGuid().ToString();
            }
            while (true)
            {
                Console.WriteLine("请输入模拟微信内容：");
                Console.WriteLine("按Q退出。按W切换新用户。");
                string info = Console.ReadLine();
                if (string.IsNullOrEmpty(info))
                {
                    continue;
                }
                if (info.ToLower().Equals("q"))
                {
                    break;
                }
                if(info.ToLower().Equals("w"))
                {
                    _UserId = Guid.NewGuid().ToString();
                    continue;
                }
                string data = GetWeiXin(info);
                Console.WriteLine(data);
                SendWeiXin(data);
                Console.WriteLine();
            }
        }

        private static void SendWeiXin(string weixin)
        {
            const string url = "http://localhost:8080/weixinconnector/run";
            byte[] w = Encoding.UTF8.GetBytes(weixin);
            var client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            client.Headers.Add("ContentLength", w.Length.ToString());
            byte[] responseData = client.UploadData(url, "POST", w);
            var result = Encoding.UTF8.GetString(responseData);
            Console.WriteLine("--回复------------");
            Console.WriteLine(result);
            Console.WriteLine("------------------");
        } 

        private static string GetWeiXin(string content)
        {
            var writer = new StringWriter();
            using (var xw = new XmlTextWriter(writer))
            {
                xw.Formatting = Formatting.Indented;
                //xw.WriteStartDocument();
                xw.WriteStartElement("xml");
                {
                    xw.WriteStartElement("ToUserName");
                    xw.WriteCData(Guid.NewGuid().ToString());
                    xw.WriteEndElement();
                }
                {
                    xw.WriteStartElement("FromUserName");
                    xw.WriteCData(_UserId);
                    xw.WriteEndElement();
                }
                {
                    xw.WriteStartElement("CreateTime");
                    xw.WriteString(DateTime.Now.Ticks.ToString());
                    xw.WriteEndElement();
                }
                {
                    xw.WriteStartElement("MsgType");
                    xw.WriteCData("text");
                    xw.WriteEndElement();
                }
                {
                    xw.WriteStartElement("Content");
                    xw.WriteCData(content);
                    xw.WriteEndElement();
                }
                {
                    xw.WriteStartElement("MsgId");
                    xw.WriteString(Guid.NewGuid().ToString());
                    xw.WriteEndElement();
                }
                xw.WriteEndElement();
                //xw.WriteEndDocument();
                xw.Flush();
                xw.Close();
            }
            return writer.ToString();
        }
    }
}