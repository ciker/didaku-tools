﻿using System.Windows.Forms;
using NLog;

namespace Didaku.QEngine.Demo
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Trace("=================");
            logger.Trace("排队队列引擎测试程序");
            logger.Trace("=================");

            Application.Run(new DemoMainForm());
        }
    }
}