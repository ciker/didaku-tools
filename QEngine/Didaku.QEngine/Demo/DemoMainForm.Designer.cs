﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Didaku.QEngine.Demo
{
    partial class DemoMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoMainForm));
            this._OpenEngineButton = new System.Windows.Forms.Button();
            this._TestTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._CounterCmbox = new System.Windows.Forms.ComboBox();
            this._NumberTbox = new System.Windows.Forms.TextBox();
            this._提示Btn = new System.Windows.Forms.Button();
            this._评价Btn = new System.Windows.Forms.Button();
            this._顺呼Btn = new System.Windows.Forms.Button();
            this._弃号Btn = new System.Windows.Forms.Button();
            this._合岗Btn = new System.Windows.Forms.Button();
            this._换岗Btn = new System.Windows.Forms.Button();
            this._查询Btn = new System.Windows.Forms.Button();
            this._转队Btn = new System.Windows.Forms.Button();
            this._转柜Btn = new System.Windows.Forms.Button();
            this._重呼Btn = new System.Windows.Forms.Button();
            this._帮助Btn = new System.Windows.Forms.Button();
            this._延后Btn = new System.Windows.Forms.Button();
            this._插前Btn = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._GenerateTicketsButton = new System.Windows.Forms.Button();
            this._QEngineGroupBox = new System.Windows.Forms.GroupBox();
            this._CloseEngineButton = new System.Windows.Forms.Button();
            this._TicketParamsGroupBox = new System.Windows.Forms.GroupBox();
            this._GenerateTicketsCountComboBox = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._DepartmentTreeView = new System.Windows.Forms.TreeView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._QueueTreeView = new System.Windows.Forms.TreeView();
            this._TestTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._QEngineGroupBox.SuspendLayout();
            this._TicketParamsGroupBox.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // _OpenEngineButton
            // 
            this._OpenEngineButton.Location = new System.Drawing.Point(17, 30);
            this._OpenEngineButton.Name = "_OpenEngineButton";
            this._OpenEngineButton.Size = new System.Drawing.Size(90, 30);
            this._OpenEngineButton.TabIndex = 0;
            this._OpenEngineButton.Text = "打开引擎";
            this._OpenEngineButton.UseVisualStyleBackColor = true;
            this._OpenEngineButton.Click += new System.EventHandler(this.OpenEngineButtonClick);
            // 
            // _TestTabControl
            // 
            this._TestTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TestTabControl.Controls.Add(this.tabPage1);
            this._TestTabControl.Controls.Add(this.tabPage2);
            this._TestTabControl.ItemSize = new System.Drawing.Size(58, 25);
            this._TestTabControl.Location = new System.Drawing.Point(547, 198);
            this._TestTabControl.Name = "_TestTabControl";
            this._TestTabControl.SelectedIndex = 0;
            this._TestTabControl.Size = new System.Drawing.Size(263, 338);
            this._TestTabControl.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this._NumberTbox);
            this.tabPage1.Controls.Add(this._提示Btn);
            this.tabPage1.Controls.Add(this._评价Btn);
            this.tabPage1.Controls.Add(this._顺呼Btn);
            this.tabPage1.Controls.Add(this._弃号Btn);
            this.tabPage1.Controls.Add(this._合岗Btn);
            this.tabPage1.Controls.Add(this._换岗Btn);
            this.tabPage1.Controls.Add(this._查询Btn);
            this.tabPage1.Controls.Add(this._转队Btn);
            this.tabPage1.Controls.Add(this._转柜Btn);
            this.tabPage1.Controls.Add(this._重呼Btn);
            this.tabPage1.Controls.Add(this._帮助Btn);
            this.tabPage1.Controls.Add(this._延后Btn);
            this.tabPage1.Controls.Add(this._插前Btn);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(255, 305);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本功能";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._CounterCmbox);
            this.groupBox1.Location = new System.Drawing.Point(13, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 57);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "当前柜台";
            // 
            // _CounterCmbox
            // 
            this._CounterCmbox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CounterCmbox.FormattingEnabled = true;
            this._CounterCmbox.Location = new System.Drawing.Point(23, 20);
            this._CounterCmbox.Name = "_CounterCmbox";
            this._CounterCmbox.Size = new System.Drawing.Size(184, 27);
            this._CounterCmbox.TabIndex = 0;
            this._CounterCmbox.Text = "1";
            // 
            // _NumberTbox
            // 
            this._NumberTbox.BackColor = System.Drawing.Color.DarkGreen;
            this._NumberTbox.Font = new System.Drawing.Font("Dutch801 XBd BT", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._NumberTbox.ForeColor = System.Drawing.Color.Yellow;
            this._NumberTbox.Location = new System.Drawing.Point(13, 79);
            this._NumberTbox.Name = "_NumberTbox";
            this._NumberTbox.Size = new System.Drawing.Size(226, 42);
            this._NumberTbox.TabIndex = 17;
            this._NumberTbox.Text = "A1234";
            // 
            // _提示Btn
            // 
            this._提示Btn.Enabled = false;
            this._提示Btn.Location = new System.Drawing.Point(129, 248);
            this._提示Btn.Name = "_提示Btn";
            this._提示Btn.Size = new System.Drawing.Size(52, 34);
            this._提示Btn.TabIndex = 12;
            this._提示Btn.Text = "提示";
            this._提示Btn.UseVisualStyleBackColor = true;
            // 
            // _评价Btn
            // 
            this._评价Btn.Enabled = false;
            this._评价Btn.Location = new System.Drawing.Point(13, 248);
            this._评价Btn.Name = "_评价Btn";
            this._评价Btn.Size = new System.Drawing.Size(110, 34);
            this._评价Btn.TabIndex = 11;
            this._评价Btn.Text = "请评价 | 欢迎光临";
            this._评价Btn.UseVisualStyleBackColor = true;
            // 
            // _顺呼Btn
            // 
            this._顺呼Btn.Location = new System.Drawing.Point(187, 208);
            this._顺呼Btn.Name = "_顺呼Btn";
            this._顺呼Btn.Size = new System.Drawing.Size(52, 74);
            this._顺呼Btn.TabIndex = 0;
            this._顺呼Btn.Text = "顺呼\r\n\r\n指定\r\n呼叫";
            this._顺呼Btn.UseVisualStyleBackColor = true;
            this._顺呼Btn.Click += new System.EventHandler(this.顺呼);
            // 
            // _弃号Btn
            // 
            this._弃号Btn.Location = new System.Drawing.Point(129, 208);
            this._弃号Btn.Name = "_弃号Btn";
            this._弃号Btn.Size = new System.Drawing.Size(52, 34);
            this._弃号Btn.TabIndex = 10;
            this._弃号Btn.Text = "弃号";
            this._弃号Btn.UseVisualStyleBackColor = true;
            // 
            // _合岗Btn
            // 
            this._合岗Btn.Location = new System.Drawing.Point(71, 208);
            this._合岗Btn.Name = "_合岗Btn";
            this._合岗Btn.Size = new System.Drawing.Size(52, 34);
            this._合岗Btn.TabIndex = 9;
            this._合岗Btn.Text = "合岗";
            this._合岗Btn.UseVisualStyleBackColor = true;
            // 
            // _换岗Btn
            // 
            this._换岗Btn.Location = new System.Drawing.Point(13, 208);
            this._换岗Btn.Name = "_换岗Btn";
            this._换岗Btn.Size = new System.Drawing.Size(52, 34);
            this._换岗Btn.TabIndex = 8;
            this._换岗Btn.Text = "换岗";
            this._换岗Btn.UseVisualStyleBackColor = true;
            // 
            // _查询Btn
            // 
            this._查询Btn.Location = new System.Drawing.Point(129, 168);
            this._查询Btn.Name = "_查询Btn";
            this._查询Btn.Size = new System.Drawing.Size(52, 34);
            this._查询Btn.TabIndex = 7;
            this._查询Btn.Text = "查询";
            this._查询Btn.UseVisualStyleBackColor = true;
            // 
            // _转队Btn
            // 
            this._转队Btn.Location = new System.Drawing.Point(71, 168);
            this._转队Btn.Name = "_转队Btn";
            this._转队Btn.Size = new System.Drawing.Size(52, 34);
            this._转队Btn.TabIndex = 6;
            this._转队Btn.Text = "转队";
            this._转队Btn.UseVisualStyleBackColor = true;
            // 
            // _转柜Btn
            // 
            this._转柜Btn.Location = new System.Drawing.Point(13, 168);
            this._转柜Btn.Name = "_转柜Btn";
            this._转柜Btn.Size = new System.Drawing.Size(52, 34);
            this._转柜Btn.TabIndex = 5;
            this._转柜Btn.Text = "转柜";
            this._转柜Btn.UseVisualStyleBackColor = true;
            // 
            // _重呼Btn
            // 
            this._重呼Btn.Location = new System.Drawing.Point(187, 128);
            this._重呼Btn.Name = "_重呼Btn";
            this._重呼Btn.Size = new System.Drawing.Size(52, 74);
            this._重呼Btn.TabIndex = 1;
            this._重呼Btn.Text = "重呼\r\n\r\n回呼";
            this._重呼Btn.UseVisualStyleBackColor = true;
            // 
            // _帮助Btn
            // 
            this._帮助Btn.Location = new System.Drawing.Point(129, 128);
            this._帮助Btn.Name = "_帮助Btn";
            this._帮助Btn.Size = new System.Drawing.Size(52, 34);
            this._帮助Btn.TabIndex = 4;
            this._帮助Btn.Text = "帮助";
            this._帮助Btn.UseVisualStyleBackColor = true;
            // 
            // _延后Btn
            // 
            this._延后Btn.Location = new System.Drawing.Point(71, 128);
            this._延后Btn.Name = "_延后Btn";
            this._延后Btn.Size = new System.Drawing.Size(52, 34);
            this._延后Btn.TabIndex = 3;
            this._延后Btn.Text = "延后";
            this._延后Btn.UseVisualStyleBackColor = true;
            // 
            // _插前Btn
            // 
            this._插前Btn.Location = new System.Drawing.Point(13, 128);
            this._插前Btn.Name = "_插前Btn";
            this._插前Btn.Size = new System.Drawing.Size(52, 34);
            this._插前Btn.TabIndex = 2;
            this._插前Btn.Text = "插前";
            this._插前Btn.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(255, 305);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "压力测试";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 552);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(824, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _GenerateTicketsButton
            // 
            this._GenerateTicketsButton.Location = new System.Drawing.Point(133, 30);
            this._GenerateTicketsButton.Name = "_GenerateTicketsButton";
            this._GenerateTicketsButton.Size = new System.Drawing.Size(62, 30);
            this._GenerateTicketsButton.TabIndex = 0;
            this._GenerateTicketsButton.Text = "生成";
            this._GenerateTicketsButton.UseVisualStyleBackColor = true;
            this._GenerateTicketsButton.Click += new System.EventHandler(this.GenerateTicketsButtonClick);
            // 
            // _QEngineGroupBox
            // 
            this._QEngineGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._QEngineGroupBox.Controls.Add(this._CloseEngineButton);
            this._QEngineGroupBox.Controls.Add(this._OpenEngineButton);
            this._QEngineGroupBox.Location = new System.Drawing.Point(547, 12);
            this._QEngineGroupBox.Name = "_QEngineGroupBox";
            this._QEngineGroupBox.Size = new System.Drawing.Size(259, 79);
            this._QEngineGroupBox.TabIndex = 0;
            this._QEngineGroupBox.TabStop = false;
            this._QEngineGroupBox.Text = "引擎操作";
            // 
            // _CloseEngineButton
            // 
            this._CloseEngineButton.Enabled = false;
            this._CloseEngineButton.Location = new System.Drawing.Point(151, 30);
            this._CloseEngineButton.Name = "_CloseEngineButton";
            this._CloseEngineButton.Size = new System.Drawing.Size(90, 30);
            this._CloseEngineButton.TabIndex = 1;
            this._CloseEngineButton.Text = "关闭引擎";
            this._CloseEngineButton.UseVisualStyleBackColor = true;
            // 
            // _TicketParamsGroupBox
            // 
            this._TicketParamsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._TicketParamsGroupBox.Controls.Add(this._GenerateTicketsCountComboBox);
            this._TicketParamsGroupBox.Controls.Add(this._GenerateTicketsButton);
            this._TicketParamsGroupBox.Location = new System.Drawing.Point(547, 108);
            this._TicketParamsGroupBox.Name = "_TicketParamsGroupBox";
            this._TicketParamsGroupBox.Size = new System.Drawing.Size(259, 84);
            this._TicketParamsGroupBox.TabIndex = 1;
            this._TicketParamsGroupBox.TabStop = false;
            this._TicketParamsGroupBox.Text = "号票模拟数量/每队列";
            // 
            // _GenerateTicketsCountComboBox
            // 
            this._GenerateTicketsCountComboBox.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GenerateTicketsCountComboBox.FormattingEnabled = true;
            this._GenerateTicketsCountComboBox.Items.AddRange(new object[] {
            "5",
            "10",
            "20",
            "30",
            "40",
            "50",
            "100",
            "200",
            "300",
            "500",
            "1000"});
            this._GenerateTicketsCountComboBox.Location = new System.Drawing.Point(60, 31);
            this._GenerateTicketsCountComboBox.Name = "_GenerateTicketsCountComboBox";
            this._GenerateTicketsCountComboBox.Size = new System.Drawing.Size(67, 27);
            this._GenerateTicketsCountComboBox.TabIndex = 1;
            this._GenerateTicketsCountComboBox.Text = "20";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(12, 12);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(529, 524);
            this.tabControl2.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._DepartmentTreeView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(521, 498);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Department";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _DepartmentTreeView
            // 
            this._DepartmentTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._DepartmentTreeView.Location = new System.Drawing.Point(3, 3);
            this._DepartmentTreeView.Name = "_DepartmentTreeView";
            this._DepartmentTreeView.Size = new System.Drawing.Size(515, 492);
            this._DepartmentTreeView.TabIndex = 4;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._QueueTreeView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(521, 498);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Transactions";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _QueueTreeView
            // 
            this._QueueTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._QueueTreeView.Location = new System.Drawing.Point(3, 3);
            this._QueueTreeView.Name = "_QueueTreeView";
            this._QueueTreeView.Size = new System.Drawing.Size(515, 492);
            this._QueueTreeView.TabIndex = 4;
            // 
            // DemoMainForm
            // 
            this.ClientSize = new System.Drawing.Size(824, 574);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this._TicketParamsGroupBox);
            this.Controls.Add(this._QEngineGroupBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._TestTabControl);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DemoMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DemoMainForm";
            this._TestTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this._QEngineGroupBox.ResumeLayout(false);
            this._TicketParamsGroupBox.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button _OpenEngineButton;
        private TabControl _TestTabControl;
        private TabPage tabPage1;
        private GroupBox groupBox1;
        private ComboBox _CounterCmbox;
        private TextBox _NumberTbox;
        private Button _提示Btn;
        private Button _评价Btn;
        private Button _顺呼Btn;
        private Button _弃号Btn;
        private Button _合岗Btn;
        private Button _换岗Btn;
        private Button _查询Btn;
        private Button _转队Btn;
        private Button _转柜Btn;
        private Button _重呼Btn;
        private Button _帮助Btn;
        private Button _延后Btn;
        private Button _插前Btn;
        private TabPage tabPage2;
        private StatusStrip statusStrip1;
        private Button _GenerateTicketsButton;
        private GroupBox _QEngineGroupBox;
        private Button _CloseEngineButton;
        private GroupBox _TicketParamsGroupBox;
        private ComboBox _GenerateTicketsCountComboBox;
        private TabControl tabControl2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TreeView _DepartmentTreeView;
        private TreeView _QueueTreeView;
    }
}