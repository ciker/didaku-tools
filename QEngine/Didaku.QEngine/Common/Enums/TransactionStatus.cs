﻿using System;

namespace Didaku.QEngine.Common.Enums
{
    /// <summary>交易状态
    /// </summary>
    [Flags]
    public enum TransactionStatus : byte
    {
        /// <summary>正在等待
        /// </summary>
        OnWait = 0,
        /// <summary>   正在服务
        /// </summary>
        OnServe = 1,
        /// <summary>未服务，
        /// </summary>
        UnServe = 2,
        /// <summary>服务结束
        /// </summary>5
        Finished = 4,
        /// <summary>弃票
        /// </summary>
        GivenUp = 8,
        /// <summary>交易关闭。(含三种状态：未服务；弃票；服务结束)
        /// </summary>
        Closed = UnServe | GivenUp | Finished,
        /// <summary>交易运行中。（含两种状态：正在等待；正在服务）
        /// </summary>
        Running = OnWait | OnServe
    }
}