﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Didaku.QEngine.Common.Counts
{
    public class CountMap : IDictionary<string, Count>
    {
        private readonly ConcurrentDictionary<string, Count> _Map = new ConcurrentDictionary<string, Count>();

        #region Implementation of IEnumerable

        public IEnumerator<KeyValuePair<string, Count>> GetEnumerator()
        {
            return _Map.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<KeyValuePair<string,int>>

        public void Add(KeyValuePair<string, Count> item)
        {
            ((ICollection<KeyValuePair<string, Count>>) _Map).Add(item);
        }

        public void Clear()
        {
            _Map.Clear();
        }

        public bool Contains(KeyValuePair<string, Count> item)
        {
            return ((ICollection<KeyValuePair<string, Count>>) _Map).Contains(item);
        }

        public void CopyTo(KeyValuePair<string, Count>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<string, Count>>) _Map).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<string, Count> item)
        {
            return ((ICollection<KeyValuePair<string, Count>>) _Map).Remove(item);
        }

        public int Count
        {
            get { return _Map.Count; }
        }

        public bool IsReadOnly
        {
            get { return ((ICollection<KeyValuePair<string, Count>>) _Map).IsReadOnly; }
        }

        #endregion

        #region Implementation of IDictionary<string,int>

        public bool ContainsKey(string key)
        {
            if (key.Equals(string.Empty))
                return false;
            return _Map.ContainsKey(key);
        }

        public void Add(string key, Count value)
        {
            if (!key.Equals(string.Empty))
                _Map.TryAdd(key, value);
        }

        public bool Remove(string key)
        {
            if (!key.Equals(string.Empty))
            {
                Count count;
                return _Map.TryRemove(key, out count);
            }
            return false;
        }

        public bool TryGetValue(string key, out Count value)
        {
            if (!key.Equals(string.Empty))
                return _Map.TryGetValue(key, out value);
            value = new Count();
            return false;
        }

        public Count this[string key]
        {
            get
            {
                if (key.Equals(string.Empty))
                    throw new ArgumentNullException("key", "查询的索引值不能为空");
                return _Map[key];
            }
            set
            {
                if (_Map[key] != value)
                {
                    _Map[key] = value;
                    OnCountChanged(new CountChangedEventArgs(key, value));
                }
            }
        }

        public ICollection<string> Keys
        {
            get { return _Map.Keys; }
        }

        public ICollection<Count> Values
        {
            get { return _Map.Values; }
        }

        #endregion

        /// <summary>
        /// 当统计数量发生改变时的事件
        /// </summary>
        public event CountChangedEventHandler CountChangedEvent;

        protected virtual void OnCountChanged(CountChangedEventArgs e)
        {
            if (CountChangedEvent != null)
                CountChangedEvent(this, e);
        }
    }
}