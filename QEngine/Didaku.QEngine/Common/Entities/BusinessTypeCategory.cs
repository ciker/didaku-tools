﻿using System;
using System.Collections.Generic;
using System.Text;
using Didaku.Attributes;
using Didaku.QEngine.Common.Interfaces;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>
    /// 标识一个包括多个业务类型的业务组，它是树状的，树的关系是由它的 ParentId 是否为空来决定的。
    /// </summary>
    public class BusinessTypeCategory : IEquatable<BusinessTypeCategory>, IServiceQueueElement, IQEntity<string>
    {
        public BusinessTypeCategory()
        {
            BusinessTypeIds = new List<string>();
            Ranges = new WorkRangeCollection(0);
        }

        [EntityColumn]
        public string Id { get; set; }

        [EntityColumn]
        public string Name { get; set; }

        [EntityColumn]
        public string Number { get; set; }

        [EntityColumn]
        public int Level { get; set; }

        [EntityColumn]
        public string ParentId { get; set; }

        /// <summary>对应的多个工作时间段
        /// </summary>
        /// <value>
        /// The ranges.
        /// </value>
        [EntityColumn]
        public WorkRangeCollection Ranges { get; set; }

        [EntityColumn]
        public List<string> BusinessTypeIds { get; set; }

        #region IEquatable<WinTypeGroupInfo> Members

        public bool Equals(BusinessTypeCategory other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id) && Equals(other.ParentId, ParentId);
        }

        #endregion

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Name);
            if (BusinessTypeIds!= null && BusinessTypeIds.Count > 0)
            {
                sb.Append(" - ").Append('[');
                foreach (string winTypeId in BusinessTypeIds)
                {
                    sb.Append(winTypeId).Append(',');
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append(']');
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (BusinessTypeCategory)) return false;
            return Equals((BusinessTypeCategory) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Id != null ? Id.GetHashCode() : 0)*397) ^ (ParentId != null ? ParentId.GetHashCode() : 0);
            }
        }

        public static bool operator ==(BusinessTypeCategory left, BusinessTypeCategory right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BusinessTypeCategory left, BusinessTypeCategory right)
        {
            return !Equals(left, right);
        }

        #region Implementation of IServiceQueueElement

        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return Id;
        }

        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        public bool IsActived()
        {
            return true;
        }

        #endregion
    }
}