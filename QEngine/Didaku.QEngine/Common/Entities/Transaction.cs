using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Abstracts.Entities;
using Didaku.QEngine.Common.Enums;
using NLog;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>排队数据管理中最重要的实体对象。描述柜员为客户所做的一次服务，交易。
    /// 2012年2月16日设计。
    /// </summary>
    [Serializable]
    public class Transaction : TransactionInfo, ICloneable, IEquatable<Transaction>
    {
        private static readonly Logger _Logger = LogManager.GetCurrentClassLogger();

        public Transaction()
        {
            Id = Guid.NewGuid().ToString();
            FlowNo = 0;
            CounterIndex = 0;
            BusinessTypeId = string.Empty;
            StaffId = string.Empty;
            TicketNumber = "0000";
            CallHead = "";
            CardNumber = string.Empty;
            CardCategoryKind = string.Empty;
            NumberGenerationType = NumberGenerationType.UnKnown;
            EventSource = "00";
            Details = new SerializableMap<string, string>(0);
        }

        #region Property

        [EntityColumn]
        public string QueueId { get; set; }

        /// <summary> 事件来源（多机互联时用于标记排队机设备编号）
        /// </summary>
        [EntityColumn]
        public string EventSource { get; set; }

        /// <summary> 排队号码分段标记，例如 排队号A0001，分段标记为A
        /// </summary>
        [EntityColumn]
        public string CallHead { get; set; }

        /// <summary> 柜台编号
        /// </summary>
        [EntityColumn]
        public int CounterIndex { get; set; }

        /// <summary> CustomerInfo的键值，取决于客户取号的方式，如果是手工点击按键取号则为空
        /// 刷卡取号则为卡号，刷二代证取号则为身份证号，输入电话号码取号则为电话号码
        /// </summary>
        [EntityColumn]
        public string CardNumber { get; set; }

        /// <summary> 客户分类。当客户校验后表示客户级别。
        /// 通常由客户信息系统提供，可以是客户星级，可以是客户贵宾卡标记
        /// </summary>
        [EntityColumn]
        public string CardCategoryKind { get; set; }

        /// <summary> BusinessType的键值，业务类型编号
        /// </summary>
        [EntityColumn]
        public string BusinessTypeId { get; set; }

        /// <summary> Staff的键值，员工号
        /// </summary>
        [EntityColumn]
        public string StaffId { get; set; }

        /// <summary> 当前工作流节点的进展编号
        /// </summary>
        [EntityColumn]
        public short WorkflowNodeIndex { get; set; }

        /// <summary>工作流
        /// </summary>
        /// <value>
        /// The workflow.
        /// </value>
        [EntityColumn]
        public string WorkflowId { get; set; }

        /// <summary>上传的状态:0,实时上传；1，补传，取号；2，补传，叫号；99，补传，完成
        /// </summary>
        [EntityColumn]
        public ushort UploadStatusIndex { get; set; }

        #endregion

        #region ICloneable Members

        object ICloneable.Clone()
        {
            var tran = new Transaction
                       {
                           Id = Id,
                           FlowNo = FlowNo,
                           AskEvalTime = AskEvalTime,
                           CallTime = CallTime,
                           GetNumberTime = GetNumberTime,
                           TransactionEndTime = TransactionEndTime,
                           CounterIndex = CounterIndex,
                           CardNumber = CardNumber,
                           CardCategoryKind = CardCategoryKind,
                           BusinessTypeId = BusinessTypeId,
                           WorkflowId = WorkflowId,
                           WorkflowNodeIndex = WorkflowNodeIndex,
                           StaffId = StaffId,
                           TransactionStatus = TransactionStatus,
                           EvaluationType = EvaluationType,
                           TicketNumber = TicketNumber,
                           CallHead = CallHead,
                           Priority = Priority,
                           NumberGenerationType = NumberGenerationType,
                           UploadStatusIndex = UploadStatusIndex,
                           EventSource = EventSource
                       };
            foreach (var detail in Details)
                tran.Details.Add(detail);
            return tran;
        }

        #endregion

        #region IEquatable<Transaction> Members

        public bool Equals(Transaction other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id);
        }

        #endregion

        #region 含事件的属性

        private readonly object _Lock = new object();

        #region

        private DateTime? _AskEvalTime;
        private DateTime? _CallTime;
        private short _EvaluationType;
        private DateTime? _GetNumberTime;
        private DateTime? _TransactionEndTime;
        private TransactionStatus _TransactionStatus;

        #endregion

        [EntityColumn]
        public short EvaluationType
        {
            get { return _EvaluationType; }
            set
            {
                if (_EvaluationType != value)
                {
                    _EvaluationType = value;
                    OnshortChanged();
                }
            }
        }

        /// <summary>获取或设置交易的状态。已加锁。
        /// </summary>
        [EntityColumn]
        public TransactionStatus TransactionStatus
        {
            get { return _TransactionStatus; }
            set
            {
                if (_TransactionStatus != value)
                {
                    lock (_Lock)
                        _TransactionStatus = value;
                    OnTransactionStatusChanged();
                }
            }
        }

        [EntityColumn]
        public DateTime? AskEvalTime
        {
            get { return _AskEvalTime; }
            set
            {
                if (_AskEvalTime != value)
                {
                    _AskEvalTime = value;
                    OnAskEvalTimeChanged();
                }
            }
        }

        [EntityColumn]
        public DateTime? GetNumberTime
        {
            get { return _GetNumberTime; }
            set
            {
                if (_GetNumberTime != value)
                {
                    _GetNumberTime = value;
                    OnGetNumberTimeChanged();
                }
            }
        }

        [EntityColumn]
        public DateTime? TransactionEndTime
        {
            get { return _TransactionEndTime; }
            set
            {
                if (_TransactionEndTime != value)
                {
                    _TransactionEndTime = value;
                    OnTransactionEndTimeChanged();
                }
            }
        }

        [EntityColumn]
        public DateTime? CallTime
        {
            get { return _CallTime; }
            set
            {
                if (_CallTime != value)
                {
                    _CallTime = value;
                    OnCallTimeChanged();
                }
            }
        }

        #region 事件

        public event EventHandler ShortChanged;

        protected virtual void OnshortChanged()
        {
            if (ShortChanged != null)
                ShortChanged(this, EventArgs.Empty);
        }

        public event EventHandler DataStatusChanged;

        protected virtual void OnDataStatusChanged()
        {
            if (DataStatusChanged != null)
                DataStatusChanged(this, EventArgs.Empty);
        }

        public event EventHandler TransactionStatusChanged;

        protected virtual void OnTransactionStatusChanged()
        {
            if (TransactionStatusChanged != null)
                TransactionStatusChanged(this, EventArgs.Empty);
        }

        public event EventHandler AskEvalTimeChanged;

        protected virtual void OnAskEvalTimeChanged()
        {
            if (AskEvalTimeChanged != null)
                AskEvalTimeChanged(this, EventArgs.Empty);
        }

        public event EventHandler GetNumberTimeChanged;

        protected virtual void OnGetNumberTimeChanged()
        {
            if (GetNumberTimeChanged != null)
                GetNumberTimeChanged(this, EventArgs.Empty);
        }

        public event EventHandler TransactionEndTimeChanged;

        protected virtual void OnTransactionEndTimeChanged()
        {
            if (TransactionEndTimeChanged != null)
                TransactionEndTimeChanged(this, EventArgs.Empty);
        }

        public event EventHandler CallTimeChanged;

        protected virtual void OnCallTimeChanged()
        {
            if (CallTimeChanged != null)
                CallTimeChanged(this, EventArgs.Empty);
        }

        #endregion

        #endregion

        #region 更改属性的一些方法

        /// <summary>恢复交易的初始状态
        /// </summary>
        public void Reconvert()
        {
            _EvaluationType = -1;
            _TransactionStatus = TransactionStatus.OnWait;

            _GetNumberTime = DateTime.Now;
            _AskEvalTime = null;
            _CallTime = null;
            _TransactionEndTime = null;
        }

        /// <summary>插前, 默认将取号时间向前推移5分钟
        /// </summary>
        public void Forward(ushort minute = 5)
        {
            if (GetNumberTime != null)
            {
                lock (_Lock)
                {
                    DateTime? oldtime = GetNumberTime;
                    _TransactionStatus = TransactionStatus.OnWait;
                    _GetNumberTime = GetNumberTime.Value.AddMinutes(-minute);
                    Details.Add(string.Format("Forward,{0}", Details.Count), string.Format("{0}执行插前.原时间:{1}", DateTime.Now.ToString("HH:mm:ss"), oldtime));
                }
                OnTransactionStatusChanged();
            }
        }

        /// <summary>延后, 默认将取号时间向后推移5分钟
        /// </summary>
        public void Backward(ushort minute = 5)
        {
            if (GetNumberTime != null)
            {
                lock (_Lock)
                {
                    DateTime? oldtime = GetNumberTime;
                    _TransactionStatus = TransactionStatus.OnWait;
                    _GetNumberTime = GetNumberTime.Value.AddMinutes(minute);
                    Details.Add(string.Format("Backward,{0}", Details.Count), string.Format("{0}执行延后.原时间:{1}", DateTime.Now.ToString("HH:mm:ss"), oldtime));
                }
                OnTransactionStatusChanged();
            }
        }

        /// <summary>设置当前流水为“正在服务”的状态，并填充相关信息
        /// </summary>
        /// <param name="counterIndex">服务的柜台</param>
        /// <param name="staffId">服务的员工</param>
        public void SetOnServe(int counterIndex, string staffId)
        {
            lock (_Lock)
            {
                _TransactionStatus = TransactionStatus.OnServe;
                _CallTime = DateTime.Now;
                CounterIndex = counterIndex;
                StaffId = staffId;
            }
            OnTransactionStatusChanged();
        }

        /// <summary>设置当前流水为“服务完成”的状态，并填充相关信息
        /// </summary>
        public void SetFinished()
        {
            lock (_Lock)
            {
                _TransactionStatus = TransactionStatus.Finished;
                _TransactionEndTime = DateTime.Now;
            }
            OnTransactionStatusChanged();
        }

        /// <summary>设置当前流水为“弃票”的状态，并填充相关信息
        /// </summary>
        public void SetGivenUp()
        {
            lock (_Lock)
            {
                _TransactionStatus = TransactionStatus.GivenUp;
                _TransactionEndTime = DateTime.Now;
            }
            OnTransactionStatusChanged();
        }

        #endregion

        #region Any helper methods

        /// <summary>根据从SQLite数据库中取出的数据创建<see cref="Transaction"/>。
        /// SQLite数据库的数据类型较简单，处理上略复杂。
        /// </summary>
        /// <param name="record">The record.</param>
        /// <returns></returns>
        public static Transaction CreateInstanceFromSqLite(IDataReader record)
        {
            var tran = new Transaction();

            TransactionStatus tranStatus;
            Enum.TryParse(record.Field<long>("TransactionStatus").ToString(), out tranStatus);
            tran.TransactionStatus = tranStatus;

            tran.Id = record.Field<string>("Id");
            tran.QueueId = record.Field<string>("QueueId");
            tran.TicketNumber = record.Field<string>("TicketNumber");
            tran.WorkflowNodeIndex = (short) record.Field<long>("WorkflowNodeIndex");

            tran.AskEvalTime = ParseDateTime(record, "AskEvalTime");
            tran.CallTime = ParseDateTime(record, "CallTime");
            tran.GetNumberTime = ParseDateTime(record, "GetNumberTime");
            tran.TransactionEndTime = ParseDateTime(record, "TransactionEndTime");

            tran.EvaluationType = record.Field<short>("EvaluationType");
            tran.NumberGenerationType = (NumberGenerationType) record.Field<short>("NumberGenerationType");

            var valuestr = record.Field<string>("Details");
            if (!string.IsNullOrWhiteSpace(valuestr))
            {
                var map = new SerializableMap<string, string>(0);
                try
                {
                    map = UtilitySerialize.Deserialize<SerializableMap<string, string>>(valuestr.Trim());
                }
                catch (Exception e)
                {
                    _Logger.WarnException("Details反序列化异常。" + valuestr, e);
                }
                tran.Details = map;
            }
            return tran;
        }

        private static DateTime? ParseDateTime(IDataReader record, string fieldName)
        {
            var value = record.Field<string>(fieldName);
            if (string.IsNullOrWhiteSpace(value))
                return null;
            DateTime dt;
            DateTime.TryParse(value, out dt);
            return dt;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(string.Format("[{0}],", TicketNumber));
            //sb.Append("ID:").Append(Id.Substring(0, 8));
            sb.Append("ID:").Append(Id);
            return sb.ToString();
        }

        public Transaction Clone()
        {
            return Clone(false);
        }

        /// <summary>当因为新动作产生创建副本时，请传入True参数，将会为Transaction创建新的Guid
        /// </summary>
        /// <param name="isNewAction">if set to <c>true</c> [is new action].</param>
        /// <returns></returns>
        public Transaction Clone(bool isNewAction)
        {
            var tran = (Transaction)((ICloneable)this).Clone();
            if (isNewAction)
            {
                string oldId = tran.Id;
                //tran.Id = Guid.NewGuid().ToString();
                tran.Id = string.Empty;
                tran.Details.Add(string.Format("Clone,{0}", Details.Count), string.Format("SrcId:{0}", oldId));
            }
            return tran;
        }

        #endregion

        #region 新的Engine不会用到的属性

        /// <summary>流水号
        /// </summary>
        [EntityColumn]
        public ushort FlowNo { get; set; }

        /// <summary> 排队号码特殊优先级，1表示绝对优先，0表示正常级别，
        /// </summary>
        [EntityColumn]
        public ushort Priority { get; set; }

        #endregion

    }
}