﻿using System;
using System.Globalization;
using Didaku.Attributes;
using Didaku.QEngine.Common.Interfaces;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Entities
{
    [Serializable]
    public class District : IComparable, IEquatable<District>, IServiceQueueElement, IQEntity<string>
    {
        public District()
        {
            Ranges = new WorkRangeCollection(0);
        }

        [EntityColumn]
        public string Id { get; set; }

        [EntityColumn]
        public string Name { get; set; }

        [EntityColumn]
        public string Number { get; set; }

        [EntityColumn]
        public int Index { get; set; }

        /// <summary>对应的多个工作时间段
        /// </summary>
        [EntityColumn]
        public WorkRangeCollection Ranges { get; set; }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj == null || !(obj is District))
                return -1;
            return Index - ((District) obj).Index;
        }

        #endregion

        #region IEquatable<DistrictInfo> Members

        public bool Equals(District other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id) && other.Index == Index;
        }

        #endregion

        public override string ToString()
        {
            return Index.ToString(CultureInfo.InvariantCulture);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (District)) return false;
            return Equals((District) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = Id.GetHashCode();
                result = (result*397) ^ Index;
                result = (result*397) ^ (Name != null ? Name.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(District left, District right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(District left, District right)
        {
            return !Equals(left, right);
        }

        #region Implementation of IServiceQueueElement

        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return Id;
        }
        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        public bool IsActived()
        {
            return true;
        }

        #endregion

    }
}