﻿using System;
using System.Xml;
using Didaku.Attributes;
using Didaku.QEngine.Common.Interfaces;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper;
using Didaku.Wrapper;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>
    /// 柜员实体类
    /// </summary>
    [Serializable]
    public class Staff : IQEntity<string>, IServiceQueueElement
    {
        public Staff()
        {
            Ranges = new WorkRangeCollection(0);
        }

        [EntityColumn]
        public string Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        [EntityColumn]
        public string Number { get; set; }

        [EntityColumn]
        public string Password { get; set; }

        [EntityColumn]
        public PersonSex Sex { get; set; }

        /// <summary>对应的多个工作时间段
        /// </summary>
        /// <value>
        /// The ranges.
        /// </value>
        [EntityColumn]
        public WorkRangeCollection Ranges { get; set; }

        [EntityColumn]
        public DateTime CreatedTime { get; set; }

        public int LoginCounterId { get; set; }
        public bool IsLogin { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Number, Name);
        }

        /// <summary>
        /// 实体转换成Xml的WinUser节点
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public XmlElement ToXml(XmlDocument doc)
        {
            XmlElement ele = doc.CreateElement("WinUser");
            ele.AppendChild(BuildElement(doc, "Id", Id));
            ele.AppendChild(BuildElement(doc, "Sex", Sex));
            ele.AppendChild(BuildElement(doc, "Number", Number));
            ele.AppendChild(BuildElement(doc, "Name", Name));
            ele.AppendChild(BuildElement(doc, "Password", Password));
            return ele;
        }

        /// <summary>
        /// 由Xml节点转换成实体
        /// </summary>
        /// <param name="element"></param>
        public void Parse(XmlElement element)
        {
            foreach (XmlNode node in element.ChildNodes)
            {
                if (node.NodeType != XmlNodeType.Element)
                    continue;
                switch (node.Name)
                {
                    case "Id":
                        Id = node.InnerText.Trim();
                        break;
                    case "Number":
                        Number = node.InnerText.Trim();
                        break;
                    case "Name":
                        Name = node.InnerText.Trim();
                        break;
                    case "Password":
                        Password = node.InnerText.Trim();
                        break;
                    case "Sex":
                        //Sex = UtilityConvert.EnumParse(node.InnerText.Trim(), PersonSex.None);
                        break;
                    default:
                        throw new ArgumentException(node.InnerText);
                }
            }
        }

        protected static XmlElement BuildElement(XmlDocument doc, string localname, object value)
        {
            XmlElement ele = doc.CreateElement(localname);
            if (value != null)
                ele.InnerText = value.ToString();
            return ele;
        }

        #region Implementation of IServiceQueueElement

        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return Id;
        }
        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        public bool IsActived()
        {
            return true;
        }

        #endregion
    }
}

//<WinUser>
//  <UserID>1004</UserID>
//  <UserName>朱红</UserName>
//  <UserClass>5</UserClass>
//</WinUser>