﻿using System;
using Didaku.Attributes;
using Didaku.QEngine.Common.Abstracts.Entities;
using Didaku.QEngine.Common.Enums;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>客户实体类
    /// 客户检测到后，经识别后产生，客户办完业务离开后，该信息应从字典中删除
    /// </summary>
    [Serializable]
    public class Customer : CustomerInfo, IEquatable<Customer>
    {
        /// <summary>客户来时输入的身份识别信息，如卡号，折号，身份证号，手机号
        /// </summary>
        [EntityColumn]
        public string CardNumber { get; set; }

        /// <summary>客户所持卡片类型
        /// </summary>
        [EntityColumn]
        public NumberGenerationType IdType { get; set; }

        /// <summary>该客户所持票号
        /// </summary>
        /// <value>The ticket number.</value>
        [EntityColumn]
        public string TicketNumber { get; set; }

        /// <summary>客户的星级所对应的CustomerLevel集合的记录ID
        /// </summary>
        [EntityColumn]
        public string CardCategoryKind { get; set; }

        /// <summary>营销信息
        /// </summary>
        /// <value>The marketing info.</value>
        [EntityColumn]
        public string[] MarketingInfo { get; set; }

        #region IEquatable<Customer> Members

        public bool Equals(Customer other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.CardNumber, CardNumber) && Equals(other.Name, Name) && Equals(other.TicketNumber, TicketNumber);
        }

        #endregion

        /**
        /// <summary>是否持有网上银行
        /// </summary>
        public bool HasInternetBank { get; set; }

        /// <summary>是否持有电话银行
        /// </summary>
        public bool HasPhoneBank { get; set; }

        /// <summary>是否持有手机银行
        /// </summary>
        public bool HasMobilePhoneBank { get; set; }

        /// <summary>是否持有理财金
        /// </summary>
        public bool HasLiCaiJin { get; set; }

        /// <summary>是否持有灵通卡
        /// </summary>
        public bool HasLingTongKa { get; set; }

        /// <summary>是否持有本外币理财
        /// </summary>
        public bool HasBenWaiBiLiCai { get; set; }

        /// <summary>是否持有基金
        /// </summary>
        public bool HasJiJin { get; set; }

        /// <summary>是否持有保险
        /// </summary>
        public bool HasBaoXian { get; set; }

        /// <summary>客户的星级
        /// </summary>
        /// <value>The level of service.</value>
        public string LevelOfService { get; set; }

        /// <summary>客户贡献星级
        /// </summary>
        /// <value>The level of contributive.</value>
        public string LevelOfContributive { get; set; }

        **/

        /// <summary>非程序的克隆方法。仅复制一份新实体，该实体的属性仅包括基本的校验前信息
        /// </summary>
        /// <returns></returns>
        public Customer SimpleBaseClone()
        {
            var ci = new Customer
                     {
                         CardNumber = CardNumber,
                         Name = Name,
                         IdType = IdType,
                         Sex = Sex,
                         Birthday = Birthday,
                         TicketNumber = TicketNumber
                     };
            return ci;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Customer)) return false;
            return Equals((Customer) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (CardNumber != null ? CardNumber.GetHashCode() : 0);
                result = (result*397) ^ (Name != null ? Name.GetHashCode() : 0);
                result = (result*397) ^ (TicketNumber != null ? TicketNumber.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(Customer left, Customer right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Customer left, Customer right)
        {
            return !Equals(left, right);
        }
    }
}