﻿using System;
using System.Data;
using System.Xml;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Interfaces;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>业务类型
    /// </summary>
    [Serializable]
    public class BusinessType : IServiceQueueElement, IQEntity<string>, IEquatable<BusinessType>, ICloneable
    {
        /// <summary>业务类型
        /// </summary>
        public BusinessType()
        {
            Ranges = new WorkRangeCollection(0);
            Details = new SerializableMap<string, string>(0);
        }

        /// <summary>业务编号
        /// </summary>
        [EntityColumn]
        public string Id { get; set; }

        /// <summary>业务编号
        /// </summary>
        /// <value>
        /// 编号
        /// </value>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>业务名称
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>号票分段编码
        /// </summary>
        [EntityColumn]
        public string CallHead { get; set; }

        /// <summary>所属流程节点的编号
        /// </summary>
        [EntityColumn]
        public string ProgressIndex { get; set; }

        /// <summary>号票起始数字
        /// </summary>
        /// <value>
        /// The ticket number start.
        /// </value>
        [EntityColumn]
        public ushort TicketNumberStart { get; set; }

        /// <summary>号票终止数字
        /// </summary>
        /// <value>
        /// The ticket number end.
        /// </value>
        [EntityColumn]
        public ushort TicketNumberEnd { get; set; }

        /// <summary>对应的多个工作时间段
        /// </summary>
        /// <value>
        /// The ranges.
        /// </value>
        [EntityColumn]
        public WorkRangeCollection Ranges { get; set; }

        /// <summary>描述信息
        /// </summary>
        /// <value>
        /// The infomation.
        /// </value>
        [EntityColumn]
        public string Infomation { get; set; }

        /// <summary>其他信息
        /// </summary>
        /// <value>
        /// The infomation.
        /// </value>
        [EntityColumn]
        public SerializableMap<string, string> Details { get; set; }

        #region IEquatable<WinTypeInfo> Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(BusinessType other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id);
        }

        #endregion

        #region override

        public object Clone()
        {
            var wintype = new BusinessType
                              {
                                  CallHead = CallHead,
                                  Id = Id,
                                  Infomation = Infomation,
                                  Name = Name,
                                  TicketNumberEnd = TicketNumberEnd,
                                  TicketNumberStart = TicketNumberStart
                              };
            if(Ranges != null)
            {
                foreach (var range in Ranges)
                    wintype.Ranges.Add(range);
            }
            if(Details != null)
            {
                foreach (var detail in Details)
                    wintype.Details.Add(detail.Key, detail.Value);
            }
            return wintype;
        }

        public XmlElement ToXml(XmlDocument doc)
        {
            XmlElement ele = doc.CreateElement("WinType");
            ele.SetChildElement("Id", Id);
            ele.SetChildElement("Number", Number);
            ele.SetChildElement("CallHead", CallHead);
            ele.SetChildElement("Name", Name);
            ele.SetChildElement("TicketNumberStart", TicketNumberStart);
            ele.SetChildElement("TicketNumberEnd", TicketNumberEnd);
            return ele;
        }

        public void Parse(XmlElement element)
        {
            foreach (XmlNode node in element.ChildNodes)
            {
                if (node.NodeType != XmlNodeType.Element)
                    continue;
                if (node.Name == "Id")
                    Id = node.InnerText.Trim();
                else if (node.Name == "Number")
                    Number = node.InnerText.Trim();
                else if (node.Name == "CallHead")
                    CallHead = node.InnerText.Trim();
                else if (node.Name == "Name")
                    Name = node.InnerText.Trim();
                else if (node.Name == "TicketNumberStart")
                    TicketNumberStart = ushort.Parse(node.InnerText.Trim());
                else if (node.Name == "TicketNumberEnd")
                    TicketNumberEnd = ushort.Parse(node.InnerText.Trim());
                else
                {
                    throw new ArgumentException(node.InnerText);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}", Number, Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (BusinessType)) return false;
            return Equals((BusinessType) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Number.GetHashCode()*397) ^ Id.GetHashCode();
            }
        }

        public static bool operator ==(BusinessType left, BusinessType right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BusinessType left, BusinessType right)
        {
            return !Equals(left, right);
        }

        #endregion

        /// <summary>从数据行中解析得到一个业务类型
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public static BusinessType Parse(DataRow row)
        {
            var buss = row.ToEntity(new BusinessType());

            if (buss.Ranges != null && buss.Ranges.Count > 0)
                buss.Ranges.ConvertToTaday(); //转换时间段中的时间表示为今天

            if (buss.TicketNumberStart <= 0)
                buss.TicketNumberStart = 1;
            if (buss.TicketNumberEnd <= 0)
                buss.TicketNumberEnd = 999;

            return buss;
        }

        #region Implementation of IServiceQueueElement

        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return Id;
        }

        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        public bool IsActived()
        {
            return true;
        }

        #endregion
    }
}