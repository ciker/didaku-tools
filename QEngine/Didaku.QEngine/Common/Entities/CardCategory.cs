﻿using Didaku.QEngine.Common.Abstracts.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;

namespace Didaku.QEngine.Common.Entities
{
    /// <summary>卡片类别
    /// </summary>
    public class CardCategory : CardCategoryInfo, IServiceQueueElement
    {
        public CardCategory()
        {
            this.Category = "0";
        }

        #region Implementation of IServiceQueueElement

        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        public string GetId()
        {
            return Id;
        }

        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        public bool IsActived()
        {
            return true;
        }

        #endregion
    }
}