﻿using System.Collections.Generic;
using System.Linq;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Common.Extensions
{
    internal static class QueueExtensions
    {
        /// <summary>超级队列
        /// </summary>
        public static ServiceQueue GetSuperQueue(this ServiceLogic logic)
        {
            return logic[0][0];
        }

        /// <summary>服务队列逻辑是否包含指定ID的队列
        /// </summary>
        public static bool ContainsQueue(this ServiceLogic logic, string id)
        {
            return logic.Any(queueGroup => queueGroup.Any(serviceQueue => serviceQueue.Id.Equals(id)));
        }

        /// <summary>获取所有队列的ID的集合
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> GetQueueIdList(this ServiceLogic logic)
        {
            var list = new List<string>();
            foreach (ServiceQueueGroup queueGroup in logic)
            {
                foreach (ServiceQueue serviceQueue in queueGroup)
                {
                    if (!list.Contains(serviceQueue.Id))
                        list.Add(serviceQueue.Id);
                }
            }
            list.TrimExcess();
            return list;
        }
    }
}