using System;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;

namespace Didaku.QEngine.Common.EventArg
{
    public delegate void ServiceQueueEventHandler(IServiceQueue<Transaction> serviceQueue, ServiceQueueEventArgs e);

    public class ServiceQueueEventArgs : EventArgs
    {
        public Transaction Transaction { get; private set; }

        public ServiceQueueEventArgs(Transaction tran)
        {
            Transaction = tran;
        }
    }
}