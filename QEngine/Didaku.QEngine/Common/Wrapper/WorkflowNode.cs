using System;
using System.ComponentModel.DataAnnotations;

namespace Didaku.QEngine.Common.Wrapper
{
    /// <summary>一个描述指定类型可做一个工作流节点的接口。如业务类型、柜员等。
    /// </summary>
    [Serializable]
    public class WorkflowNode
    {
        /// <summary>节点所属类型的ID，如业务类型ID
        /// </summary>
        [Key]
        public string NodeId { get; set; }

        /// <summary>当前节点在链中的顺序
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public short Order { get; set; }

        /// <summary>节点类型
        /// </summary>
        public string NodeType { get; set; }
    }
}