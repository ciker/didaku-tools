using System;
using System.Collections.Generic;
using System.Linq;

namespace Didaku.QEngine.Common.Wrapper
{
    /// <summary>工作时间段的集合
    /// </summary>
    [Serializable]
    public class WorkRangeCollection : List<WorkRange>
    {
        public WorkRangeCollection()
        {
        }

        public WorkRangeCollection(int length)
            : base(length)
        {
        }

        /// <summary>当前时间所属工作时间段
        /// </summary>
        public WorkRange GetCurrentWorkRange()
        {
            foreach (WorkRange range in this)
            {
                range.ConvertToTaday();
                if (range.InTheTime(DateTime.Now))
                {
                    return range;
                }
            }
            return null;
        }

        /// <summary>当前时间所属工作时间段的最大取票量
        /// </summary>
        public int GetWorkRangeMaxValue()
        {
            var range = GetCurrentWorkRange();
            return range == null ? 0 : range.MaxValue;
        }

        /// <summary>指定的时间是否在本类型描述的时间段集合之内
        /// </summary>
        public bool InTheTime(DateTime dateTime)
        {
            return this.Any(range => range.InTheTime(dateTime));
        }

        /// <summary>转换时间段中的时间表示为今天
        /// </summary>
        public void ConvertToTaday()
        {
            foreach (WorkRange range in this)
                range.ConvertToTaday();
        }
    }
}