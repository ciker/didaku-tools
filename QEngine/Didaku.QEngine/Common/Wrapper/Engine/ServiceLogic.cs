using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;

namespace Didaku.QEngine.Common.Wrapper.Engine
{
    /// <summary>描述一个窗口(柜台)的服务队列逻辑。它由多个【服务队列组】组成。[队列组]的优先级由[队列组]在List中的顺序决定。
    /// </summary>
    /// <remarks></remarks>
    public class ServiceLogic : List<ServiceQueueGroup>, IServiceLogic
    {
        /// <summary>该逻辑所属的柜台
        /// </summary>
        /// <value>
        /// The counter.
        /// </value>
        public Counter Counter { get; set; }

        #region Implementation of ICloneable

        public object Clone()
        {
            return Clone(true);
        }

        public ServiceLogic Clone(bool needCopyContent)
        {
            var newLogic = new ServiceLogic();
            if (needCopyContent) //是否拷贝内容
                newLogic.AddRange(this);
            newLogic.Counter = (Counter) Counter.Clone();
            return newLogic;
        }

        #endregion
    }
}