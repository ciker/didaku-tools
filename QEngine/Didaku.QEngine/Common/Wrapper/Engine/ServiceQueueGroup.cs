using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;

namespace Didaku.QEngine.Common.Wrapper.Engine
{
    /// <summary>描述营业厅的一个窗口(柜台)服务队列的组。它将被包括在<see cref="IServiceQueueGroup.TryAssign"/>中。
    /// 如果这个组的分配方法有特殊，请重写<seealso IServiceQueueGroupAssign"/>方法。
    /// </summary>
    public abstract class ServiceQueueGroup : List<ServiceQueue>, IServiceQueueGroup, ICloneable
    {
        private readonly string _Id;
        private readonly List<string> _QueueIds = new List<string>();

        protected ServiceQueueGroup()
        {
            _Id = Guid.NewGuid().ToString();
        }

        #region IEquatable<ServiceQueueGroup> Members

        public bool Equals(ServiceQueueGroup other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._Id.Equals(_Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(ServiceQueueGroup)) return false;
            return Equals((ServiceQueueGroup)obj);
        }

        public override int GetHashCode()
        {
            return _Id.GetHashCode();
        }

        public abstract object Clone();

        public static bool operator ==(ServiceQueueGroup left, ServiceQueueGroup right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ServiceQueueGroup left, ServiceQueueGroup right)
        {
            return !Equals(left, right);
        }

        #endregion

        #region Implementation of IXmlSerializable

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("QueueList");
            if (Count > 0)
            {
                foreach (ServiceQueue q in this)
                    writer.WriteElementString("QueueId", q.Id);
            }
            writer.WriteEndElement();
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                return;

            if (!reader.Read())
                throw new XmlException("XmlReader读取异常");
            reader.MoveToContent();

            reader.ReadStartElement("QueueList");
            while (reader.NodeType != XmlNodeType.EndElement && reader.NodeType != XmlNodeType.None)
            {
                reader.ReadStartElement("QueueId");
                _QueueIds.Add(reader.ReadString());
            }
            reader.ReadEndElement();
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        #endregion

        /// <summary>队列组的识别名
        /// </summary>
        public string Name { get; set; }

        /// <summary>尝试从队列组中分配一个交易
        /// </summary>
        /// <param name="transaction">输出:一个排队的交易</param>
        /// <returns>如果有可用的交易，返回真。反之，返回否。</returns>
        public virtual bool TryAssign(out Transaction transaction)
        {
            transaction = null;
            foreach (var queue in this)
            {
                if (queue.TryTake(out transaction))
                    return true;
            }
            return false;
        }

        /// <summary>判断队列组是否激活。(即俗称的备用队列的激活条件)
        /// </summary>
        public virtual bool IsActive()
        {
            return true;
        }

        /// <summary>通过本函数填充组中的队列实体。
        /// 当序列化过程时为节约空间，仅存储了队列的ID，本函数传入程序启动时从配置文件中反序列化得到的队列字典。
        /// </summary>
        /// <param name="queues">程序启动时从配置文件中反序列化得到的队列字典.</param>
        public virtual void FillItems(IDictionary<string, ServiceQueue> queues)
        {
            foreach (string queueId in _QueueIds)
            {
                if (queues.ContainsKey(queueId))
                    Add(queues[queueId]);
            }
        }
    }
}