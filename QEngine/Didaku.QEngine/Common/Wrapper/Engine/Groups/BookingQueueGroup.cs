﻿using Didaku.QEngine.Common.Attributes;

namespace Didaku.QEngine.Common.Wrapper.Engine.Groups
{
    [QueueGroup("预约队列组，本组中排队票号的生成是预约为条件的。")]
    public class BookingQueueGroup : ServiceQueueGroup
    {
        #region Overrides of ServiceQueueGroup

        public override object Clone()
        {
            var group = new GenericQueueGroup();
            group.AddRange(this);
            return group;
        }

        #endregion

    }
}
