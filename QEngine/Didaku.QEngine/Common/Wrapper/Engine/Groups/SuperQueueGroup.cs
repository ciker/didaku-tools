﻿using System;
using Didaku.QEngine.Common.Attributes;
using Didaku.QEngine.Common.Entities;

namespace Didaku.QEngine.Common.Wrapper.Engine.Groups
{
    [QueueGroup("超级队列组，本组中排队票号将最优先被呼叫。")]
    public class SuperQueueGroup : ServiceQueueGroup
    {
        public SuperQueueGroup(Counter counter)
        {
            var superQueue = new ServiceQueue();
            superQueue.Name = string.Format("SuperQueue-{0}", counter.Index);
            superQueue.Id = Guid.NewGuid().ToString();
            superQueue.Elements.Add(counter);
            Add(superQueue);
        }

        #region Overrides of ServiceQueueGroup

        public override object Clone()
        {
            var group = new SuperQueueGroup((Counter) this[0].Elements[0]);
            return group;
        }

        #endregion

    }
}
