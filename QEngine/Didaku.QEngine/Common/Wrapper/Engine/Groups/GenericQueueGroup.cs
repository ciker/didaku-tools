﻿using Didaku.QEngine.Common.Attributes;

namespace Didaku.QEngine.Common.Wrapper.Engine.Groups
{
    [QueueGroup("普通队列组，本组中排队票号的生成按照基本的取票时间为准则。")]
    public class GenericQueueGroup : ServiceQueueGroup
    {
        #region Overrides of ServiceQueueGroup

        public override object Clone()
        {
            var group = new GenericQueueGroup();
            group.AddRange(this);
            return group;
        }

        #endregion
    }
}
