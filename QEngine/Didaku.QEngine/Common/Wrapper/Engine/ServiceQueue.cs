using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Didaku.Attributes;
using Didaku.QEngine.Common.Counts;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Enums;
using Didaku.QEngine.Common.EventArg;
using Didaku.QEngine.Common.Interfaces.Engine;

namespace Didaku.QEngine.Common.Wrapper.Engine
{
    /// <summary>描述营业厅的一个窗口(柜台)服务队列的属性特征类型。
    /// 它将被包括在一个窗口(柜台)所拥有的<see cref="ServiceQueueGroup"/>队列组中。
    /// </summary>
    [Serializable]
    public class ServiceQueue : IServiceQueue<Transaction>, IEnumerable<Transaction>, IXmlSerializable, IEquatable<ServiceQueue>
    {
        private readonly ConcurrentBag<Transaction> _Trans = new ConcurrentBag<Transaction>();

        public ServiceQueue()
        {
            Elements = new List<IServiceQueueElement>(1);
            CallHead = string.Empty;
        }

        #region IServiceQueue: 基本特征属性

        /// <summary>队列标识
        /// </summary>
        [EntityColumn]
        public string Id { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>队列的表示元素，可以是业务,业务分类,客户类别,员工,等等
        /// </summary>
        /// <value>
        /// The elements.
        /// </value>
        [EntityColumn]
        [XmlIgnore]
        public List<IServiceQueueElement> Elements { get; set; }

        /// <summary>队列分段编码(号票前缀)
        /// </summary>
        [EntityColumn]
        public string CallHead { get; set; }

        /// <summary>队列名称
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>号票长度(包含队列编码)
        /// </summary>
        [EntityColumn]
        public ushort TicketLength { get; set; }

        /// <summary>当前队列最多可出票量，为0时表示不限制
        /// </summary>
        [EntityColumn]
        public ushort TicketMaxCount { get; set; }

        /// <summary>号票起始数字
        /// </summary>
        /// <value>
        /// The ticket number start.
        /// </value>
        [EntityColumn]
        public ushort TicketStartNumber { get; set; }

        /// <summary>号票终止数字
        /// </summary>
        /// <value>
        /// The ticket number end.
        /// </value>
        [EntityColumn]
        public ushort TicketEndNumber { get; set; }

        #endregion

        #region IServiceQueue: 基本方法

        /// <summary>计算已完成的交易数量
        /// </summary>
        public virtual Count Calculate(TransactionStatus calculateType)
        {
            var count = new Count
            {
                OnWait = _Trans.Count(tran => tran.TransactionStatus == TransactionStatus.OnWait),
                OnServe = _Trans.Count(tran => tran.TransactionStatus == TransactionStatus.OnServe),
                UnServe = _Trans.Count(tran => tran.TransactionStatus == TransactionStatus.UnServe),
                GivenUp = _Trans.Count(tran => tran.TransactionStatus == TransactionStatus.GivenUp), 
                Finished = _Trans.Count(tran => tran.TransactionStatus == TransactionStatus.Finished)
            };
            return count;
        }

        /// <summary>生成一条新的交易<see cref="Transaction"/>，这条交易生成后被加入到队列的交易集合中。
        /// </summary>
        /// <param name="currCount">输出:当前队列的号票计数</param>
        /// <returns></returns>
        public virtual Transaction Generate(out int currCount)
        {
            Transaction tran = GetBaseTransaction();
            tran.TicketNumber = GenerateTicketNumber();
            Add(tran);//加入到队列的交易集合中
            currCount = _CurrentTicketNumber;
            return tran;
        }

        /// <summary>尝试根据指定的条件返回最早加入的<see cref="Transaction"/>.当指定的条件为Null时,采用最基本的取票时间优先原则。
        /// </summary>
        /// <param name="transaction">输出:可办理的交易</param>
        /// <param name="func">指定的条件。条件为Null时,采用最基本的取票时间优先原则。</param>
        /// <returns>如果有可办理的交易，返回真。否则反之。</returns>
        public virtual bool TryTake(out Transaction transaction, Func<IEnumerable<Transaction>, Transaction> func = null)
        {
            transaction = (func != null) ? func.Invoke(_Trans) : GetByGetNumberTimeFunc().Invoke(_Trans);
            return transaction != null;
        }

        /// <summary>尝试根据指定的票号返回<see cref="Transaction"/>.
        /// </summary>
        /// <param name="transaction">输出:可办理的交易</param>
        /// <param name="number">指定的票号</param>
        /// <returns>如果有可办理的交易，返回真。否则反之。</returns>
        public virtual bool TrySearchRunning(string number, out Transaction transaction)
        {
            transaction = _Trans.FirstOrDefault(t => IsSearchCondition(t, number, TransactionStatus.OnWait));
            return transaction != null;
        }

        /// <summary>尝试从历史数据中根据指定的票号返回可以找到的<see cref="Transaction"/>，并返回它的Clone体（恢复初始状态）。输出时未被加入到队列中。一般是回呼时使用。
        /// </summary>
        /// <param name="number">指定的票号</param>
        /// <param name="transaction">输出:可办理的交易, 该交易是原交易的Clone（恢复初始状态），且输出时未被加入到队列中。</param>
        /// <returns>如果有可办理的交易，返回真。否则反之。</returns>
        public virtual bool TrySearchClosed(string number, out Transaction transaction)
        {
            var closedTran = _Trans.FirstOrDefault(t => IsSearchCondition(t, number, TransactionStatus.Finished));
            if(closedTran != null)
            {
                transaction = closedTran.Clone(true);
                transaction.Reconvert();
                return true;
            }
            transaction = null;
            return false;
        }

        private bool IsSearchCondition(Transaction tran, string number, TransactionStatus status)
        {
            return tran.TicketNumber.Equals(number) && tran.TransactionStatus.HasFlag(status);
        }

        /// <summary>向队列中添加一个新的交易<see cref="Transaction"/>,当是新的交易时本方法中会激发新增事件
        /// </summary>
        /// <param name="tran">The tran.</param>
        /// <param name="isNewTransaction">被添加的交易是否是新的交易(可能是被转移过来的,就不属于新交易)</param>
        public virtual void Add(Transaction tran, bool isNewTransaction = true)
        {
            _Trans.Add(tran);
            if (isNewTransaction)
                OnAdded(new ServiceQueueEventArgs(tran));
        }

        #endregion

        #region Implementation of IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>返回循环访问<see cref="ServiceQueue"/>中的<see cref="Transaction"/>的枚举器
        /// </summary>
        public IEnumerator<Transaction> GetEnumerator()
        {
// ReSharper disable AssignNullToNotNullAttribute
            return _Trans.GetEnumerator();
// ReSharper restore AssignNullToNotNullAttribute
        }

        #endregion

        #region Protected

        private int _CurrentTicketNumber = -1;
        private StringBuilder _TicketNumberBuilder = new StringBuilder();
        private readonly List<string> _ElementIds = new List<string>();

        /// <summary>填充队列的表示元素，可以是业务,业务分类,客户类别,员工,等等。因当队列类型实例生成后，仅有各元素的Id。
        /// </summary>
        public void Fill(Func<List<string>, List<IServiceQueueElement>> action)
        {
            Elements = action.Invoke(_ElementIds);
        }

        protected int Count
        {
            get { return _Trans.Count; }
        }

        /// <summary>队列中是否包含指定的<see cref="Transaction"/>
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns><c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.</returns>
        /// <remarks></remarks>
        protected bool Contains(Transaction item)
        {
            return _Trans.Contains(item);
        }

        /// <summary>将队列中的<see cref="Transaction"/>拷贝到指定的数组中
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        /// <remarks></remarks>
        protected void CopyTo(Transaction[] array, int arrayIndex)
        {
            _Trans.CopyTo(array, arrayIndex);
        }

        /// <summary>根据队列规则生成对应排队号码
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual string GenerateTicketNumber()
        {
            if (_CurrentTicketNumber < 0)
            {
                _CurrentTicketNumber = TicketStartNumber;
                _TicketNumberBuilder = new StringBuilder(TicketLength);
            }
            _TicketNumberBuilder.Clear();
            _TicketNumberBuilder.Append(CallHead);
            var number = _CurrentTicketNumber.ToString().PadLeft(TicketLength - CallHead.Length, '0');
            _CurrentTicketNumber++;
            _TicketNumberBuilder.Append(number);
            return _TicketNumberBuilder.ToString();
        }

        /// <summary>创建基本的<see cref="Transaction"/>
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual Transaction GetBaseTransaction()
        {
            var t = new Transaction();
            BindTransactionEvent(t);
            t.QueueId = Id;
            return t;
        }

        /// <summary>绑定一条交易的事件在本队列中
        /// </summary>
        /// <param name="t">The t.</param>
        private void BindTransactionEvent(Transaction t)
        {
            t.AskEvalTimeChanged += TransactionChanged;
            t.CallTimeChanged += TransactionChanged;
            t.DataStatusChanged += TransactionChanged;
            t.ShortChanged += TransactionChanged;
            t.GetNumberTimeChanged += TransactionChanged;
            t.TransactionEndTimeChanged += TransactionChanged;
            t.TransactionStatusChanged += TransactionChanged;
        }

        /// <summary>当交易的一些关键属性发生变化时
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void TransactionChanged(object sender, EventArgs e)
        {
            var transaction = (Transaction) sender;
            OnChanged(new ServiceQueueEventArgs(transaction));
        }

        #endregion

        #region 事件的定义

        /// <summary>当队列中新增一条交易<see cref="Transaction"/>时发生的事件
        /// </summary>
        public event ServiceQueueEventHandler TransactionAddedEvent;

        /// <summary>当队列中移除一条交易<see cref="Transaction"/>时发生的事件
        /// </summary>
        public event ServiceQueueEventHandler TransactionRemovedEvent;

        /// <summary>当队列中一条交易<see cref="Transaction"/>的属性改变时发生的事件
        /// </summary>
        public event ServiceQueueEventHandler TransactionChangedEvent;

        protected virtual void OnAdded(ServiceQueueEventArgs e)
        {
            if (TransactionAddedEvent != null)
                TransactionAddedEvent(this, e);
        }

        protected virtual void OnRemoved(ServiceQueueEventArgs e)
        {
            if (TransactionRemovedEvent != null)
                TransactionRemovedEvent(this, e);
        }

        protected virtual void OnChanged(ServiceQueueEventArgs e)
        {
            if (TransactionChangedEvent != null)
                TransactionChangedEvent(this, e);
        }

        #endregion

        #region Implementation of IXmlSerializable

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("Id", Id);
            writer.WriteElementString("CallHead", CallHead);
            writer.WriteElementString("Name", Name);
            writer.WriteElementString("TicketLength", TicketLength.ToString());
            writer.WriteElementString("TicketMaxCount", TicketMaxCount.ToString());
            writer.WriteElementString("TicketStartNumber", TicketStartNumber.ToString());
            writer.WriteElementString("TicketEndNumber", TicketEndNumber.ToString());
            writer.WriteStartElement("Elements");
            if (Elements != null && Elements.Count > 0)
            {
                foreach (IServiceQueueElement element in Elements)
                    writer.WriteElementString("ElementId", element.GetId());
            }
            writer.WriteEndElement();
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                return;

            if (!reader.Read())
                throw new XmlException("XmlReader读取异常");
            reader.MoveToContent();

            Id = reader.ReadElementString("Id");
            CallHead = reader.ReadElementString("CallHead");
            Name = reader.ReadElementString("Name");

            reader.ReadStartElement("TicketLength");
            TicketLength = UInt16.Parse(reader.ReadString());
            reader.ReadEndElement();

            reader.ReadStartElement("TicketMaxCount");
            TicketMaxCount = UInt16.Parse(reader.ReadString());
            reader.ReadEndElement();

            reader.ReadStartElement("TicketStartNumber");
            TicketStartNumber = UInt16.Parse(reader.ReadString());
            reader.ReadEndElement();

            reader.ReadStartElement("TicketEndNumber");
            TicketEndNumber = UInt16.Parse(reader.ReadString());
            reader.ReadEndElement();

            reader.ReadStartElement("Elements");
            while (reader.NodeType != XmlNodeType.EndElement && reader.NodeType != XmlNodeType.None)
            {
                reader.ReadStartElement("ElementId");
                _ElementIds.Add(reader.ReadString());
            }
            reader.ReadEndElement();
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        #endregion

        #region IEquatable<ServiceQueue> Members

        public bool Equals(ServiceQueue other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id.Equals(Id);
        }

        /// <summary>Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (ServiceQueue)) return false;
            return Equals((ServiceQueue) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public object Clone()
        {
            return Clone(true);
        }

        public ServiceQueue Clone(bool isCopyContent)
        {
            var newQueue = new ServiceQueue();
            if (isCopyContent)
            {
                foreach (var tran in this)
                    newQueue.Add(tran);
            }
            newQueue.Id = Id;
            newQueue.Number = Number;
            newQueue.CallHead = CallHead;
            newQueue.Name = Name;
            newQueue.TicketEndNumber = TicketEndNumber;
            newQueue.TicketLength = TicketLength;
            newQueue.TicketMaxCount = TicketMaxCount;
            newQueue.TicketStartNumber = TicketStartNumber;
            newQueue.Elements = Elements;
            return newQueue;
        }

        public static bool operator ==(ServiceQueue left, ServiceQueue right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ServiceQueue left, ServiceQueue right)
        {
            return !Equals(left, right);
        }

        #endregion

        #region 从一个集合中根据取票时间按取票时间优先原则筛选

        /// <summary>从一个集合中根据取票时间按取票时间优先原则筛选
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        internal static Func<IEnumerable<Transaction>, Transaction> GetByGetNumberTimeFunc()
        {
            return trans =>
                   {
                       if (trans == null || !trans.Any())
                           return null;
                       var result = from tran in trans
                                    where tran.TransactionStatus == TransactionStatus.OnWait
                                    orderby
                                        tran.GetNumberTime
                                    select tran;
                       return result.FirstOrDefault();
                   };
        }

        #endregion

    }
}