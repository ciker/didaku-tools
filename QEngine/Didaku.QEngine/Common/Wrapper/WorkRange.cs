﻿using System;

namespace Didaku.QEngine.Common.Wrapper
{
    /// <summary>描述一个工作时间段
    /// </summary>
    [Serializable]
    public class WorkRange
    {
        /// <summary>工作开始时间
        /// </summary>
        /// <value>
        /// The begin.
        /// </value>
        public DateTime Begin { get; set; }

        /// <summary>工作结束时间
        /// </summary>
        /// <value>
        /// The end.
        /// </value>
        public DateTime End { get; set; }

        /// <summary>本工作时间内可用最大的值(在排队系统中即为本工作时间内的最大取票量)
        /// </summary>
        public ushort MaxValue { get; set; }

        /// <summary>指定的时间是否在本类型描述的时间段之内
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        public bool InTheTime(DateTime dateTime)
        {
            return dateTime >= Begin && dateTime <= End;
        }

        /// <summary>转换时间段中的时间表示为今天
        /// </summary>
        public void ConvertToTaday()
        {
            DateTime now = DateTime.Now;
            Begin = new DateTime(now.Year, now.Month, now.Day, Begin.Hour, Begin.Minute, Begin.Second);
            End = new DateTime(now.Year, now.Month, now.Day, End.Hour, End.Minute, End.Second);
        }
    }
}