﻿namespace Didaku.QEngine.Common.Interfaces.Entities
{
    /// <summary>卡片(客户)类型。
    /// 我们理解：在排队系统中通过卡片进行的校验或识别，均得到的是卡片的分类，并被引用为客户的分类属性。
    /// 在排队系统中，鉴于一般很难真正获取到较完整的客户的属性，“客户分类”这个概念被移到CardInfo中。
    /// </summary>
    public interface ICardCategory
    {
        string Category { get; set; }

        string CallHead { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>级别描述
        /// </summary>
        string Description { get; set; }
    }
}