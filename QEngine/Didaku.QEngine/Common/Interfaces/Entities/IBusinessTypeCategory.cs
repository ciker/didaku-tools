﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    public interface IBusinessTypeCategory<T>
        where T : IBusinessTypeCategory<T>
    {
        [Key]
        Guid Id { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>返回该分组的的父级组
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        T Parent { get; set; }
    }
}