﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Wrapper;
using Didaku.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    /// <summary>描述一个员工
    /// </summary>
    public interface IStaff<T> 
        where T : IDepartment
    {
        [Key]
        Guid Id { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>隶属机构
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        [EntityColumn]
        T Department { get; set; }

        /// <summary>密码
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        string Password { get; set; }

        /// <summary>性别
        /// </summary>
        /// <value>
        /// The sex.
        /// </value>
        PersonSex Sex { get; set; }

        /// <summary>本员工在系统中的创建时间
        /// </summary>
        /// <value>
        /// The created time.
        /// </value>
        DateTime? CreatedTime { get; set; }

        /// <summary>员工的其他相关属性信息集合
        /// </summary>
        SerializableMap<string, string> Details { get; set; }

    }
}