﻿using System;
using Didaku.Attributes;
using Didaku.Collections;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    /// <summary>描述一个机构：可以理解为银行网点，保险行业、电信、移动行业的营业厅等。
    /// </summary>
    public interface IDepartment : IQEntity<string>, IComparable, IEquatable<IDepartment>
    {
        /// <summary>地区编码
        /// </summary>
        string AreaCode { get; set; }

        /// <summary>网点的其他相关属性信息集合
        /// </summary>
        [EntityColumn]
        SerializableMap<string, string> Details { get; set; }
    }
}