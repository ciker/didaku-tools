﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Abstracts.Entities;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    public interface IDistrict<T> : IEquatable<DistrictInfo> where T : IDepartment
    {
        [Key]
        Guid Id { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>隶属机构
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        T Department { get; set; }

        /// <summary>分区的其他相关属性信息集合
        /// </summary>
        SerializableMap<string, string> Details { get; set; }
    }
}