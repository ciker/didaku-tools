﻿using System;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Enums;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    public interface ITransaction<TBusinessTypeCategory, TCustomerInfo, TDepartment, TBusinessType, TCounter, TStaff, TCard, TCardKind, TWorkflow, TDistrict>
        where TBusinessTypeCategory : IBusinessTypeCategory<TBusinessTypeCategory>
        where TCustomerInfo : ICustomer
        where TDepartment : IDepartment
        where TBusinessType : IBusinessType<TBusinessTypeCategory>
        where TDistrict : IDistrict<TDepartment>
        where TCounter : ICounter<TDistrict, TDepartment> 
        where TStaff : IStaff<TDepartment>
        where TCardKind : ICardCategory
        where TCard : ICard<TCustomerInfo, TCardKind>
        where TWorkflow : IWorkflow
    {
        /// <summary>全局唯一的ID，采用GUID，以保证无论多少个网点，交易的编号都是不发生重复的
        /// </summary>
        /// <value>The id.</value>
        [Key]
        string Id { get; set; }
         
        /// <summary>排队号码
        /// </summary>
        [StringLength(8)]
        string TicketNumber { get; set; }

        /// <summary>隶属机构
        /// </summary>
        TDepartment Department { get; set; }

        /// <summary>业务类型
        /// </summary>
        TBusinessType BusinessType { get; set; }

        /// <summary>工作流
        /// </summary>
        TWorkflow Workflow { get; set; }

        /// <summary>当交易开始办理时的柜台
        /// </summary>
        TCounter Counter { get; set; }

        /// <summary>当交易开始办理时的员工
        /// </summary>
        TStaff Staff { get; set; }

        /// <summary> 产生号票的卡片信息
        /// </summary>
        TCard Card { get; set; }

        /// <summary> 取号时间
        /// </summary>
        DateTime? GetNumberTime { get; set; }

        /// <summary> 呼叫时间
        /// </summary>
        DateTime? CallTime { get; set; }

        /// <summary> 发起请评价的时间
        /// </summary>
        DateTime? AskEvalTime { get; set; }

        /// <summary> 服务结束时间-呼叫下一个号码时、弃号、退出等动作均会产生结束时间
        /// </summary>
        DateTime? TransactionEndTime { get; set; }

        /// <summary>评价结果
        /// </summary>
        short? EvaluationType { get; set; }

        /// <summary> 取号方式，包括手工取号，刷卡取号，刷二代证取号等等
        /// </summary>
        NumberGenerationType NumberGenerationType { get; set; }

        /// <summary> 详细信息
        /// </summary>
        SerializableMap<string, string> Details { get; set; }
    }
}