﻿using Didaku.QEngine.Common.Entities.Collections;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    /// <summary>工作流
    /// </summary>
    public interface IWorkflow : IQEntity<string>
    {
        /// <summary>工作时间段集合
        /// </summary>
        WorkRangeCollection Ranges { get; set; }

        /// <summary>工作流描述
        /// </summary>
        string Description { get; set; }

        /// <summary>工作流节点链
        /// </summary>
        WorkflowNodeCollection Chain { get; set; }
    }
}