﻿using System;
using Didaku.Collections;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    /// <summary>客户实体类
    /// 客户检测到后，经识别后产生，客户办完业务离开后，该信息应从字典中删除
    /// </summary>
    public interface ICustomer
    {
        /// <summary>
        /// 主键，构造函数中自动赋值
        /// </summary>
        Guid Id { get; set; }

        /// <summary>客户姓名
        /// </summary>
        string Name { get; set; }

        /// <summary>客户性别
        /// </summary>
        int Sex { get; set; }

        /// <summary>生日：格式yyyy-MM-dd
        /// </summary>
        string Birthday { get; set; }

        /// <summary>
        /// 客户的业务相关的属性（例如）
        /// </summary>
        SerializableMap<string, string> Details { get; set; }
    }
}