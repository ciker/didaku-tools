using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    public interface IBusinessType<TB> : IEquatable<IBusinessType<TB>>, ICloneable
        where TB : IBusinessTypeCategory<TB>
    {
        [Key]
        Guid Id { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        ICollection<WorkRange> WorkRanges { get; set; }
        
        /// <summary>父级业务类型分类</summary>
        /// <value>
        /// The parent id.
        /// </value>
        TB Category { get; set; }

        /// <summary>
        /// 业务类型的其他相关属性信息集合
        /// </summary>
        SerializableMap<string, string> Details { get; set; }
    }
}