﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Interfaces.Entities
{
    public interface ICounter<TDistrict, TDepartment>
        where TDepartment : IDepartment
        where TDistrict : IDistrict<TDepartment> 
    {
        [Key]
        Guid Id { get; set; }

        /// <summary>名称。一般是用来显示描述该实体的简称。
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>营业厅分区
        /// </summary>
        TDistrict District { get; set; }

        /// <summary>
        /// 柜台Ip地址格式如 "127.0.0.1"
        /// </summary>
        string IpAddress { get; set; }

        /// <summary>柜台的其他相关属性信息集合
        /// </summary>
        SerializableMap<string, string> Details { get; set; }
    }
}