﻿namespace Didaku.QEngine.Common.Interfaces
{
    /// <summary>描述一个实体的最基本的信息
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IQEntity<T>
    {
        T Id { get; set; }

        /// <summary>名称
        /// </summary>
        string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        string Number { get; set; }
    }
}