using Didaku.Collections;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Common.Interfaces
{
    /// <summary>
    ///     网点的配置项集合
    /// </summary>
    public interface IDepositories
    {
        /// <summary>
        ///     当前网点的分区字典。
        /// </summary>
        SerializableMap<string, District> DistrictMap { get; set; }

        /// <summary>
        ///     当前网点的柜台字典。
        /// </summary>
        SerializableMap<string, Counter> CounterMap { get; }

        /// <summary>
        ///     当前网点的业务类型字典。
        /// </summary>
        SerializableMap<string, BusinessType> BusinessTypeMap { get; set; }

        /// <summary>
        ///     当前网点的员工字典。
        /// </summary>
        SerializableMap<string, Staff> StaffMap { get; set; }

        /// <summary>
        ///     当前网点的工作流字典。
        /// </summary>
        SerializableMap<string, Workflow> WorkflowMap { get; set; }

        /// <summary>
        ///     当前网点的卡片类别字典。
        /// </summary>
        SerializableMap<string, CardCategory> CardCategoryMap { get; set; }

        /// <summary>
        ///     当前网点的服务队列字典。Key是队列的Id。
        /// </summary>
        SerializableMap<string, ServiceQueue> ServiceQueueMap { get; set; }

        /// <summary>
        ///     当前网点的服务逻辑字典。Key是柜台的Id，Value指的是服务逻辑。
        /// </summary>
        SerializableMap<string, ServiceLogic> ServiceLogicMap { get; set; }
    }
}