using Didaku.QEngine.Common.Abstracts.Entities;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>交易所涉及的卡片的存储与获取服务
    /// </summary>
    public interface ICardService
    {
        /// <summary>是否需要持久化
        /// </summary>
        /// <value>
        ///   <c>true</c> if [require persistence]; otherwise, <c>false</c>.
        /// </value>
        bool RequirePersistence { get; }

        /// <summary>尝试根据指定的卡片获取卡片
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="cardInfo">The card info.</param>
        /// <returns></returns>
        bool TryGet(string number, out CardInfo cardInfo);

        /// <summary>增加一张卡片，如果该卡片已存在，将更新
        /// </summary>
        /// <param name="cardInfo">卡片.</param>
        /// <param name="needUpdate">当卡片已存在时，是否需要更新</param>
        /// <returns></returns>
        CardInfo AddOrUpdate(CardInfo cardInfo, bool needUpdate);
    }
}