using System.Collections.Generic;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>描述调整交易与队列方法的接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IAdjustFunc<T>
    {
        /// <summary>为调整交易与队列的动作准备的条件
        /// </summary>
        /// <value>The params.</value>
        /// <remarks></remarks>
        T Condition { get; set; }

        /// <summary>执行调整交易与队列的动作
        /// </summary>
        /// <returns></returns>
        bool Execute(IDictionary<string, ServiceLogic> logicMap);
    }
}