using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>描述营业厅的一个窗口(柜台)服务队列的组，这个组里将包含一个或多个队列。它将被包括在<see cref="ServiceLogic"/>中。
    /// </summary>
    public interface IServiceQueueGroup : IList<ServiceQueue>, IXmlSerializable, IEquatable<ServiceQueueGroup>
    {
        /// <summary>队列组的识别名
        /// </summary>
        string Name { get; set; }

        /// <summary>尝试从队列组中分配一个交易
        /// </summary>
        /// <param name="transaction">输出:一个排队的交易</param>
        /// <returns>如果有可用的交易，返回真。反之，返回否。</returns>
        bool TryAssign(out Transaction transaction);

        /// <summary>判断队列组是否激活。(即俗称的备用队列的激活条件)
        /// </summary>
        bool IsActive();
    }
}