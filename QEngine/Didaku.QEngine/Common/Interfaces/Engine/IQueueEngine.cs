﻿using Didaku.QEngine.Common.Abstracts.Entities;
using Didaku.QEngine.Common.Entities;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>一个描述排队系统队列模型引擎的接口
    /// </summary>
    /// <remarks></remarks>
    public interface IQueueEngine
    {
        /// <summary>初始化
        /// </summary>
        /// <param name="department">网点基础信息.</param>
        bool Initialize(DepartmentInfo department);

        /// <summary>统计人数服务
        /// </summary>
        ICountService CountService { get; }

        /// <summary>卡片信息存储服务器
        /// </summary>
        ICardService CardService { get; }

        /// <summary>根据指定的队列生成交易
        /// </summary>
        /// <param name="transaction">输出:生成的交易.</param>
        /// <param name="serviceQueueId">指定的队列的id.</param>
        /// <returns>是否生成成功，当未成功时，交易为Null</returns>
        bool Generate(out Transaction transaction, string serviceQueueId);

        /// <summary>根据指定的呼叫方法分配一条交易
        /// </summary>
        /// <param name="transaction">输出:获取的交易.</param>
        /// <param name="assignFunc">指定的分配方法.</param>
        /// <returns>是否获取成功，当未成功时，交易为Null</returns>
        bool Assign<T>(out Transaction transaction, IAssignFunc<T> assignFunc);

        /// <summary>修改指定的交易的相关属性。如:弃号，插前，延后
        /// </summary>
        /// <param name="modifyFunc">指定的修改方法.</param>
        /// <returns>是否生成成功，当未成功时，交易为Null</returns>
        bool Modify<T>(IModifyFunc<T> modifyFunc);

        /// <summary>调整指定的交易与队列之间关系。如转移交易到另外的队列。
        /// </summary>
        /// <param name="adjustFunc">The service queue id.</param>
        /// <returns>是否调整成功</returns>
        bool Adjust<T>(IAdjustFunc<T> adjustFunc);

        /// <summary>根据指定的删除筛选条件从队列中移除交易
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="deleteFunc">The delete condition.</param>
        /// <returns>是否删除成功</returns>
        bool Delete<T>(IDeleteFunc<T> deleteFunc);

        /// <summary>调整柜台间的逻辑（合岗，换岗等）
        /// </summary>
        /// <param name="changeFunc">The counters.</param>
        /// <returns>是否调整成功</returns>
        bool ChangeLogic<T>(IChangeLogicFunc<T> changeFunc);
    }
}