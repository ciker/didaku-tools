using System.Collections.Generic;
using Didaku.QEngine.Common.Counts;
using Didaku.QEngine.Common.Entities;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    public interface ICountService
    {
        /// <summary>该引擎对应的柜台等待人数的集合
        /// </summary>
        CountMap Counters { get; }

        /// <summary>该引擎对应的所有队列等待人数的集合
        /// </summary>
        CountMap Queues { get; }

        /// <summary>该引擎对应的所有队列元素的集合
        /// </summary>
        CountMap Elements { get; }

        /// <summary>启动服务
        /// </summary>
        void Start(IEnumerable<IServiceQueue<Transaction>> queues);

        /// <summary>关闭服务
        /// </summary>
        void Close();
    }
}