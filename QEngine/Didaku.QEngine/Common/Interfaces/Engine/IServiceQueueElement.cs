namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>一个描述指定类型可做一个排队队列的元素。如业务类型、客户类别均可以做为队列的元素。
    /// 如果一个指定的类型将做为排队队列的元素，他需要实现本接口。
    /// </summary>
    public interface IServiceQueueElement
    {
        /// <summary>获得本元素的ID
        /// </summary>
        /// <returns></returns>
        string GetId();

        /// <summary>队列的元素是否是可用状态。如，业务类型可能不在工作时间范围之内。
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is actived; otherwise, <c>false</c>.
        /// </returns>
        bool IsActived();
    }
}