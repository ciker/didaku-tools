using System;
using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Common.Interfaces.Engine
{
    /// <summary>描述一个窗口(柜台)的服务队列逻辑。它由多个【服务队列组】组成。[队列组]的优先级由[队列组]在List中的顺序决定。
    /// </summary>
    /// <remarks></remarks>
    public interface IServiceLogic : IList<ServiceQueueGroup>, ICloneable
    {
        /// <summary>该逻辑所属的柜台
        /// </summary>
        /// <value>
        /// The counter.
        /// </value>
        Counter Counter { get; set; }
    }
}