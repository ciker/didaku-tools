namespace Didaku.QEngine.Common.Interfaces
{
    /// <summary>排队管理引擎的数据持久化
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IQEnginePersistence<T>
    {
        /// <summary>是否异步持久化
        /// </summary>
        bool IsAync { get; set; }

        int Add(params T[] trans);
        int Update(params T[] trans);
        int Remove(params T[] trans);

        /// <summary>搜索指定队列当天的交易
        /// </summary>
        /// <param name="queueId">The queue id.</param>
        /// <returns></returns>
        T[] Select(string queueId);

        /// <summary>备份指定队列的号票生成计数
        /// </summary>
        void Backup(string serviceQueueId, int currCount);
    }
}