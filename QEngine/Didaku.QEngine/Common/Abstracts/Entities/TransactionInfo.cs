using System;
using System.ComponentModel.DataAnnotations;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Enums;
using Didaku.QEngine.Common.Interfaces.Entities;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    [Serializable]
    public abstract class TransactionInfo
        : ITransaction<BusinessTypeCategoryInfo, CustomerInfo, DepartmentInfo, BusinessTypeInfo, CounterInfo, StaffInfo, CardInfo, CardCategoryInfo, WorkflowInfo, DistrictInfo>,
          IEquatable<ITransaction<BusinessTypeCategoryInfo, CustomerInfo, DepartmentInfo, BusinessTypeInfo, CounterInfo, StaffInfo, CardInfo, CardCategoryInfo, WorkflowInfo, DistrictInfo>>
    {
        #region IEquatable<ITransaction<BusinessTypeCategoryInfo,CustomerInfo,DepartmentInfo,BusinessTypeInfo,CounterInfo,StaffInfo,CardInfo,CardKindInfo,WorkflowInfo,DistrictInfo>> Members

        public bool Equals(ITransaction<BusinessTypeCategoryInfo, CustomerInfo, DepartmentInfo, BusinessTypeInfo, CounterInfo, StaffInfo, CardInfo, CardCategoryInfo, WorkflowInfo, DistrictInfo> other)
        {
            return other.Id.Equals(Id) && other.TicketNumber.Equals(TicketNumber);
        }

        #endregion

        #region ITransaction<BusinessTypeCategoryInfo,CustomerInfo,DepartmentInfo,BusinessTypeInfo,CounterInfo,StaffInfo,CardInfo,CardKindInfo,WorkflowInfo,DistrictInfo> Members

        /// <summary>全局唯一的ID，采用GUID，以保证无论多少个网点，交易的编号都是不发生重复的
        /// </summary>
        /// <value>The id.</value>
        [Key]
        [EntityColumn]
        public string Id { get; set; }

        /// <summary>排队号码
        /// </summary>
        [EntityColumn]
        public string TicketNumber { get; set; }

        /// <summary>交易所关系的网点编号
        /// </summary>
        [EntityColumn]
        public DepartmentInfo Department { get; set; }

        /// <summary>排队交易类型
        /// </summary>
        [EntityColumn]
        public BusinessTypeInfo BusinessType { get; set; }

        /// <summary>
        /// 当交易开始办理时的柜台
        /// </summary>
        [EntityColumn]
        public CounterInfo Counter { get; set; }

        /// <summary> CustomerInfo的键值，取决于客户取号的方式，如果是手工点击按键取号则为空
        /// 刷卡取号则为卡号，刷二代证取号则为身份证号，输入电话号码取号则为电话号码
        /// </summary>
        [EntityColumn]
        public CardInfo Card { get; set; }

        /// <summary>工作流
        /// </summary>
        /// <value>
        /// The workflow.
        /// </value>
        [EntityColumn]
        public WorkflowInfo Workflow { get; set; }

        /// <summary>
        /// 当交易开始办理时的员工
        /// </summary>
        [EntityColumn]
        public StaffInfo Staff { get; set; }

        /// <summary> 发起请评价的时间
        /// </summary>
        [EntityColumn]
        public virtual DateTime? AskEvalTime { get; set; }

        /// <summary> 呼叫时间
        /// </summary>
        [EntityColumn]
        public virtual DateTime? CallTime { get; set; }

        /// <summary> 取号时间
        /// </summary>
        [EntityColumn]
        public virtual DateTime? GetNumberTime { get; set; }

        /// <summary> 服务结束时间-呼叫下一个号码时、弃号、退出等动作均会产生结束时间
        /// </summary>
        [EntityColumn]
        public virtual DateTime? TransactionEndTime { get; set; }

        /// <summary>评价结果
        /// </summary>
        [EntityColumn]
        public virtual short? EvaluationType { get; set; }

        /// <summary> 取号方式，包括手工取号，刷卡取号，刷二代证取号等等
        /// </summary>
        [EntityColumn]
        public NumberGenerationType NumberGenerationType { get; set; }

        /// <summary> 详细信息
        /// </summary>
        [EntityColumn]
        public SerializableMap<string, string> Details { get; set; }

        #endregion

        public bool Equals(TransactionInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (TransactionInfo)) return false;
            return Equals((TransactionInfo) obj);
        }

        public override int GetHashCode()
        {
            return (Id.GetHashCode());
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", TicketNumber, BusinessType, Card, Id);
        }

        public static bool operator ==(TransactionInfo left, TransactionInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TransactionInfo left, TransactionInfo right)
        {
            return !Equals(left, right);
        }
    }
}