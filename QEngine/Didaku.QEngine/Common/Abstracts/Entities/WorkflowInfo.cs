﻿using System;
using Didaku.Attributes;
using Didaku.QEngine.Common.Entities.Collections;
using Didaku.QEngine.Common.Interfaces.Entities;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    [Serializable]
    public abstract class WorkflowInfo : IWorkflow, IEquatable<WorkflowInfo>
    {
        protected WorkflowInfo()
        {
            Chain = new WorkflowNodeCollection();
        }

        #region Implementation of IWorkflow

        [EntityColumn]
        public string Id { get; set; }

        /// <summary>名称
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        [EntityColumn]
        public WorkRangeCollection Ranges { get; set; }

        /// <summary>
        /// 工作流描述
        /// </summary>
        [EntityColumn]
        public string Description { get; set; }

        /// <summary>工作流节点链
        /// </summary>
        [EntityColumn]
        public WorkflowNodeCollection Chain { get; set; }

        #endregion

        #region IEquatable<WorkflowInfo> Members

        public bool Equals(WorkflowInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id.Equals(Id);
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (WorkflowInfo)) return false;
            return Equals((WorkflowInfo) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(WorkflowInfo left, WorkflowInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(WorkflowInfo left, WorkflowInfo right)
        {
            return !Equals(left, right);
        }
    }
}