﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Interfaces.Entities;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>描述柜台信息的实体类
    /// </summary>
    [Serializable]
    public abstract class CounterInfo : ICounter<DistrictInfo, DepartmentInfo>, IEquatable<ICounter<DistrictInfo, DepartmentInfo>>
    {
        /// <summary>构造函数（给窗口编号赋值）
        /// </summary>
        protected CounterInfo()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>柜台的全局ID。当取号系统在集中管理状态下保证柜台的唯一性。
        /// </summary>
        [Key]
        [EntityColumn]
        public Guid Id { get; set; }

        /// <summary>柜台编号
        /// </summary>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        public ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>柜台名称
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>营业厅分区ID
        /// </summary>
        [EntityColumn]
        public DistrictInfo District { get; set; }

        /// <summary>
        /// 柜台Ip地址格式如 "127.0.0.1"
        /// </summary>
        [EntityColumn]
        public string IpAddress { get; set; }

        /// <summary>柜台的其他相关属性信息集合
        /// </summary>
        [EntityColumn]
        public SerializableMap<string, string> Details { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("(");
            sb.Append(Number);
            sb.Append(")");
            sb.Append(!string.IsNullOrWhiteSpace(Name) ? Name : " 柜台");
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (CustomerInfo)) return false;
            return Equals((ICounter<DistrictInfo, DepartmentInfo>) obj);
        }

        public bool Equals(ICounter<DistrictInfo, DepartmentInfo> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(CounterInfo left, CounterInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CounterInfo left, CounterInfo right)
        {
            return !Equals(left, right);
        }

        /*
public XmlElement ToXml(XmlDocument doc)
{
    XmlElement ele = doc.CreateElement("CounterInfo");
    ele.AppendChild(BuildElement(doc, "CounterIndex", CounterIndex));
    ele.AppendChild(BuildElement(doc, "CounterName", CounterName));
    ele.AppendChild(BuildElement(doc, "DistrictId", DistrictId));
    ele.AppendChild(BuildElement(doc, "CounterViewNumber", CounterViewNumber));
    return ele;
}

public void Parse(XmlElement element)
{
    foreach (XmlNode node in element.ChildNodes)
    {
        if (node.NodeType != XmlNodeType.Element)
            continue;
        if (node.Name == "CounterIndex")
            CounterIndex = int.Parse(node.InnerText.Trim());
        else if (node.Name == "CounterName")
            CounterName = node.InnerText.Trim();
        else if (node.Name == "DistrictId")
            DistrictId = Convert.ToInt16(node.InnerText.Trim());
        else if (node.Name == "CounterViewNumber")
            CounterViewNumber = node.InnerText.Trim();
        else
        {
            throw new ArgumentException(node.InnerText);
        }
    }
}

protected static XmlElement BuildElement(XmlDocument doc, string localname, object value)
{
    XmlElement ele = doc.CreateElement(localname);
    if (value != null)
        ele.InnerText = value.ToString();
    return ele;
}
 */

    }
}