﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Didaku.QEngine.Common.Interfaces.Entities;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>
    /// 标识一个包括多个业务类型的业务组，它是树状的，树的关系是由它的 ParentId 是否为空来决定的。
    /// </summary>
    [Serializable]
    public abstract class BusinessTypeCategoryInfo : IBusinessTypeCategory<BusinessTypeCategoryInfo>, IEquatable<BusinessTypeCategoryInfo>
    {
        protected BusinessTypeCategoryInfo()
        {
            Id = Guid.NewGuid();
        }

        #region IBusinessTypeCategory<BusinessTypeCategoryInfo> Members

        [Key]
        public Guid Id { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        public ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public BusinessTypeCategoryInfo Parent { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        public string Number { get; set; }

        #endregion

        #region IEquatable<BusinessTypeCategoryInfo> Members

        public bool Equals(BusinessTypeCategoryInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id) && Equals(other.Parent, Parent);
        }

        #endregion

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Name);
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (BusinessTypeCategoryInfo)) return false;
            return Equals((BusinessTypeCategoryInfo) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Id != null ? Id.GetHashCode() : 0)*397) ^ (Parent != null ? Parent.GetHashCode() : 0);
            }
        }

        public static bool operator ==(BusinessTypeCategoryInfo left, BusinessTypeCategoryInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BusinessTypeCategoryInfo left, BusinessTypeCategoryInfo right)
        {
            return !Equals(left, right);
        }
    }
}