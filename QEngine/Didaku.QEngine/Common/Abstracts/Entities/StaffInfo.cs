﻿using System;
using System.Collections.Generic;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Interfaces.Entities;
using Didaku.QEngine.Common.Wrapper;
using Didaku.Wrapper;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>描述一个员工
    /// </summary>
    [Serializable]
    public abstract class StaffInfo : IStaff<DepartmentInfo>, IEquatable<StaffInfo>
    {
        /// <summary>
        /// 全局唯一的Id，以Guid表示。
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [EntityColumn]
        public Guid Id { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        public ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>隶属机构
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        [EntityColumn]
        public DepartmentInfo Department { get; set; }

        /// <summary>
        /// 编号（排队系统中的编号）。排队系统中将使用该属性进行登录操作。
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [EntityColumn]
        public string Password { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        /// <value>
        /// The sex.
        /// </value>
        [EntityColumn]
        public PersonSex Sex { get; set; }

        /// <summary>
        /// 本员工在系统中的创建时间
        /// </summary>
        /// <value>
        /// The created time.
        /// </value>
        [EntityColumn]
        public DateTime? CreatedTime { get; set; }

        /// <summary>
        /// 员工的其他相关属性信息集合
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        [EntityColumn]
        public SerializableMap<string, string> Details { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Number, Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (StaffInfo)) return false;
            return Equals((StaffInfo) obj);
        }

        public bool Equals(StaffInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(StaffInfo left, StaffInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(StaffInfo left, StaffInfo right)
        {
            return !Equals(left, right);
        }
    }
}

//<WinUser>
//  <UserID>1004</UserID>
//  <UserName>朱红</UserName>
//  <UserClass>5</UserClass>
//</WinUser>