﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Interfaces.Entities;
using Didaku.QEngine.Common.Wrapper;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    [Serializable]
    public abstract class DistrictInfo : IDistrict<DepartmentInfo>
    {
        [Key]
        public Guid Id { get; set; }

        /// <summary>工作时间段集合
        /// </summary>
        public ICollection<WorkRange> WorkRanges { get; set; }

        /// <summary>隶属机构
        /// </summary>
        /// <value>
        /// The department.
        /// </value>
        public DepartmentInfo Department { get; set; }

        public string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        public string Number { get; set; }

        /// <summary>分区的其他相关属性信息集合
        /// </summary>
        public SerializableMap<string, string> Details { get; set; }

        #region IEquatable<DistrictInfo> Members

        public bool Equals(DistrictInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Id, Id) && other.Number == Number;
        }

        #endregion

        public override string ToString()
        {
            return Number.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (DistrictInfo)) return false;
            return Equals((DistrictInfo) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (!Id.Equals(Guid.Empty) ? Id.GetHashCode() : 0);
                result = (result*397) ^ Number.GetHashCode();
                result = (result*397) ^ (Name != null ? Name.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(DistrictInfo left, DistrictInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DistrictInfo left, DistrictInfo right)
        {
            return !Equals(left, right);
        }

        public static SerializableMap<int, DistrictInfo> DistrictsParserFunc(object obj)
        {
            var districtsParserFunc = obj as SerializableMap<int, DistrictInfo>;
            return districtsParserFunc;
        }
    }
}