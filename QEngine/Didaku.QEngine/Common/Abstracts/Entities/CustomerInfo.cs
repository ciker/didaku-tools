﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using Didaku.Attributes;
using Didaku.Collections;
using Didaku.QEngine.Common.Interfaces.Entities;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>客户实体类
    /// 客户检测到后，经识别后产生，客户办完业务离开后，该信息应从字典中删除
    /// </summary>
    [Serializable]
    public abstract class CustomerInfo : ICustomer, IEquatable<CustomerInfo>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        protected CustomerInfo()
        {
            Id = Guid.NewGuid();
            Details = new SerializableMap<string, string>();
        }

        #region ICustomerInfo Members

        /// <summary>
        /// 主键，构造函数中自动赋值
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>客户姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>客户性别
        /// </summary>
        public int Sex { get; set; }

        /// <summary>生日：格式yyyy-MM-dd
        /// </summary>
        public string Birthday { get; set; }

        /// <summary>其他信息
        /// </summary>
        [XmlIgnore]
        [EntityColumn]
        public SerializableMap<string, string> Details { get; set; }

        #endregion

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (CustomerInfo)) return false;
            return Equals((CustomerInfo) obj);
        }

        public bool Equals(CustomerInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(CustomerInfo left, CustomerInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CustomerInfo left, CustomerInfo right)
        {
            return !Equals(left, right);
        }
    }
}

