using System;
using System.ComponentModel.DataAnnotations;
using Didaku.Attributes;
using Didaku.QEngine.Common.Interfaces;
using Didaku.QEngine.Common.Interfaces.Entities;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>卡片(客户)分类。
    /// 我们理解：在排队通过卡片进行的校验或识别，均得到的是卡片的分类，并被引用为客户的分类属性。
    /// 在排队系统中，鉴于一般很难真正获取到较完整的客户的属性，“客户分类”这个概念被移到CardInfo中。
    /// </summary>
    [Serializable]
    public abstract class CardCategoryInfo : ICardCategory, IQEntity<string>, IEquatable<ICardCategory>
    {
        #region ICardCategory Members

        [EntityColumn]
        public string Id { get; set; }

        /// <summary>名称
        /// </summary>
        [EntityColumn]
        public string Name { get; set; }

        /// <summary>助记编号（单网点唯一）
        /// </summary>
        [EntityColumn]
        public string Number { get; set; }

        /// <summary>卡片(客户)分类的索引属性。如:1星级。
        /// </summary>
        [Key]
        [EntityColumn]
        public string Category { get; set; }

        [EntityColumn]
        public string CallHead { get; set; }

        /// <summary>客户类别描述。
        /// </summary>
        [EntityColumn]
        public string Description { get; set; }

        #endregion

        #region ToString Equals GetHashCode

        public bool Equals(ICardCategory other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Category, Category);
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Category, Description);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (CardCategoryInfo)) return false;
            return Equals((CardCategoryInfo) obj);
        }

        public override int GetHashCode()
        {
            return Category.GetHashCode();
        }

        #endregion
    }
}