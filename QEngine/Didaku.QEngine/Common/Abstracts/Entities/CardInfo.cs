﻿using System;
using System.ComponentModel.DataAnnotations;
using Didaku.Collections;
using Didaku.QEngine.Common.Enums;
using Didaku.QEngine.Common.Interfaces.Entities;

namespace Didaku.QEngine.Common.Abstracts.Entities
{
    /// <summary>客户实体类
    /// 客户检测到后，经识别后产生，客户办完业务离开后，该信息应从字典中删除
    /// </summary>
    [Serializable]
    public abstract class CardInfo : ICard<CustomerInfo, CardCategoryInfo>
    {
        #region ICardInfo Members

        /// <summary>
        /// 客户来时输入的身份识别信息，如卡号，折号，身份证号，手机号，
        /// CardNumber是CardInfo的主键
        /// </summary>
        [Key]
        public string CardNumber { get; set; }

        /// <summary>
        /// 客户所持卡片类型
        /// </summary>
        public abstract CardType CardType { get; }

        /// <summary>卡片分类(客户分类)
        /// </summary>
        /// <value>
        /// The kind of the card.
        /// </value>
        public CardCategoryInfo Category { get; set; }

        /// <summary>
        /// 卡的拥有者
        /// </summary>
        public CustomerInfo Customer { get; set; }

        /// <summary>
        /// 除卡号的额外信息，以键值对的形式存储，不同的卡对应的
        /// 身份证:包含姓名，身份证，地址等等
        /// 磁卡：包含二磁道，三磁道，一磁道
        /// </summary>
        public SerializableMap<string, string> Details { get; set; }

        #endregion
    }
}