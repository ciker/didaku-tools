﻿using System.Collections.Generic;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.ChangeLogic
{
    /// <summary>换岗：将本柜台的逻辑替换为指定柜台的柜台逻辑
    /// </summary>
    internal class ReplaceWithTargetCounter : IChangeLogicFunc<Pair<string, string>>
    {
        #region Implementation of IChangeLogicFunc<Pair<Guid,Guid>>

        /// <summary>逻辑调整时的条件参数:当前柜台(First),目标柜台(Second).
        /// </summary>
        public Pair<string, string> Condition { get; set; }

        /// <summary>执行逻辑调整的动作
        /// </summary>
        /// <param name="sourceLogics">供参考的逻辑库的源</param>
        /// <param name="targetLogics">需调整的逻辑库</param>
        /// <returns></returns>
        public bool Execute(IDictionary<string, ServiceLogic> sourceLogics, IDictionary<string, ServiceLogic> targetLogics)
        {
            var curr = Condition.First; //当前柜台
            var target = Condition.Second; //目标柜台
            targetLogics.Remove(curr);
            targetLogics.Add(curr, sourceLogics[target]);
            return true;
        }

        #endregion
    }
}