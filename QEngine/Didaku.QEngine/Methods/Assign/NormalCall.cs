using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.Assign
{
    /// <summary>普通呼叫。
    /// </summary>
    internal class NormalCall : IAssignFunc<object>
    {
        #region Implementation of ICallParams<in string>

        /// <summary>发起呼叫(提取交易)的柜台的ID
        /// </summary>
        /// <value>The counter.</value>
        /// <remarks></remarks>
        public Counter Counter { get; set; }

        /// <summary>发起呼叫(提取交易)的员工
        /// </summary>
        /// <value>
        /// The staff.
        /// </value>
        public Staff Staff { get; set; }

        /// <summary>呼叫时的条件参数
        /// </summary>
        /// <value>The params.</value>
        /// <remarks></remarks>
        public object Condition { get; set; }

        /// <summary>从一个柜台的服务逻辑中获取交易的方法
        /// </summary>
        /// <returns></returns>
        public bool Execute(out Transaction transaction, ServiceLogic logic, ICollection<ServiceQueue> queues)
        {
            foreach (ServiceQueueGroup group in logic)
            {
                if (group.IsActive() && group.TryAssign(out transaction))
                    return true;
            }
            transaction = null;
            return false;
        }

        #endregion
    }
}