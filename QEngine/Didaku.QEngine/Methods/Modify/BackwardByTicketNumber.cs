﻿using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.Modify
{
    /// <summary>延后指定的号票
    /// </summary>
    internal class BackwardByTicketNumber : IModifyFunc<string>
    {
        #region Implementation of IModifyFunc<string>

        /// <summary>指定的号票
        /// </summary>
        public string Condition { get; set; }

        /// <summary>延后指定的号票
        /// </summary>
        /// <param name="queues"> </param>
        /// <returns></returns>
        public bool Execute(ICollection<ServiceQueue> queues)
        {
            foreach (ServiceQueue queue in queues)
            {
                foreach (Transaction tran in queue)
                {
                    if (tran.TicketNumber.Equals(Condition))
                    {
                        tran.Backward();
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}