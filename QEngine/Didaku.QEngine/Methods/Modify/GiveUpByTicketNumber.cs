﻿using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Enums;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.Modify
{
    internal class GiveUpByTicketNumber : IModifyFunc<string>
    {
        #region Implementation of IModifyFunc<string>

        /// <summary>为调整交易动作准备的条件
        /// </summary>
        /// <value>The params.</value>
        /// <remarks></remarks>
        public string Condition { get; set; }

        /// <summary>执行调整交易动作方法
        /// </summary>
        /// <param name="queues"> </param>
        /// <returns></returns>
        public bool Execute(ICollection<ServiceQueue> queues)
        {
            foreach (ServiceQueue queue in queues)
            {
                foreach (Transaction tran in queue)
                {
                    if (tran.TicketNumber.Equals(Condition))
                    {
                        tran.TransactionStatus = TransactionStatus.GivenUp;
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion
    }
}