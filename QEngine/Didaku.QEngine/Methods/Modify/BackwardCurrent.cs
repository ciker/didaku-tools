﻿using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.Modify
{
    /// <summary>将当前交易的取号时间延后
    /// </summary>
    internal class BackwardCurrent : IModifyFunc<Transaction>
    {
        #region Implementation of IModifyFunc<Transaction>

        /// <summary>为调整交易动作准备的条件
        /// </summary>
        /// <value>The params.</value>
        /// <remarks></remarks>
        public Transaction Condition { get; set; }

        /// <summary>执行调整交易动作方法
        /// </summary>
        /// <returns></returns>
        public bool Execute(ICollection<ServiceQueue> queues)
        {
            Condition.Backward();
            return true;
        }

        #endregion
    }
}