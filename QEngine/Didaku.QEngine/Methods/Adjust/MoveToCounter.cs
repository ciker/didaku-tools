﻿using System.Collections.Generic;
using Didaku.QEngine.Common.Entities;
using Didaku.QEngine.Common.Extensions;
using Didaku.QEngine.Common.Interfaces.Engine;
using Didaku.QEngine.Common.Wrapper.Engine;

namespace Didaku.QEngine.Methods.Adjust
{
    /// <summary>转移指定交易(Left)转移到目标柜台中去(Right)
    /// </summary>
    /// <remarks></remarks>
    internal class MoveToCounter : IAdjustFunc<Pair<Transaction, Counter>>
    {
        #region Implementation of IAdjustFunc<Pair<Transaction, ServiceLogic>>

        /// <summary>为调整交易与队列的动作准备的参数
        /// </summary>
        /// <value>The params.</value>
        /// <remarks></remarks>
        public Pair<Transaction, Counter> Condition { get; set; }

        /// <summary>执行调整交易与队列的动作
        /// </summary>
        /// <returns></returns>
        public bool Execute(IDictionary<string, ServiceLogic> logicMap)
        {
            Transaction transaction = Condition.First;
            ServiceLogic logic = logicMap[Condition.Second.Id];
            if (transaction != null)
            {
                logic.GetSuperQueue().Add(transaction, false); //将指定的交易添加到指定的柜台的超级队列中
                return true;
            }
            return false;
        }

        #endregion
    }
}