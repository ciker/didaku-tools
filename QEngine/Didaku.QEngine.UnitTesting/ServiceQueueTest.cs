﻿using System.IO;
using System.Text;
using Didaku.QEngine.Common.Wrapper.Engine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Serialization;

namespace Didaku.QEngine.UnitTesting
{
    [TestClass()]
    public class ServiceQueueTest
    {
        #region MyRegion
        
        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        // 
        //编写测试时，还可使用以下特性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion

        [TestMethod()]
        public void ReadXmlTest()
        {
            var queue = new ServiceQueue()
            {
                Id = Guid.NewGuid().ToString(),
                CallHead = "A",
                Name = "现金客户1级",
                TicketStartNumber = 1,
                TicketEndNumber = 999,
                TicketLength = 4,
                TicketMaxCount = 500
            };

            var sb = new StringBuilder();
            var serializer = new XmlSerializer(typeof(ServiceQueue));
            var writer = new StringWriter(sb);
            serializer.Serialize(writer, queue);

            Console.WriteLine(sb.ToString());

            var result = sb.ToString();
            var sr = new StringReader(result);
            object newQueue = serializer.Deserialize(sr);
            Assert.AreEqual(queue, newQueue);
        }
    }
}
