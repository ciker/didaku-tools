using Didaku.Engine.Timeaxis.Data;
using Didaku.Engine.Timeaxis.Data.Base;
using Didaku.Engine.Timeaxis.Kernel.IoC;
using NLog;
using NLog.Targets;

namespace Didaku.Engine.Timeaxis.Kernel.Common
{
    class MongoDatabaseTarget : TargetWithLayout
    {
        protected override void Write(LogEventInfo logEvent)
        {
            var logStore = Core.Singleton<Datas>().Logs;
            if (logStore != null)
                logStore.Add(LogInfo.Parse(logEvent));
        }
    }
}