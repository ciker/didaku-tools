﻿using System;
using NLog;

namespace Didaku.Engine.Timeaxis.Data.Base
{
    /// <summary>对应NLog的日志体对象的一个简单对象。
    /// </summary>
    public class LogInfo
    {
        private static int _SequenceNumber = 1;

        public LogInfo()
        {
            Stack = string.Empty;
        }

        public long Id { get; set; }
        public string Info { get; set; }
        public string Src { get; set; }
        public string Stack { get; set; }
        public string Level { get; set; }

        /// <summary>将NLog的日志体对象转换成一个简单对象以方便向数据库存储。
        /// </summary>
        /// <param name="src">NLog的日志体对象</param>
        /// <returns>简单对象</returns>
        public static LogInfo Parse(LogEventInfo src)
        {
            var log = new LogInfo
            {
                Id = src.TimeStamp.Ticks + _SequenceNumber,
                Info = src.FormattedMessage,
                Level = src.Level.Name
            };
            log.Src = src.LoggerName.Substring(src.LoggerName.LastIndexOf('.') + 1);
            if (_SequenceNumber >= 1000)
                _SequenceNumber = 0;
            _SequenceNumber++;
            if (src.HasStackTrace)
                log.Stack = src.StackTrace.ToString();
            return log;
        }
    }
}
