using Didaku.Engine.Timeaxis.Base.Interfaces;

namespace Didaku.Engine.Timeaxis.Implement.Abstracts
{
    /// <summary>预约的动作参数
    /// </summary>
    public abstract class BaseBookingActiveParams : BaseActiveParams
    {
    }
}