﻿using System.Web.Mvc;

namespace Didaku.Engine.Timeaxis.Site.Manager
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}