﻿using System.Web.Mvc;

namespace Didaku.Engine.Timeaxis.Site.Manager
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
