﻿using Didaku.Engine.Timeaxis.Implement.Abstracts;

namespace Didaku.Engine.Timeaxis.Implement.Industry.Bank
{
    /// <summary>在银行排队中基于取号机取票的交易信息
    /// </summary>
    public class TicketByQueueMachineTransaction : BaseBookingTransaction
    {
    }
}