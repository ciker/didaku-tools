﻿using System;
using Jeelu.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jeelu.UnitTest
{
    [TestClass]
    public class GreatestCommonDivisorTest
    {
        [TestMethod]
        public void GetGCDTestMethod1()
        {
            var gcd = new GreatestCommonDivisor();
            gcd.AddToArray(600,100,200,300);

            Assert.AreEqual(100, gcd.GetGCD());
        }

        [TestMethod]
        public void GetGCDTestMethod2()
        {
            var gcd = new GreatestCommonDivisor();
            gcd.AddToArray(3452236, 45623562, 234254, 8478568);
            Assert.AreEqual(2, gcd.GetGCD());
        }

        [TestMethod]
        public void GetGCDTestMethod3()
        {
            var gcd = new GreatestCommonDivisor();
            gcd.AddToArray(1131503, 1094103, 109803, 1104286);
            Assert.AreEqual(17, gcd.GetGCD());
        }

        [TestMethod]
        public void GetGCDTestMethod4()
        {
            var gcd = new GreatestCommonDivisor();
            gcd.AddToArray(234, 456, 5678, 3456, 23456, 1452, 674, 25456, 7858);
            Assert.AreEqual(2, gcd.GetGCD());
        }

    }
}
