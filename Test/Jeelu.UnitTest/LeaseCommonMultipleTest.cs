﻿using System;
using Jeelu.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jeelu.UnitTest
{
    [TestClass]
    public class LeaseCommonMultipleTest
    {
        [TestMethod]
        public void GetLCMTestMethod1()
        {
            var lcm = new LeaseCommonMultiple();
            lcm.AddToArray(2,3,4);
            Assert.AreEqual(12, lcm.GetLCM());
        }

        [TestMethod]
        public void GetLCMTestMethod2()
        {
            var lcm = new LeaseCommonMultiple();
            lcm.AddToArray(8, 32, 4);
            Assert.AreEqual(32, lcm.GetLCM());
        }

        [TestMethod]
        public void GetLCMTestMethod3()
        {
            var lcm = new LeaseCommonMultiple();
            lcm.AddToArray(8, 36, 4);
            Assert.AreEqual(72, lcm.GetLCM());
        }
    }
}
