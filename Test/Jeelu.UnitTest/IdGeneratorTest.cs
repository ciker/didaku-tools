﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Jeelu.Math;
using Jeelu.Wrapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jeelu.UnitTest
{
    [TestClass]
    public class IdGeneratorTest
    {
        private readonly IdGenerator _Generator = new IdGenerator();

        [TestMethod]
        public void GenerateTest()
        {
            var id = _Generator.Generate();
            Assert.IsNotNull(id);
        }

        [TestMethod]
        public void GenerateTimeTest()
        {
            var watch = new Stopwatch();
            var n = 600000;
            var list = new List<string>(n);
            //每秒生成不少于60万个ID
            watch.Start();
            for (int i = 0; i < 999; i++)
            {
                var id = _Generator.Generate();
                Assert.IsNotNull(id);
                Assert.IsFalse(list.Contains(id));
                list.Add(id);
            }
            watch.Stop();
            Assert.IsTrue(watch.ElapsedMilliseconds < 1*1000);
        }

        class TestIdGenerator : IdGenerator
        {
            protected override string GetSecond()
            {
                var time = DateTime.Now;
                return string.Format("{0}{1}", time.Second.ToString().PadLeft(2,'0'), time.Millisecond.ToString().PadLeft(3,'0'));
            }
        }

    }
}
