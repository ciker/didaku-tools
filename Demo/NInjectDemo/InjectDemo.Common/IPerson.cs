﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;

namespace InjectDemo.Common
{
    public interface IPerson
    {
        String Name { get; }
        Sex Sex { get; }
        uint Age { get; }

        //void SetCurrentFood(IFood food);
        //void SetCurrentSport(ISport sport);

        void Eat();
        void Play();
    }
}
