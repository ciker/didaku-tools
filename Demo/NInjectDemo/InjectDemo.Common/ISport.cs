﻿using System;

namespace InjectDemo.Common
{
    public interface ISport
    {
        String Name { get; }
    }
}