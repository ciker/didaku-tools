﻿using System;

namespace InjectDemo.Common
{
    public interface IFood
    {
        String Name { get; }
    }
}