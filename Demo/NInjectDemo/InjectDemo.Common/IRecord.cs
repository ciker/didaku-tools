﻿using System;

namespace InjectDemo.Common
{
    public interface IRecord
    {
        string Id { get; }
        DateTime Occur { get; }
        string Infomation { get; }
    }
}