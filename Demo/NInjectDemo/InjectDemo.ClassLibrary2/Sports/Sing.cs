﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary2.Sports
{
    public class Sing : ISport
    {
        public string Name
        {
            get { return GetType().Name; }
        }
    }
}