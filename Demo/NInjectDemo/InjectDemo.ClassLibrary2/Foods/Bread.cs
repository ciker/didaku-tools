﻿namespace InjectDemo.ClassLibrary2.Foods
{
    public class Bread : Ice
    {
        private static int _Flag;
        private string _Name;

        public override string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_Name))
                {
                    _Name = string.Format("{0},{1}", _Flag, GetType().Name);
                    _Flag++;
                }
                return _Name;
            }
        }
    }
}