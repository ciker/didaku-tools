﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary2.Foods
{
    public class Ice : IFood
    {
        public virtual string Name
        {
            get { return GetType().Name; }
        }
    }
}