﻿using System;
using InjectDemo.ClassLibrary1;
using InjectDemo.Common;
using InjectDemo.Manager;
using Ninject;

namespace InjectDemo.Consoles
{
    internal class Program
    {
        private static bool _WillFinished;

        private static void Main(string[] args)
        {
            Console.WriteLine(">>>=======================");
            Console.WriteLine(">>>== NInject Demo. ======");
            Console.WriteLine(">>>=======================");
            while (!_WillFinished)
            {
                Console.WriteLine(">>>=======================");
                Console.WriteLine("0.退出");
                Console.WriteLine("1.基本的函数注入方法");
                Console.WriteLine("2.一个接口多种实现的对象集合的使用方法");

                string read = Console.ReadLine();
                if (!string.IsNullOrEmpty(read))
                {
                    switch (read.TrimStart().Substring(0, 1))
                    {
                        case "0":
                            _WillFinished = true;
                            break;
                        case "1":
                            基本的函数注入方法();
                            break;
                        case "2":
                            一个接口多种实现的对象集合的使用方法();
                            break;
                    }
                    Console.WriteLine();
                }
            }
        }

        private static void 基本的函数注入方法()
        {
            PersonModule.SetModel(PersonModule.Model.Single);
            FoodModule.SetModel(FoodModule.Model.Single);
            SportModule.SetModel(SportModule.Model.Single);

            var kernel = new CoreKernel();

            var food = kernel.Get<IFood>();
            Console.WriteLine("食物:"+food.Name);

            var sport = kernel.Get<ISport>();
            Console.WriteLine("运动:" + sport.Name);

            var person = kernel.Get<IPerson>();
            Console.WriteLine("人:" + person.Name);
            person.Play();
            person.Eat();
        }

        private static void 一个接口多种实现的对象集合的使用方法()
        {
            Console.Clear();
            Console.WriteLine("PersonManager中的设置IPerson集合的方法是注入方式的");
            Console.WriteLine("本函数测试1.如何使得一个类型能够激发注入");
            Console.WriteLine("本函数测试2.一个接口多种实现的集合的获取方式");

            PersonModule.SetModel(PersonModule.Model.All);
            var kernel = new CoreKernel();
            var people = kernel.Get<PersonManager>(); //不能直接实例，如果直接实例将无法激发注入的动作
            people.Start();

            try
            {
                people = new PersonManager();
                people.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("失败的操作：" + e.Message);
            }
        }
    }
}