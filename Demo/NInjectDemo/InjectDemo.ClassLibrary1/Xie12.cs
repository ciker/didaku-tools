﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Xie12 : Huangyang
    {
        public override uint Age
        {
            get { return 33; }
        }

        public override Sex Sex
        {
            get { return Sex.Female; }
        }
    }
}