﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Chen9 : Huangyang
    {
        public override uint Age
        {
            get { return 12; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}