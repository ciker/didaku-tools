﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class He4 : Huangyang
    {
        public override uint Age
        {
            get { return 35; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}