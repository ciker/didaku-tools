﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Cheng6 : Huangyang
    {
        public override uint Age
        {
            get { return 54; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}