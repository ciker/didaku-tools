﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Peng8 : Huangyang
    {
        public override uint Age
        {
            get { return 35; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}