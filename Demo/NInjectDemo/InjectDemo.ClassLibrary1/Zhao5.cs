﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Zhao5 : Huangyang
    {
        public override uint Age
        {
            get { return 21; }
        }

        public override Sex Sex
        {
            get { return Sex.Female; }
        }
    }
}