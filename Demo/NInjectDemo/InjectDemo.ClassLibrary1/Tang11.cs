﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Tang11 : Huangyang
    {
        public override uint Age
        {
            get { return 16; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}