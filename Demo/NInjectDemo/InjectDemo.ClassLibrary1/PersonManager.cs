﻿using System;
using InjectDemo.Common;
using Ninject;

namespace InjectDemo.ClassLibrary1
{
    /// <summary>
    /// IPerson接口的多种实现的管理类
    /// </summary>
    public class PersonManager
    {
        private IPerson[] _People;

        [Inject]
        public void SetPeople(IPerson[] people)
        {
            _People = people;
        }

        public void Start()
        {
            foreach (IPerson person in _People)
            {
                person.Eat();
                person.Play();
            }
        }
    }
}