﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Hou10 : Huangyang
    {
        public override uint Age
        {
            get { return 45; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}