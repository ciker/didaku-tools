﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Wang2 : Huangyang
    {
        public override uint Age
        {
            get { return 56; }
        }

        public override Sex Sex
        {
            get { return Sex.Male; }
        }
    }
}