﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Zhang1 : Huangyang
    {
        public override uint Age
        {
            get { return 23; }
        }

        public override Sex Sex
        {
            get { return Sex.Female; }
        }
    }
}