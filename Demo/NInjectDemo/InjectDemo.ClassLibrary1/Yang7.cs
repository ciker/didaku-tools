﻿using InjectDemo.Common;

namespace InjectDemo.ClassLibrary1
{
    public class Yang7 : Huangyang
    {
        public override uint Age
        {
            get { return 18; }
        }

        public override Sex Sex
        {
            get { return Sex.Female; }
        }
    }
}