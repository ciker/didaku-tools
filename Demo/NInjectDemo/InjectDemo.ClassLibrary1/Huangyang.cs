﻿using System;
using InjectDemo.Common;
using Ninject;

namespace InjectDemo.ClassLibrary1
{
    public class Huangyang : IPerson
    {
        protected IFood _Food;
        protected ISport _Sport;

        public virtual string Name
        {
            get { return GetType().Name; }
        }

        public virtual Sex Sex
        {
            get { return Sex.Male; }
        }

        public virtual uint Age
        {
            get { return 50; }
        }

        [Inject]
        protected void SetCurrentFood(IFood food)
        {
            _Food = food;
        }

        [Inject]
        protected void SetCurrentSport(ISport sport)
        {
            _Sport = sport;
        }

        public virtual void Eat()
        {
            Console.WriteLine(String.Format("{0} eat {1}.", Name, _Food.Name));
        }

        public virtual void Play()
        {
            Console.WriteLine(String.Format("{0} play {1}.", Name, _Sport.Name));
        }
    }
}