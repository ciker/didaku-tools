﻿using InjectDemo.ClassLibrary1;
using InjectDemo.Common;
using Ninject.Modules;

namespace InjectDemo.Manager
{
    public class PersonModule : NinjectModule
    {
        public enum Model
        {
            All,
            Single
        }

        private static Model _Model = Model.Single;

        public static void SetModel(Model model)
        {
            _Model = model;
        }

        public override void Load()
        {
            switch (_Model)
            {
                case Model.All:
                    BindAll();
                    break;
                case Model.Single:
                default:
                    Bind<IPerson>().To<Huangyang>();
                    break;
            }
        }

        private void BindAll()
        {
            Bind<IPerson>().To<Huangyang>();
            Bind<IPerson>().To<Zhang1>();
            Bind<IPerson>().To<Wang2>();
            Bind<IPerson>().To<Li3>();
            Bind<IPerson>().To<He4>();
            Bind<IPerson>().To<Zhao5>();
            Bind<IPerson>().To<Cheng6>();
            Bind<IPerson>().To<Chen9>();
            Bind<IPerson>().To<Hou10>();
            Bind<IPerson>().To<Tang11>();
            Bind<IPerson>().To<Xie12>();
            Bind<IPerson>().To<Person1000>();
        }
    }
}