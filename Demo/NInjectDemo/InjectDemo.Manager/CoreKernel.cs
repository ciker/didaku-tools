﻿using Ninject;

namespace InjectDemo.Manager
{
    public class CoreKernel : StandardKernel
    {
        public CoreKernel()
            : base(new NinjectSettings() { InjectNonPublic = true }, new FoodModule(), new PersonModule(), new SportModule())
        {
        }
    }
}