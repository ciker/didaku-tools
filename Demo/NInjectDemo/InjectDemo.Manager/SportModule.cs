﻿using InjectDemo.ClassLibrary2.Sports;
using InjectDemo.Common;
using Ninject.Modules;

namespace InjectDemo.Manager
{
    public class SportModule : NinjectModule
    {
        public enum Model
        {
            All,
            Single,
        }

        private static Model _Model = Model.Single;

        public static void SetModel(Model model)
        {
            _Model = model;
        }

        public override void Load()
        {
            switch (_Model)
            {
                case Model.All:
                    BindAll();
                    break;
                case Model.Single:
                default:
                    Bind<ISport>().To<Football>();
                    break;
            }
        }

        private void BindAll()
        {
            Bind<ISport>().To<Sing>();
            Bind<ISport>().To<Dance>();
            Bind<ISport>().To<Golf>();
        }
    }
}