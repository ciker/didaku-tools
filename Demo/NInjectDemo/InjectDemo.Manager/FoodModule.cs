﻿using InjectDemo.ClassLibrary2.Foods;
using InjectDemo.Common;
using Ninject.Modules;

namespace InjectDemo.Manager
{
    public class FoodModule : NinjectModule
    {
        public enum Model
        {
            All,
            Single,
            NamedAll,
        }

        private static Model _Model = Model.Single;

        public static void SetModel(Model model)
        {
            _Model = model;
        }

        public override void Load()
        {
            switch (_Model)
            {
                case Model.All:
                    BindAll();
                    break;
                case Model.NamedAll:
                    BindNamedAll();
                    break;
                case Model.Single:
                default:
                    Bind<IFood>().To<Bread>();
                    break;
            }
        }

        private void BindAll()
        {
            
            Bind<IFood>().To<Ice>();
            Bind<IFood>().To<Apple>();
            Bind<IFood>().To<Cooky>();
            Bind<IFood>().To<Bread>();
        }

        private void BindNamedAll()
        {
            Bind<IFood>().To<Ice>().Named("ice");
            Bind<IFood>().To<Apple>().Named("apple");
            Bind<IFood>().To<Cooky>().Named("cooky");
            Bind<IFood>().To<Bread>().Named("bread");
        }
    }
}